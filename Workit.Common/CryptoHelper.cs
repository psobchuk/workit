﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Workit.Common {
	public class CryptoHelper {
		SymmetricAlgorithm cryptoProvider;

		#region Constructors

		public CryptoHelper(string keyString) {
			cryptoProvider = Rijndael.Create();
			string[] keyValues = keyString.Split('|');

			cryptoProvider.Key = Convert.FromBase64String(keyValues[0]);
			cryptoProvider.IV = Convert.FromBase64String(keyValues[1]);
		}

		public CryptoHelper(byte[] key, byte[] IV) {
			cryptoProvider = Rijndael.Create();
			cryptoProvider.Key = key;
			cryptoProvider.IV = IV;
		}

		#endregion

		/// <summary>
		/// Generates string combined with aes key and IV (information vector)
		/// </summary>
		/// <returns>base64 combined string</returns>
		public static string CreateKeyString() {
			SymmetricAlgorithm aesProvider = Rijndael.Create();
			aesProvider.GenerateKey();
			aesProvider.GenerateIV();

			return string.Format("{0}|{1}", Convert.ToBase64String(aesProvider.Key), Convert.ToBase64String(aesProvider.IV));
		}

		public static CryptoHelper LoadCryptoHelper(string filepath) {
			string aesKey = string.Empty;
			using (StreamReader sReader = new StreamReader(filepath)) {
				aesKey = sReader.ReadToEnd();
			}

			return new CryptoHelper(aesKey);
		}

		/// <summary>
		/// Encrypts provided string
		/// </summary>
		/// <param name="value">string to encrypt</param>
		/// <returns>Base64 string</returns>
		public string EncryptString(string value) {
			byte[] buffer = EncryptToBytes(value);
			return Convert.ToBase64String(buffer);

		}

		/// <summary>
		/// Decrypts provided string
		/// </summary>
		/// <param name="value">Encrypted string</param>
		/// <returns>decrypted string</returns>
		public string DecryptString(string value) {
			byte[] buffer = DecryptToBytes(value);
			return Encoding.Unicode.GetString(buffer);
		}

		/// <summary>
		/// Get a cryptographic hash (based on SHA256) of the provided string
		/// </summary>
		/// <param name="plainText">Any plain string</param>
		/// <returns>A base 64 string representing a 256 bit hash of the plain text</returns>
		public static string Hash(string plainText) {
			return ComputeHash(plainText, new SHA256Managed());
		}

		/// <summary>
		/// Get a cryptographic hash (based on MD5) of the provided string
		/// </summary>
		/// <param name="plainText">Any plain string</param>
		/// <returns>A base 64 string representing a md5 hash of the plain text</returns>
		public static string MD5Hash(string plainText) {
			return ComputeHash(plainText, new MD5CryptoServiceProvider());
		}

		#region Cryptography methods

		private byte[] EncryptToBytes(string value) {
			byte[] buffer = Encoding.Unicode.GetBytes(value);
			MemoryStream mStream = new MemoryStream();

			ICryptoTransform encryptor = cryptoProvider.CreateEncryptor();
			using (CryptoStream cStream = new CryptoStream(mStream, encryptor, CryptoStreamMode.Write)) {
				cStream.Write(buffer, 0, buffer.Length);
				cStream.FlushFinalBlock();
				cStream.Close();
			}

			buffer = mStream.ToArray();
			mStream.Close();

			return buffer;
		}

		private byte[] DecryptToBytes(string value) {
			byte[] buffer = Convert.FromBase64String(value);
			MemoryStream mStream = new MemoryStream();

			ICryptoTransform decryptor = cryptoProvider.CreateDecryptor();
			using (CryptoStream cStream = new CryptoStream(mStream, decryptor, CryptoStreamMode.Write)) {
				cStream.Write(buffer, 0, buffer.Length);
				cStream.FlushFinalBlock();
				cStream.Close();
			}

			buffer = mStream.ToArray();
			mStream.Close();

			return buffer;
		}

		private static string ComputeHash(string plainText, HashAlgorithm algorithm) {
			byte[] buffer = Encoding.UTF8.GetBytes(plainText);
			byte[] hashBuffer = algorithm.ComputeHash(buffer);

			return Convert.ToBase64String(hashBuffer);
		}

		#endregion

	}
}
