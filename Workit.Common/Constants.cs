﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workit.Common {
	public class Constants {
		//db constants
		public const string CONNECTION_STRING = "workitconnection";

		//authentication constants
		public const string CURRENT_USER = "currentuser";
		public const string ROLES = "roles";
		public const int COOKIE_EXPIRE_TIME = 30;

		//common constants
		public const string DEFAULT_CULTURE = "uk";
		public const string RETURN_URL = "returnurl";
		public const string AES_KEY_FILE = "~/keyFile.txt";
		public const string LOGIN_MESSAGE = "loginmessage";

		//mailing constants
		public const string DEFAULT_EMAIL = "test@rocketwizard.com.ua";

		//upload constants
		public const string PROFILE_PHOTO = "avatar.png";
		public const string DEFAULT_PHOTO_PATH = "/Content/img/profile.png";

		public const string TASK_MANAGER = "TASK_MANAGER";
		public const string USER_MANAGER = "USER_MANAGER";
		public const string PROFILE_MANAGER = "PROFILE_MANAGER";
		public const string PAYMENT_MANAGER = "PAYMENT_MANAGER";
	}
}
