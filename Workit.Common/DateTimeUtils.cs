﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workit.Common {
	public static class DateTimeUtils {
		public static bool ParseDateTime(string datestring, string timestring, out DateTime datetime) {
			var date = new DateTime();
			var time = new DateTime();
			if(DateTime.TryParse(datestring, out date) && DateTime.TryParse(timestring, out time)) {
				datetime = new DateTime(date.Year, date.Month, date.Day, time.Hour, time.Minute, time.Second);
				return true;
			}

			datetime = DateTime.MinValue;
			return false;
		}
	}
}
