﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Workit.Common {
	public enum SystemRoles {
		Administrator = 1,
		Customer = 2,
		Performer = 3
	}

	public enum UserStatus {
		Active = 1,
		Deleted = 2,
		Banned = 3,
		Pending = 4
	}

	public enum EmailTemplate {
		EmailConfirmation = 1,
		PasswordReset = 2,
		RecoverAccount = 3,
		NewRequest = 4,
		TaskCompleted = 5,
		RequestAccepted = 6
	}

	public enum UploadFileType {
		RemoteUrl = 1,
		LocalFile = 2
		
	}

	public enum TaskStatus {
		NotStarted = 1,
		InProgress = 2,
		Finished = 3,
		Drafted = 4
	}

	public enum ResponseType {
		Good = 1,
		Neutral = 2,
		Bad = 3
	}

	public enum TaskUserType {
		Owner = 1,
		Performer = 2
	}

	public enum SocialResponseType {
		Vkontakte = 1,
		GooglePlus = 2,
		Facebook = 3
	}
}
