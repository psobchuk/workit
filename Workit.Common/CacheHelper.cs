﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;

namespace Workit.Common {
	public delegate object emptyCacheCallback();

	public class CacheHelper {
		private static CacheHelper cache;

		public static CacheHelper Instance {
			get {
				if (cache == null) {
					cache = new CacheHelper();
				}

				return cache;
			}
		}

		/// <summary>
		/// Returns an object from cache and populates it from delegate in case of empty cache
		/// </summary>
		/// <param name="key">cache key string</param>
		/// <param name="fnCallback">function that return new instance of object to cache</param>
		/// <returns>Requested object from cache</returns>
		public T GetObject<T>(string key, emptyCacheCallback fnCallback) {
			if (HttpContext.Current.Cache[key] == null) {
				HttpContext.Current.Cache.Insert(key, fnCallback(), null, DateTime.Now.AddMinutes(10), Cache.NoSlidingExpiration);
			}

			return (T)HttpContext.Current.Cache[key];
		}
	}
}
