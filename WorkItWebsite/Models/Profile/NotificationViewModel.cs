﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Workit.DataModel;

namespace WorkItWebsite.Models {
	public class NotificationViewModel {
		public bool NotifyNews { get; set; }
		public bool NotifyTaskAssigned { get; set; }
		public bool NotifyTaskFinished { get; set; }
		public bool NotifyNewTaskComment { get; set; }
		public bool NotifyTaskAccepted { get; set; }

		public NotificationViewModel() { }

		public NotificationViewModel(UserSettings settings) {
			NotifyNews = settings.NotifyNews;
			NotifyTaskAssigned = settings.NotifyTaskAssigned;
			NotifyTaskFinished = settings.NotifyTaskFinished;
			NotifyNewTaskComment  = settings.NotifyNewTaskComment;
			NotifyTaskAccepted = settings.NotifyTaskAccepted;
		}
	}
}