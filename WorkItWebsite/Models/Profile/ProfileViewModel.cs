﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Workit.DataModel;

namespace WorkItWebsite.Models {
	public class ProfileViewModel {
		public UserViewModel UserVM { get; set; }
		public NotificationViewModel NotificationVM { get; set; }
		public List<string> UserAlerts { get; set; }

		public ProfileViewModel(User user) {
			UserVM = new UserViewModel(user);
			NotificationVM = new NotificationViewModel(user.UserSettings);
		}

		public ProfileViewModel() { }
	}
}