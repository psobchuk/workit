﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Workit.Common;
using Workit.DataModel;
using Workit.Resources;

namespace WorkItWebsite.Models {
	public class UserViewModel {
		public long UserId { get; set; }
		public string UserName { get; set; }
		public string Email { get; set; }
		public string Type { get; set; }
		public string Status { get; set; }
		public string LastLogin { get; set; }
		public string ImgUrl { get; set; }
		public string Age { get; set; }
		public string BirthDate { get; set; }
		public int Gender { get; set; }
		public string Description { get; set; }
		public bool IsVerified { get; set; }

		public int ResponsesCount { get; set; }
		public int OwnerTaskCount { get; set; }
		public int PerformerTaskCount { get; set; }
		public bool IsProfileOwner { get; set; }

		public UserInfo UserInfo { get; set; }

		public UserResponseStats ResponseStats { get; set; }

		public UserViewModel(User user) {
			UserId = user.UserId;
			UserName = user.FirstName + " " + user.LastName;
			Email = user.Email;
			Type = string.Join(", ", user.Roles.Select(r => r.RoleName));
			Status = ((UserStatus)user.Status).ToString();
			LastLogin = user.LastLogin.ToString();
			ImgUrl = ContentManager.GetUserFilePath(user.UserId, Constants.PROFILE_PHOTO);
			if (user.BirthDate.HasValue) {
				BirthDate = user.BirthDate.Value.ToShortDateString();
				var year = new DateTime(DateTime.Now.Ticks - user.BirthDate.Value.Ticks).Year - 1;
				Age = year.ToString();
			}

			Gender = user.Gender;

			Description = user.UserInfo.ExtraDescription;
			if(string.IsNullOrEmpty(user.UserInfo.ExtraDescription)) {
				Description = StringResources.AboutMeDescription;
			}

			UserInfo = user.UserInfo;
		}
	}
}