﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkItWebsite.Models {
	public class UserResponseStats {
		public int GoodCount { get; set; }
		public int BadCount { get; set; }
		public int NeutralCount { get; set; }
		public int TotalCount { get; set; }
	}
}