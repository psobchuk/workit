﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Workit.Common;
using Workit.DataModel;
using Workit.Resources;

namespace WorkItWebsite.Models {
	public class UserEditViewModel {
		public long UserId { get; set; }
		public string Email { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Phone { get; set; }
		public ImageUploadModel ImageUploadData { get; set; }
		public int Gender { get; set; }
		public BirthDateModel BirthDate { get; set; }
		public NotificationViewModel NotificationVM { get; set; }
		public List<string> UserAlerts { get; set; }
		public string ImgUrl { get; set; }
		public UserInfo UserInfo { get; set; }

		public SelectList EducationOptions { get; set; }
		
		public UserEditViewModel() { }

		public UserEditViewModel(User user) {
			UserId = user.UserId;
			FirstName = user.FirstName;
			LastName = user.LastName;
			Phone = user.Phones;
			Gender = user.Gender;
			ImageUploadData = new ImageUploadModel();
			BirthDate = new BirthDateModel(user.BirthDate.Value);
			NotificationVM = new NotificationViewModel(user.UserSettings);
			ImageUploadData.Url = ContentManager.GetUserFilePath(user.UserId, Constants.PROFILE_PHOTO);
			Email = user.Email;
			ImgUrl = ImageUploadData.Url;
			UserInfo = user.UserInfo;

			GetEducationOptions();
		}

		private void GetEducationOptions() {
			var options = new List<SelectListItem>();

			options.Add(new SelectListItem { Text = StringResources.Basic, Value = StringResources.Basic });
			options.Add(new SelectListItem { Text = StringResources.PreMiddle, Value = StringResources.PreMiddle });
			options.Add(new SelectListItem { Text = StringResources.Middle, Value = StringResources.Middle });
			options.Add(new SelectListItem { Text = StringResources.PreFull, Value = StringResources.PreFull });
			options.Add(new SelectListItem { Text = StringResources.Full, Value = StringResources.Full });

			EducationOptions = new SelectList(options, "Value", "Text");
		}
	}
}