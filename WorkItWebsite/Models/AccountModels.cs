﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using Workit.Resources;
using WorkItWebsite.Helpers;

namespace WorkItWebsite.Models
{
	public class PartialLoginModel {
		public string UserLink { get; set; }
		public string DisplayName { get; set; }
	}

	public class RegisterExternalLoginModel
	{
		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public string FirstName { get; set; }

		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public string LastName { get; set; }

		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		[CheckEmail(false, ErrorMessageResourceName = "EmailInUse", ErrorMessageResourceType = typeof(StringResources))]
		[RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
		ErrorMessageResourceName = "InvalidEmail", ErrorMessageResourceType = typeof(StringResources))]
		public string Email { get; set; }

		public string Phone { get; set; }

		public ImageUploadModel ImageUploadData { get; set; }
		
		public string ImageUrl { get; set; }

		public string ProviderUserId { get; set; }

		public string ProviderName { get; set; }

		public string ActivationKey { get; set; }

		public bool TermsAccepted { get; set; }

		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public int Gender { get; set; }

		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public BirthDateModel BirthDate { get; set; }

		public RegisterExternalLoginModel() {
			ImageUploadData = new ImageUploadModel();
			BirthDate = new BirthDateModel();
		}
	}

	public class ResetPasswordModel {
		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		[StringLength(100, ErrorMessageResourceName = "PasswordValidationMessage",
		ErrorMessageResourceType = typeof(StringResources), MinimumLength = 6)]
		[DataType(DataType.Password)]
		public string NewPassword { get; set; }

		[DataType(DataType.Password)]
		[Compare("NewPassword", ErrorMessageResourceName = "ComparePasswordValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public string ConfirmPassword { get; set; }
	}

	public class ChangePasswordModel {
		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public string OldPassword { get; set; }
		
		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		[StringLength(100, ErrorMessageResourceName = "PasswordValidationMessage",
		ErrorMessageResourceType = typeof(StringResources), MinimumLength = 6)]
		[DataType(DataType.Password)]
		public string NewPassword { get; set; }

		[DataType(DataType.Password)]
		[Compare("NewPassword", ErrorMessageResourceName = "ComparePasswordValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public string ConfirmPassword { get; set; }
	}

	public class LoginModel
	{
		public string LoginEmail { get; set; }

		[DataType(DataType.Password)]
		public string Password { get; set; }

		public bool RememberMe { get; set; }
	}

	public class RegisterModel
	{
		[Required(ErrorMessageResourceName="RequiredValidationMessage",
		ErrorMessageResourceType=typeof(StringResources))]
		public string Name { get; set; }

		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public string Surname { get; set; }

		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		[CheckEmail(false, ErrorMessageResourceName="EmailInUse", ErrorMessageResourceType=typeof(StringResources))]
		[RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
		ErrorMessageResourceName = "InvalidEmail", ErrorMessageResourceType = typeof(StringResources))]
		public string Email { get; set; }

		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		[StringLength(100, ErrorMessageResourceName="PasswordValidationMessage", 
		ErrorMessageResourceType=typeof(StringResources), MinimumLength = 6)]
		[DataType(DataType.Password)]
		public string Password { get; set; }

		[DataType(DataType.Password)]
		[Compare("Password", ErrorMessageResourceName="ComparePasswordValidationMessage",
		ErrorMessageResourceType=typeof(StringResources))]
		public string ConfirmPassword { get; set; }

		public string Phone { get; set; }

		public string RoleId { get; set; }

		public string ActivationKey { get; set; }

		public ImageUploadModel ImageUploadData { get; set; }

		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public int Gender { get; set; }

		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public BirthDateModel BirthDate { get; set; }

		public string ReturnUrl { get; set; }

		public bool TermsAccepted { get; set; }

		public RegisterModel() {
			ImageUploadData = new ImageUploadModel();
			BirthDate = new BirthDateModel(DateTime.Now);
		}

	}

	public class ExternalLogin
	{
		public string Provider { get; set; }
		public string ProviderDisplayName { get; set; }
		public string ProviderUserId { get; set; }
	}
}
