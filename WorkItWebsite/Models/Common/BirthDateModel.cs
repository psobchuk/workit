﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Workit.Resources;

namespace WorkItWebsite.Models {
	public class BirthDateModel {
		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public string BirthDay { get; set; }
		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public string BirthMonth { get; set; }
		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public string BirthYear { get; set; }

		public SelectList Days { get; set; }

		public SelectList Months { get; set; }

		public SelectList Years { get; set; }

		public BirthDateModel() {
			PopulateDates();
		}

		public BirthDateModel(DateTime? selectedDate = null) {
			PopulateDates(selectedDate);
		}

		public DateTime SelectedDate { 
			get {
				return new DateTime(int.Parse(BirthYear), int.Parse(BirthMonth), int.Parse(BirthDay));
			}
		}

		public void SetDate(DateTime date) {
			SelectListItem item  = Days.Where(u => u.Value == date.Day.ToString()).FirstOrDefault();
			
			Months.Where(u => u.Value == date.Month.ToString()).FirstOrDefault().Selected = true;
			Years.Where(u => u.Value == date.Year.ToString()).FirstOrDefault().Selected = true;
		}

		private void PopulateDates(DateTime? selectedDate = null) {
			var options = new List<SelectListItem>();
			//get days
			for (int i = 1; i <= 31; i++) options.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
			Days = selectedDate.HasValue ? new SelectList(options, "Value", "Text", selectedDate.Value.Day.ToString()) : new SelectList(options, "Value", "Text");

			//get months
			options = new List<SelectListItem>();
			options.Add(new SelectListItem { Text = "Січень", Value = "1" });
			options.Add(new SelectListItem { Text = "Лютий", Value = "2" });
			options.Add(new SelectListItem { Text = "Березень", Value = "3" });
			options.Add(new SelectListItem { Text = "Квітень", Value = "4" });
			options.Add(new SelectListItem { Text = "Травень", Value = "5" });
			options.Add(new SelectListItem { Text = "Червень", Value = "6" });
			options.Add(new SelectListItem { Text = "Липень", Value = "7" });
			options.Add(new SelectListItem { Text = "Серпень", Value = "8" });
			options.Add(new SelectListItem { Text = "Вересень", Value = "9" });
			options.Add(new SelectListItem { Text = "Жовтень", Value = "10" });
			options.Add(new SelectListItem { Text = "Листопад", Value = "11" });
			options.Add(new SelectListItem { Text = "Грудень", Value = "12" });
			Months = selectedDate.HasValue ? new SelectList(options, "Value", "Text", selectedDate.Value.Month.ToString()) : new SelectList(options, "Value", "Text");

			//get years
			options = new List<SelectListItem>();
			for (int i = DateTime.Now.Year; i >= 1930; i--) options.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
			Years = selectedDate.HasValue ? new SelectList(options, "Value", "Text", selectedDate.Value.Year.ToString()) : new SelectList(options, "Value", "Text");
		}
	}
}