﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Workit.Resources;
using WorkItWebsite.Helpers;

namespace WorkItWebsite.Models {
	public class RecoverAccountModel {
		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		[CheckEmail(true, ErrorMessageResourceName = "EmailInUse", ErrorMessageResourceType = typeof(StringResources))]
		[RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
		ErrorMessageResourceName = "InvalidEmail", ErrorMessageResourceType = typeof(StringResources))]
		public string Email { get; set; }
	}
}