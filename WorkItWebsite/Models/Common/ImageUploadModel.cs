﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkItWebsite.Models {
	public class ImageUploadModel {
		public HttpPostedFileBase ProfilePhoto { get; set; }
		public string Url { get; set; }
		public int X { get; set; }
		public int Y { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
		public int UploadFileType { get; set; }
	}
}