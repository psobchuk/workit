﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace WorkItWebsite.Models {
	public class AccountBalanceViewModel {
		public string OperationXml { get; set; }
		public string Signature { get; set; }

		public AccountBalanceViewModel() {
		}
	}
}