﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Workit.DataModel;

namespace WorkItWebsite.Models {
	public class RequestViewModel {
		public long RequestId { get; set; }
		public long TaskId { get; set; }
		public string Question { get; set; }
		public long RequestorId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string RequestorImage { get; set; }
		public string Created { get; set; }
		public string Price { get; set; }
		public string ProfileUrl { get; set; }

		public List<CommentViewModel> Comments { get; set; }
	}
}