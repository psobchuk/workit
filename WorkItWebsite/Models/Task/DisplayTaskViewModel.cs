﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Workit.Common;
using Workit.DataAccess;
using Workit.DataModel;
using Workit.Resources;

namespace WorkItWebsite.Models {
	public class DisplayTaskViewModel {
		public TaskViewModel Task { get; set; }
		public bool IsPerformer { get; set; }
		public bool IsOwner { get; set; }
		public bool IsSubscribed { get; set; }
		public bool IsLocked { get; set; }
		public bool IsFinished { get; set; }
		public bool IsCustomPrice { get; set; }
		public bool IsActive { get; set; }
		public bool IsAcceptedPerformer { get; set; }
		public string Price { get; set; }
		public string OwnerUrl { get; set; }
		public string OwnerName { get; set; }
		public string TaskStatusMessage { get; set; }

		public DisplayTaskViewModel(Order task, User currUser) {
			var db = new EntityProvider();
			var model = new TaskViewModel();
			model.Name = task.Name;
			model.Description = task.Description;
			model.User = task.Client;
			model.Category = task.Category.Name;
			model.Price = task.Price.ToString();
			model.Deadline = task.Deadline.Value.ToString();
			model.Created = task.Created.Value.ToString();
			model.Phone = task.Phone;

			Task = model;
			Price = (task.Price > 0) ? task.Price.ToString() + " грн." : StringResources.PriceSetByExecutor;
			var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
			OwnerUrl =  urlHelper.Action("profile", "user", new { id = task.Client.UserId });
			OwnerName = task.Client.FirstName + " " + task.Client.LastName;

			if(currUser != null) {
				IsOwner = task.Client.UserId == currUser.UserId;
				var um = new UserManager(db);
				IsPerformer = um.IsUserInRole(currUser.UserId, SystemRoles.Performer);
				var tm = new TaskManager(db);
				IsSubscribed = tm.IsRequested(task.OrderId, currUser.UserId);
				IsLocked = task.PerformerId > 0;
				IsFinished = task.Status == (int)TaskStatus.Finished;
				IsCustomPrice = !(task.Price > 0);
				IsActive = string.IsNullOrEmpty(currUser.ActivationKey);
				IsAcceptedPerformer = currUser.UserId == task.PerformerId;
			}

			SetTaskStatusMessage(task);
		}

		private void SetTaskStatusMessage(Order task) {
			switch(task.Status) {
				case (int)TaskStatus.NotStarted:
					TaskStatusMessage = StringResources.TaskIsOpenedForRequests;
					break;
				case (int)TaskStatus.InProgress:
					TaskStatusMessage = StringResources.TaskIsInProgress;
					break;
				case (int)TaskStatus.Finished:
					TaskStatusMessage = StringResources.TaskCompleted;
					break;
			}
		}
	}
}