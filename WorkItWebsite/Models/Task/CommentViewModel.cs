﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Workit.DataModel;

namespace WorkItWebsite.Models {
	public class CommentViewModel {
		public string Text { get; set; }
		public string Created { get; set; }
		public long CommenterId { get; set; }
		public string ImgUrl { get; set; }
		public long RequestId { get; set; }
		public string ProfileUrl { get; set; }
		public string DisplayName { get; set; }
		public long TaskId { get; set; }
	}
}