﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Workit.Common;
using Workit.DataAccess;
using Workit.DataModel;
using Workit.Resources;

namespace WorkItWebsite.Models {
	public class TaskViewModel {
		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public string Name { get; set; }
		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public string Description { get; set; }
		
		public string Price { get; set; }
		public string Created { get; set; }
		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public string DeadlineDate { get; set; }

		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public string DeadlineTime { get; set; }

		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public string Category { get; set; }

		public bool TermsAccepted { get; set; }
		public bool CustomPrice { get; set; }

		public User User { get; set; }
		public int Status { get; set; }
		public string Deadline { get; set; }
		public string StatusText { get; set; }

		public string TaskUrl { get; set; }

		public string DisplayName { get; set; }
		public string ProfileUrl { get; set; }
		
		[Required(ErrorMessageResourceName = "RequiredValidationMessage",
		ErrorMessageResourceType = typeof(StringResources))]
		public string Phone { get; set; }

		public SelectList CategoryOptions { get; set; }

		public TaskViewModel() { }

		public TaskViewModel(bool setCategories = false) { 
			if(setCategories) PopulateCategories();
		}

		public void PopulateCategories() {
			var db = new EntityProvider();
			List<CategoryView> categories = db.CategoryRepository.Get().Select( c => new CategoryView(c)).ToList();
			CategoryOptions = new SelectList(categories, "CategoryId", "Name");
		}

		public class CategoryView {
			public string CategoryId { get; set; }
			public string Name { get; set; }

			public CategoryView(Category category) {
				this.CategoryId = category.CategoryId.ToString();
				this.Name = SetCategoryName(category.CategoryId);
			}

			private string SetCategoryName(int id) {
				switch(id) {
					case 1:
						return StringResources.CourierTask;
					case 2:
						return StringResources.DeliveryTask;
					case 3:
						return StringResources.ComputerTask;
					case 4:
						return StringResources.PhotoVideoTask;
					case 5:
						return StringResources.HelperTask;
					case 6:
						return StringResources.RepairTask;
					case 7:
						return StringResources.FlyersTask;
				}

				return string.Empty;
			}
		}
	}
}