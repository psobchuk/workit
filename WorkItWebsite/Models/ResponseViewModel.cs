﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkItWebsite.Models {
	public class ResponseViewModel {
		public long RespondentId { get; set; }
		public string RespondentName { get; set; }
		public string CreatedDate { get; set; }
		public string Description { get; set; }
		public int ResponseType { get; set; }
		public string RespondentImage { get; set; }
		public string ProfileUrl { get; set; }
	}
}