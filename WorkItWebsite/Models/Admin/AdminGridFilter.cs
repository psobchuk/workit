﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkItWebsite.Models {
	public class AdminGridFilter {
		public string SortColumn { get; set; }
		public string SortOrder { get; set; }
		public int PageIndex { get; set; }
		public int PageSize  { get; set; }
	}
}