﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Workit.Common;
using Workit.DataModel;
using WorkItWebsite.Helpers;

namespace WorkItWebsite.Models {
	public class UserAdministrationVM {
		public List<UserAdminModel> Users { get; set; }

		public SelectList StatusOptions { get; set; }

		public SelectList TypeOptions { get; set; }

		public UserAdministrationVM() {
			var options = new List<SelectListItem>();
			options.Add(new SelectListItem { Text = UserStatus.Active.ToString(), Value = ((int)UserStatus.Active).ToString() });
			options.Add(new SelectListItem { Text = UserStatus.Banned.ToString(), Value = ((int)UserStatus.Banned).ToString() });
			options.Add(new SelectListItem { Text = UserStatus.Deleted.ToString(), Value = ((int)UserStatus.Deleted).ToString() });
			options.Add(new SelectListItem { Text = UserStatus.Pending.ToString(), Value = ((int)UserStatus.Pending).ToString() });
			StatusOptions = new SelectList(options, "Value", "Text");

			options = new List<SelectListItem>();
			options.Add(new SelectListItem { Text = SystemRoles.Administrator.ToString(), Value = ((int)SystemRoles.Administrator).ToString() });
			options.Add(new SelectListItem { Text = SystemRoles.Customer.ToString(), Value = ((int)SystemRoles.Customer).ToString() });
			options.Add(new SelectListItem { Text = SystemRoles.Performer.ToString(), Value = ((int)SystemRoles.Performer).ToString() });
			TypeOptions = new SelectList(options, "Value", "Text");
		}
	}

	public class UserAdminModel {
		public long UserId { get; set; }
		[Required]
		public string FirstName { get; set; }
		[Required]
		public string LastName { get; set; }
		[Required]
		[CheckEmail(false)]
		public string Email { get; set; }
		[Required]
		public string Password { get; set; }

		public string Status { get; set; }

		public string Type { get; set; }

		public string UserName { get; set; }

		public string LastLogin { get; set; }

		public UserAdminModel() { }

		public UserAdminModel(User user) {
			UserId = user.UserId;
			UserName = user.FirstName + " " + user.LastName;
			Email = user.Email;
			LastLogin = user.LastLogin.HasValue ? user.LastLogin.Value.ToString() : "";
			Status = ((UserStatus)user.Status).ToString();
			Type = string.Join(", ", user.Roles.Select(r => r.RoleName));
		}
	}

	public class TaskAdminModel {
		[Required]
		public string Name { get; set; }
		[Required]
		public string Description { get; set; }
		[Required]
		public int Price { get; set; }
		[Required]
		public string DeadlineDate { get; set; }
		[Required]
		public string DeadlineTime { get; set; }
		public int CreatorId { get; set; }
		public string CategoryId { get; set; }

		public SelectList CategoryOptions { get; set; }

		public TaskAdminModel(List<Category> categories) {
			var options = new List<SelectListItem>();
			foreach(var item in categories) {
				options.Add(new SelectListItem { Text = item.Name, Value = item.CategoryId.ToString() });
			}

			CategoryOptions = new SelectList(options, "Value", "Text");
		}
	}
}