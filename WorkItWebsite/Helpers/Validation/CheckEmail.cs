﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Workit.Common;
using Workit.DataAccess;

namespace WorkItWebsite.Helpers {
	public class CheckEmail : ValidationAttribute {
		private bool isExists;
		public CheckEmail(bool exists) { 
			isExists = exists;
		}

		
		public override bool IsValid(object value) {
			var isValid = false;
			string email = Convert.ToString(value);
			var db = new EntityProvider();
			var user = db.UserRepository.Get(u => u.Email == email).FirstOrDefault();
			
			if(isExists) {
				if (user == null || user.Status == (int)UserStatus.Deleted) {
					isValid = false;
				} else {
					isValid = true;
				}
			} else {
				if (user == null || user.Status == (int)UserStatus.Deleted) {
					isValid = true;
				}
			}

			return isValid; 
		}
	}
}