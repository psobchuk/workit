﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkItWebsite.Helpers {
	public class JsonResponse {
		public int Status { get ; set; }
		public string ResponseMessage { get; set; }
		public string Data { get; set; }
	}
}