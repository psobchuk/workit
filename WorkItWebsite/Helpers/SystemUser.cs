﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace WorkItWebsite.Helpers {
	public class SystemUser : IPrincipal {
		#region Private Members

		private string[] roles;
		private IIdentity identity;

		#endregion

		#region Costructors

		public SystemUser(IIdentity identity, string[] roles) {
			this.identity = identity;
			this.roles = new string[roles.Length];
			roles.CopyTo(this.roles, 0);
			Array.Sort(this.roles);
		}

		#endregion

		public IIdentity Identity {
			get { return this.identity; }
		}

		public bool IsInRole(string role) {
			return Array.BinarySearch(this.roles, role) >= 0 ? true : false;
		}
	}

	public class SystemIdentity : IIdentity {
		#region Private Members

		private string authenticationType;
		private string name;

		#endregion

		#region Constructors

		public SystemIdentity() {
			this.name = String.Empty;
			this.authenticationType = "Forms";
		}

		public SystemIdentity(string name)
			: base() {
			this.name = name;
		}

		#endregion

		#region Public Properties

		public string AuthenticationType {
			get { return this.authenticationType; }
		}

		public bool IsAuthenticated {
			get { return (this.Name != null && !this.name.Equals("")); }
		}

		public string Name {
			get { return this.name; }
		}

		#endregion
	}
}