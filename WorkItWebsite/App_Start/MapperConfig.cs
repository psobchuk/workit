﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Workit.Common;
using Workit.DataModel;
using WorkItWebsite.Models;

namespace WorkItWebsite {
	public static class MapperConfig {
		public static void ConfigureMappings() {
			Mapper.CreateMap<UserAdminModel, User>()
			.ForMember(m => m.Status, opt => opt.MapFrom(m => Convert.ToInt32(m.Status)))
			.ForMember(m => m.Password, opt => opt.MapFrom(m => CryptoHelper.MD5Hash(m.Password)));

			Mapper.CreateMap<User, UserAdminModel>();

			Mapper.CreateMap<TaskAdminModel, Order>()
			.ForMember(m => m.Created, opt => opt.UseValue(DateTime.UtcNow));

			Mapper.CreateMap<Order, TaskAdminModel>();

			Mapper.CreateMap<Request, RequestViewModel>()
			.ForMember(m => m.Created, opt => opt.MapFrom(m => m.RequestDate.Value.ToString()))
			.ForMember(m => m.TaskId, opt => opt.MapFrom(m => m.Task.OrderId))
			.ForMember(m => m.RequestorId, opt => opt.MapFrom(m => m.Requestor.UserId))
			.ForMember(m => m.FirstName, opt => opt.MapFrom(m => m.Requestor.FirstName))
			.ForMember(m => m.LastName, opt => opt.MapFrom(m => m.Requestor.LastName))
			.ForMember(m => m.Price, opt => opt.MapFrom(m => m.CustomPrice))
			.ForMember(m => m.ProfileUrl, opt => opt.MapFrom( m => ContentManager.GetUrl("profile", "user", new { id = m.Requestor.UserId })))
			.ForMember(m => m.RequestorImage, opt => opt.MapFrom(m => 
				ContentManager.GetUserFilePath(m.Requestor.UserId, Constants.PROFILE_PHOTO)
			));

			Mapper.CreateMap<Comment, CommentViewModel>()
			.ForMember(m => m.Created, opt => opt.MapFrom(m => m.Created.Value.ToString()))
			.ForMember(m => m.RequestId, opt => opt.MapFrom(m => m.Request.RequestId))
			.ForMember(m => m.CommenterId, opt => opt.MapFrom(m => m.Commenter.UserId))
			.ForMember(m => m.DisplayName, opt => opt.MapFrom(m => m.Commenter.FirstName + " " + m.Commenter.LastName))
			.ForMember(m => m.ProfileUrl, opt => opt.MapFrom(m => ContentManager.GetUrl("profile", "user", new { id = m.Commenter.UserId })))
			.ForMember(m => m.ImgUrl, opt => opt.MapFrom(m =>
				ContentManager.GetUserFilePath(m.Commenter.UserId, Constants.PROFILE_PHOTO)
			));

			Mapper.CreateMap<Order, TaskViewModel>()
			.ForMember(m => m.Created, opt => opt.MapFrom(m => m.Created.Value.ToShortDateString()))
			.ForMember(m => m.Deadline, opt => opt.MapFrom(m => m.Deadline.Value.ToShortDateString()))
			.ForMember(m => m.DisplayName, opt => opt.MapFrom(m => m.Client.FirstName + " " + m.Client.LastName))
			.ForMember(m => m.ProfileUrl, opt => opt.MapFrom(m => ContentManager.GetUrl("profile", "user", new { id = m.Client.UserId })))
			.ForMember(m => m.TaskUrl, opt => opt.MapFrom(m => ContentManager.GetUrl("view", "task", new { id =  m.OrderId })))
			.ForMember(m => m.StatusText, opt => opt.MapFrom( m => TaskManager.GetTaskStatusText(m)))
			.ForMember(m => m.Price, opt => opt.MapFrom(m => m.Price.ToString() + " грн."));

			Mapper.CreateMap<NotificationViewModel, UserSettings>()
			.ForMember(m => m.NotifyNewTaskCategory, opt => opt.UseValue(false));

			Mapper.CreateMap<PaymentResponse, PaymentHistory>();
		}
	}

	public class RoleResolver: ValueResolver<IEnumerable<Role>, string> {

		protected override string ResolveCore(IEnumerable<Role> source) {
			var lst = new List<string>();
			foreach(var item in source) {
				lst.Add(item.RoleName);
			}

			return string.Join(", ", lst);
		}
	}
}