﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Workit.Common;
using WorkItWebsite.Helpers;

namespace WorkItWebsite {
	public class RouteConfig {
		public static void RegisterRoutes(RouteCollection routes) {
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			//RouteTable.Routes.Add("Localized", new LocalizedRoute(
			//	url: "ru/{controller}/{action}/{id}",
			//	defaults: new { controller = "home", action = "index", id = UrlParameter.Optional }
			//));

			routes.MapRoute(
				name: "Russian",
				url: "{lang}/{controller}/{action}/{id}",
				defaults: new { controller = "home", action = "index", id = UrlParameter.Optional },
				constraints: new { lang = @"^ru$" }

			);

			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "home", action = "index", id = UrlParameter.Optional }
			);
		}
	}
}