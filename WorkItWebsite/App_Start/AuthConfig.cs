﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Web.WebPages.OAuth;
using WorkItWebsite.Models;

namespace WorkItWebsite {
	public static class AuthConfig {
		public static void RegisterAuth() {
			OAuthWebSecurity.RegisterClient(
				new GooglePlusClient(
					"112004154040-l8orjf89m89lgmjcl3jop37nc9ipnl4d.apps.googleusercontent.com", 
					"p2_m57v4cir4zmZ274FyR6-q"
				), 
				"Google+", 
				null
			);

			OAuthWebSecurity.RegisterClient(
				new VKontakteClient(
					"4037934", 
					"PeBypyH1rJvuBJOvFnl1"
				), 
				"Vkontakte", 
				null
			);
			
			OAuthWebSecurity.RegisterClient(
				new FacebookClient(
					"1412956505613713", 
					"5655e110ee52adaf20f39bcddc498755"
				), 
				"Facebook", 
				null
			);
		}
	}
}
