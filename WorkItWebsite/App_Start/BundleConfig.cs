﻿using System.Web;
using System.Web.Optimization;

namespace WorkItWebsite {
	public class BundleConfig {
		// For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
		public static void RegisterBundles(BundleCollection bundles) {
			
			//JQuery
			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
						"~/ThirdParty/jquery/js/jquery-{version}.js"));
			
			//JQuery validation
			bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
						"~/ThirdParty/jquery/js/jquery.unobtrusive*",
						"~/ThirdParty/jquery/js/jquery.validate*",
						"~/ThirdParty/datetimepicker/maskedinput.js"));

			//Bootstrap
			bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
						"~/ThirdParty/requirejs/require.js",
						"~/ThirdParty/bootstrap/js/bootstrap*"));
			bundles.Add(new StyleBundle("~/ThirdParty/bootstrap/rw/css").Include(
						"~/ThirdParty/normalize/normalize.css",
						"~/ThirdParty/bootstrap/css/default.css"));

			//Kendo UI
			bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
						"~/ThirdParty/kendoui/kendo.all.js",
						"~/ThirdParty/kendoui/kendo.aspnetmvc.js"));
			bundles.Add(new StyleBundle("~/ThirdParty/kendoui/rw/css").Include(
						"~/ThirdParty/kendoui/css/kendo.common.css",
						"~/ThirdParty/kendoui/css/kendo.default.css"));
					

			//Knockout
			bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
						"~/ThirdParty/knockout/knockout-{version}.js"));

			//RocketWizard
			bundles.Add(new StyleBundle("~/Content/rw/css").Include(
						"~/Content/css/site.css",
						"~/Content/css/mobile.css"));
			bundles.Add(new ScriptBundle("~/bundles/rw").Include(
				"~/Content/js/common.js"));

			bundles.Add(new ScriptBundle("~/bundles/login").Include(
				"~/Content/js/register/login.js"));

			bundles.Add(new ScriptBundle("~/bundles/imageupload").Include(
				"~/ThirdParty/imageareaselect/jquery.imgareaselect.js",
				"~/Content/js/register/photoupload.js"));

			bundles.Add(new ScriptBundle("~/bundles/editprofile").Include(
				"~/Content/js/editprofile.js"));

			bundles.Add(new ScriptBundle("~/bundles/profile").Include(
				"~/Content/js/profile.js"));

			bundles.Add(new ScriptBundle("~/bundles/task").Include(
				"~/Content/js/tasks.js"));

			bundles.Add(new ScriptBundle("~/bundles/register").Include(
				"~/Content/js/register/register.js"));
		}
	}
}