﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using Workit.Common;
using Workit.DataAccess;
using WorkItWebsite.Controllers;
using WorkItWebsite.Helpers;

namespace WorkItWebsite
{
	// Note: For instructions on enabling IIS6 or IIS7 classic mode, 
	// visit http://go.microsoft.com/?LinkId=9394801

	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();

			WebApiConfig.Register(GlobalConfiguration.Configuration);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			AuthConfig.RegisterAuth();
			MapperConfig.ConfigureMappings();

			ApplicationManager.CreateCryptographyKeyFile();
			DbManager.InitializeDatabaseMigration();
		}

		protected void Application_AuthenticateRequest() {
			AuthenticationManager.ProcessAuthenticationCookie();
		}

		protected void Application_AcquireRequestState() {
			//var languageCookie = HttpContext.Current.Request.Cookies["lang"];
			//var culture = languageCookie != null ? languageCookie.Value : Constants.DEFAULT_CULTURE;
			//var cultureInfo = new CultureInfo("ru");
			//Thread.CurrentThread.CurrentUICulture = cultureInfo;
			//Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureInfo.Name);
		}

		protected void Application_Error() {
			var ctx = HttpContext.Current;
			var ex = ctx.Server.GetLastError();
			if (ex != null) {
				Logger.LogErrorException("Application Error Occurred", ex);
			}

			if(!ApplicationManager.IsDevelopmentMode) {
				ctx.Response.Clear();
 
				RequestContext rc = ((MvcHandler)ctx.CurrentHandler).RequestContext;
				IController controller = new BaseController();
				var context = new ControllerContext(rc, (ControllerBase)controller);
 
				var viewResult = new ViewResult();
				viewResult.ViewName = "Error";
				var httpException = ex as HttpException;
				if (httpException != null) {
					switch (httpException.GetHttpCode()) {
						case 404:
							viewResult.ViewName = "Error404";
							break;
						case 500:
							viewResult.ViewName = "Error500";
							break;
					}

					Response.StatusCode = httpException.GetHttpCode();
				} else {
					Response.StatusCode = 500;
					viewResult.ViewName = "Error500";
				}
				
				viewResult.ViewData.Model = new HandleErrorInfo(ex, context.RouteData.GetRequiredString("controller"), context.RouteData.GetRequiredString("action"));
				context.RequestContext.RouteData.Values["controller"] = "Error";
				viewResult.ExecuteResult(context);
				ctx.Server.ClearError();
			}
		}
	}
}