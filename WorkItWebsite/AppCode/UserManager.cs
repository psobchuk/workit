﻿using AutoMapper;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Workit.Common;
using Workit.DataAccess;
using Workit.DataModel;
using WorkItWebsite.Models;

namespace WorkItWebsite {
	public class UserManager {
		private EntityProvider db;

		public UserManager(EntityProvider entityProvider) {
			db = entityProvider;
		}

		public bool IsUserInRole(long userId, SystemRoles roleType) {
			var user = GetUser(userId);
			var role = user.Roles.Where(r => r.RoleId == (int)roleType).SingleOrDefault();
			
			return role != null ? true : false;
		}

		public User GetUser(long userId) {
			return db.UserRepository.Get(u => u.UserId == userId).SingleOrDefault();
		}

		public User GetUser(string email) {
			return db.UserRepository.Get(u => u.Email == email).FirstOrDefault();
		}

		public bool ValidateUserStatus(User user, UserStatus status) {
			return user.Status == (int)status;
		}

		public void UpdateUser(User oldUser, User updatedUser) {
			UserInfo info = db.UserInfoRepository.Get(u => u.UserInfoId == oldUser.UserInfo.UserInfoId).SingleOrDefault();
			info = updatedUser.UserInfo;
			info.UserInfoId = oldUser.UserInfo.UserInfoId;
			db.UserInfoRepository.Update(oldUser.UserInfo, info);
			db.UserRepository.Update(oldUser, updatedUser);
			db.SaveChanges();
			DisposeCurrentUser();
		}

		public void UpdateUserSettings(User user, UserSettings settings) {
			settings.UserSettingsId = user.UserSettings.UserSettingsId;
			db.UserSettingsRepository.Update(user.UserSettings, settings);
			db.SaveChanges();
			DisposeCurrentUser();
		}

		public void DeleteUser(long userId) {
			User user = db.UserRepository.Get(u => u.UserId == userId).SingleOrDefault();
			var newUser = user;
			newUser.Status = (int)UserStatus.Deleted;
			
			UpdateUser(user, newUser);
		}

		public void ChangePassword(User user, string password) {
			User newUser = user;
			newUser.Password = CryptoHelper.MD5Hash(password);
			UpdateUser(user, newUser);
		}

		public void DisposeCurrentUser() {
			HttpContext.Current.Session[Constants.CURRENT_USER] = null;
		}

		public bool VerifySettingsEnabled(User user, EmailTemplate template) {
			switch(template) {
				case EmailTemplate.NewRequest:
					return user.UserSettings.NotifyTaskAccepted;
				case EmailTemplate.RequestAccepted:
					return user.UserSettings.NotifyTaskAssigned;
				case EmailTemplate.TaskCompleted:
					return user.UserSettings.NotifyTaskFinished;
				default:
					return false;
			}
		}

		public void ProcessPayment(string operation_xml, string signature, long userId) {
			try {
				PaymentResponse response = LiqPayManager.GetPaymentResponse(operation_xml, signature, userId);
				IncreaseAccountBalance(response.Amount, userId);
				SaveTransaction(response);
				DisposeCurrentUser();
			} catch (Exception ex) {
				Logger.LogErrorException("Error processing transaction", ex);
				throw;
			}
		}

		public void IncreaseAccountBalance(int amount, long userId) {
			UserInfo info = db.UserInfoRepository.Get(u => u.UserInfoId== userId).SingleOrDefault();
			var newInfo = info;
			newInfo.PaymentBalance += amount;
			db.UserInfoRepository.Update(info, newInfo);
			db.SaveChanges();
			DisposeCurrentUser();
		}

		public void DecreaseAccountBalance(int amount, long userId) {
			UserInfo info = db.UserInfoRepository.Get(u => u.UserInfoId == userId).SingleOrDefault();
			var newInfo = info;
			newInfo.PaymentBalance -= amount;
			db.UserInfoRepository.Update(info, newInfo);
			db.SaveChanges();
			DisposeCurrentUser();
		}

		public void SaveTransaction(PaymentResponse response) {
			var history = Mapper.Map<PaymentHistory>(response);
			history.Owner = db.UserRepository.Get(u => u.UserId == response.UserId).SingleOrDefault();

			db.PaymentRepository.Insert(history);
			db.SaveChanges();

		}

		public List<PaymentHistory> GetPaymentHistory(long userId) {
			var lst = db.PaymentRepository.Get(u => u.Owner.UserId == userId).ToList();
			return lst;
		}


		#region Admin Methods

		public List<User> FilterUsers(DataSourceRequest filter, out int total) {
			IEnumerable<User> filterQuery = db.UserRepository.dbSet.OrderBy(u => u.UserId);
			total = filterQuery.Count();

			foreach(SortDescriptor sorter in filter.Sorts) {
				Func<User, object> exp = item => item.GetType().GetProperty(sorter.Member).GetValue(item, null);
				if(sorter.SortDirection == ListSortDirection.Ascending) {
					filterQuery = filterQuery.OrderBy(exp);
				} else {
					filterQuery = filterQuery.OrderByDescending(exp);
				}
			}

			int skip = filter.PageSize * (filter.Page - 1);
			return filterQuery.Skip(skip).Take(filter.PageSize).ToList();
		}

		#endregion
	}
}