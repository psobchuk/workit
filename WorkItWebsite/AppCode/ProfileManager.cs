﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Workit.Common;
using Workit.DataAccess;
using Workit.DataModel;
using WorkItWebsite.Models;

namespace WorkItWebsite {
	public class ProfileManager {
		private EntityProvider db;
		private UserManager um;

		public ProfileManager(EntityProvider entityProvider) {
			db = entityProvider;
			um = new UserManager(db);
		}

		public UserResponseStats GetUserResponseStats(long userId) {
			var user = um.GetUser(userId);
			var stats = new UserResponseStats();
			stats.GoodCount = db.ResponseRepository.Get(r => r.Owner.UserId == user.UserId && r.ResponseType == (int)ResponseType.Good).Count();
			stats.NeutralCount = db.ResponseRepository.Get(r => r.Owner.UserId == user.UserId && r.ResponseType == (int)ResponseType.Neutral).Count();
			stats.BadCount = db.ResponseRepository.Get(r => r.Owner.UserId == user.UserId && r.ResponseType == (int)ResponseType.Bad).Count();
			stats.TotalCount = db.ResponseRepository.Get(r => r.Owner.UserId == user.UserId).Count();

			return stats;
		}

		public int GetTaskCount(long userId, TaskUserType type) {
			var user = um.GetUser(userId);
			switch(type) {
				case TaskUserType.Owner:
					return db.OrderRepository.Get(u => u.Client.UserId == user.UserId).Count();
				case TaskUserType.Performer:
					return db.OrderRepository.Get(u => u.PerformerId == user.UserId).Count();
				default:
					return int.MinValue;
			}
		}

		public void SaveUserData(long userId, UserEditViewModel model) {
			User oldUser = um.GetUser(userId);
			
			//general user data
			var updatedUser = oldUser;
			if (!string.IsNullOrEmpty(model.FirstName)) {
				updatedUser.FirstName = model.FirstName;
			}

			if (!string.IsNullOrEmpty(model.LastName)) {
				updatedUser.LastName = model.LastName;
			}

			string phone = string.Empty;
			if (HttpContext.Current.Request.Form["Phone"] != null) {
				phone = HttpContext.Current.Request.Form["Phone"].ToString();
				updatedUser.Phones = phone.Replace(",", ", ");
			}

			updatedUser.Gender = model.Gender;
			updatedUser.BirthDate = model.BirthDate.SelectedDate;

			model.UserInfo.UserInfoId = updatedUser.UserInfo.UserInfoId;
			updatedUser.UserInfo = model.UserInfo;

			um.UpdateUser(oldUser, updatedUser);
		}

		public List<string> GetUserAlerts(User user) {
			var alerts = new List<string>();
			if (user.Status == (int)UserStatus.Pending) {
				alerts.Add("Ваша електронна адреса не підтверджена. Будь ласка, завершіть процедуру активації профілю.");
			}

			return alerts;
		}

		public UserEditViewModel GetUserEditModel(User user) {
			var vm = new UserEditViewModel(user);
			vm.UserAlerts = GetUserAlerts(user);

			return vm;
		}

		public ProfileViewModel GetProfileModel(User user, User currentUser) {
			try {
				var profile = new ProfileViewModel(user);
				var model = new UserViewModel(user);

				if(currentUser != null) {
					model.IsProfileOwner = IsProfileOwner(user, currentUser);
					if (model.IsProfileOwner) {
						model.OwnerTaskCount = GetTaskCount(user.UserId, TaskUserType.Owner);
						profile.UserAlerts = GetUserAlerts(user);
					}
				}
					model.ResponseStats = GetUserResponseStats(user.UserId);
					model.PerformerTaskCount = GetTaskCount(user.UserId, TaskUserType.Performer);
				

				profile.UserVM = model;

				return profile;

			} catch(Exception ex) {
				Logger.LogErrorException("Error displaying user", ex);
				throw;
			}
		}

		public bool IsProfileOwner(User user, User currUser) {
			return user.UserId == currUser.UserId;
		}
	}
}