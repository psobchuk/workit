﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Workit.Common;
using WorkItWebsite.Models;

namespace WorkItWebsite {
	public class ContentManager {
		private static string BASE_PHYSICAL_PATH = HttpContext.Current.Server.MapPath("~/Uploads/");
		private static string BASE_ABSOLUTE_PATH = "/Uploads/";
		public static string DEFAULT_PHOTO_PATH = "/Content/img/profile.png";

		/// <summary>
		/// Upload file to a filesystem
		/// </summary>
		/// <param name="userId">Id of user who uploaded the file</param>
		/// <param name="model">uploaded file model</param>
		/// <returns>Upload result true/false</returns>
		public static bool UploadFile(long userId, ImageUploadModel model) {
			//Allow null apload for default image
			if (CheckEmptyUpload(model)) return true;
			
			Bitmap original = null;
			var name = "newimagefile";
			switch((UploadFileType)model.UploadFileType) {
				case UploadFileType.RemoteUrl:
					var filename = GetUrlFileName(model.Url);
					if(!ValidateFileExtension(filename)) return false;

					name = Path.GetFileNameWithoutExtension(filename);
					original = GetImageFromUrl(model.Url);
					break;
				case UploadFileType.LocalFile:
					if(!ValidateFileExtension(model.ProfilePhoto.FileName)) return false;

					name = Path.GetFileNameWithoutExtension(model.ProfilePhoto.FileName);
					original = Bitmap.FromStream(model.ProfilePhoto.InputStream) as Bitmap;
					break;
				default:
					//if there was no matching filetype return false result
					Logger.LogError("Upload file: Unknown filetype");
					return false;
			}
			
			if (original != null) {
				if(!Directory.Exists(BASE_PHYSICAL_PATH + userId.ToString())) {
					Directory.CreateDirectory(BASE_PHYSICAL_PATH + userId.ToString());
				}

				try {
					var fn = HttpContext.Current.Server.MapPath("~/Uploads/" + userId.ToString() + "/avatar.png");
					if(File.Exists(fn)) File.Delete(fn);
					var img = CreateImage(original, model.X, model.Y, model.Width, model.Height);
					using (MemoryStream memory = new MemoryStream()) {
						using (FileStream fs = new FileStream(fn, FileMode.Create, FileAccess.ReadWrite)) {
							img.Save(memory, ImageFormat.Png);
							byte[] bytes = memory.ToArray();
							fs.Write(bytes, 0, bytes.Length);
						}
					}
				
					return true;
				} catch(Exception ex) {
					Logger.LogErrorException("Error uploading file", ex);
					throw;
				}
			}

			return false;
		}

		public static Task<bool> UploadFileAsync(long userId, ImageUploadModel model) {
			Task<bool> task = new Task<bool>(() => {
				return UploadFile(userId, model);
			});

			task.Start();
			return task;
		}

		/// <summary>
		/// Deletes specified file
		/// </summary>
		/// <param name="userId">User id which specifies foldername</param>
		/// <param name="filename">name of the file</param>
		public static void DeleteFile(long userId, string filename) {
			string directory = BASE_PHYSICAL_PATH + userId.ToString();
			if (Directory.Exists(directory)) {
				if(File.Exists(directory + "/" + filename)) {
					File.Delete(directory + "/" + filename);
				} else {
					Logger.LogDebug("DeleteFile: No such directory or file found");
				}

				if(Directory.GetFiles(directory).Length == 0) {
					Directory.Delete(directory);
				}
			}
		}

		public static string GetUserFilePath(long userId, string filename) {
			var filePath = string.Empty;
			if(File.Exists(BASE_PHYSICAL_PATH + userId.ToString() + "/" + filename)) {
				filePath = BASE_ABSOLUTE_PATH + userId.ToString() + "/" + filename;
			} else {
				//if searching for default user profile photo
				if(filename.Equals(Constants.PROFILE_PHOTO)) {
					filePath = Constants.DEFAULT_PHOTO_PATH;
				}
			}

			return filePath;
		}

		#region File Upload Helpers

		private static string GetUrlFileName(string url) {
			var parts = url.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
			var last = parts[parts.Length - 1];
			return last;
		}

		private static Bitmap GetImageFromUrl(string url) {
			var buffer = 1024;
			Bitmap image = null;

			if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
				return image;
			try {
				using (var ms = new MemoryStream()) {
					var req = WebRequest.Create(url);

					using (var resp = req.GetResponse()) {
						using (var stream = resp.GetResponseStream()) {
							var bytes = new byte[buffer];
							var n = 0;

							while ((n = stream.Read(bytes, 0, buffer)) != 0)
								ms.Write(bytes, 0, n);
						}
					}

					image = Bitmap.FromStream(ms) as Bitmap;
				}
			} catch(Exception ex) {
				Logger.LogErrorException("Error uploading image from remote url", ex);
				throw;
			}

			return image;
		}

		private static Bitmap CreateImage(Bitmap original, int x, int y, int width, int height) {
			var img = new Bitmap(width, height);
			try {
				using (var g = Graphics.FromImage(img)) {
					g.SmoothingMode = SmoothingMode.AntiAlias;
					g.InterpolationMode = InterpolationMode.HighQualityBicubic;
					g.DrawImage(original, new Rectangle(0, 0, width, height), x, y, width, height, GraphicsUnit.Pixel);
				}

			} catch(Exception ex) {
				Logger.LogErrorException("Error creating image", ex);
				throw;
			}

			return img;
		}

		public static bool ValidateFileExtension(string filename) {
			var allowed = new List<string> { ".jpg", ".jpeg", ".png" };
			string extension = Path.GetExtension(filename);
			
			return allowed.Contains(extension);
		}

		public static bool CheckEmptyUpload(ImageUploadModel model) {
			return (string.IsNullOrEmpty(model.Url) || model.Url.Equals(DEFAULT_PHOTO_PATH)) && model.ProfilePhoto != null && model.ProfilePhoto.ContentLength == 0;
		}

		public static string GetUrl(string action, string controller, object routeValues) {
			var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
			return urlHelper.Action(action, controller, routeValues);
		}

		#endregion
	}
}