﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Workit.DataAccess;
using Workit.DataModel;

namespace WorkItWebsite {
	public class PaymentManager {
		private EntityProvider db;

		public PaymentManager(EntityProvider provider) {
			db = provider;
		}

		public bool CheckBalance(long userId, long amount) {
			return amount <= ( AvailableBalance(userId) - GetLockedMoneyAmount(userId));
		}

		private bool CheckWithoutLocked(long userId, long amount) {
			return AvailableBalance(userId) - amount >= 0;
		}

		public long GetLockedMoneyAmount(long userId) {
			UserInfo info = GetUserInfo(userId);
			return info.LockedBalance;
		}

		public long AvailableBalance(long userId) {
			UserInfo info = GetUserInfo(userId);
			return info.PaymentBalance;
		}

		public void LockMoney(long userId, long amount) {
			UserInfo info = GetUserInfo(userId);
			var newInfo = info;
			newInfo.LockedBalance += amount;
			db.UserInfoRepository.Update(info, newInfo);
			db.SaveChanges();
		}

		public void ProcessPayment(long fromUserId, long toUserId, long amount) {
			if(CheckWithoutLocked(fromUserId, amount)) {
				UserInfo fromInfo = GetUserInfo(fromUserId);
				var newInfo = fromInfo;
				fromInfo.PaymentBalance -= amount;
				fromInfo.LockedBalance -= amount;
				db.UserInfoRepository.Update(fromInfo, newInfo);
				db.SaveChanges();

				UserInfo toInfo = GetUserInfo(toUserId);
				newInfo = new UserInfo();
				newInfo = toInfo;

				toInfo.PaymentBalance += amount;
				db.UserInfoRepository.Update(toInfo, newInfo);
				db.SaveChanges();
			}
		}

		private UserInfo GetUserInfo(long userId) {
			return db.UserInfoRepository.Get(u => u.UserInfoId == userId).SingleOrDefault();
		}
	}
}