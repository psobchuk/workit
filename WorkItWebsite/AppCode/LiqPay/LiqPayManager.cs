﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Xml;

namespace WorkItWebsite {
	public class LiqPayManager {
		public static string Merch_id = "i3534815183";
		public static string Merch_sign = "MDKj6tlW6srxyJHWF7v1R2jj6kNOtWlVZbAZ";

		public static XmlDocument GetXmlDocument(
			decimal sum,
			string currency,
			string url_CSuccess,
			string url_PSuccess,
			string order_id,
			string description,
			string default_phone,
			string pay_way,
			int goods_id
			) {
			return GetXmlDocument(
				Merch_id,
				sum,
				currency,
				url_CSuccess,
				url_PSuccess,
				order_id,
				description,
				default_phone,
				pay_way,
				goods_id
				);
		}

		public static XmlDocument GetXmlDocument(
			string merch_id,
			decimal sum,
			string currency,
			string url_CSuccess,
			string url_PSuccess,
			string order_id,
			string description,
			string default_phone,
			string pay_way,
			int goods_id
			) {
			XmlDocument doc = new XmlDocument();
			XmlNode item;
			XmlNode request = doc.CreateNode(XmlNodeType.Element, "request", "");
			doc.AppendChild(request);

			item = doc.CreateNode(XmlNodeType.Element, "version", "");
			item.InnerText = "1.2";
			request.AppendChild(item);

			item = doc.CreateNode(XmlNodeType.Element, "merchant_id", "");
			item.InnerText = merch_id;
			request.AppendChild(item);

			item = doc.CreateNode(XmlNodeType.Element, "result_url", "");
			item.InnerText = url_CSuccess;
			request.AppendChild(item);

			item = doc.CreateNode(XmlNodeType.Element, "server_url", "");
			item.InnerText = url_PSuccess;
			request.AppendChild(item);

			item = doc.CreateNode(XmlNodeType.Element, "order_id", "");
			item.InnerText = order_id;
			request.AppendChild(item);

			item = doc.CreateNode(XmlNodeType.Element, "amount", "");
			item.InnerText = sum.ToString().Replace(",", ".");
			request.AppendChild(item);

			item = doc.CreateNode(XmlNodeType.Element, "currency", "");
			item.InnerText = currency;
			request.AppendChild(item);

			item = doc.CreateNode(XmlNodeType.Element, "description", "");
			item.InnerText = description;
			request.AppendChild(item);

			item = doc.CreateNode(XmlNodeType.Element, "default_phone", "");
			item.InnerText = default_phone;
			request.AppendChild(item);

			item = doc.CreateNode(XmlNodeType.Element, "pay_way", "");
			item.InnerText = pay_way;
			request.AppendChild(item);

			item = doc.CreateNode(XmlNodeType.Element, "goods_id", "");
			item.InnerText = goods_id.ToString();
			request.AppendChild(item);

			return doc;
		}


		public static string GetOperation_xml(XmlDocument doc) {
			return Convert.ToBase64String(Encoding.ASCII.GetBytes(doc.InnerXml));
		}

		public static string GetSignature(XmlDocument doc) {
			return GetSignature(doc, Merch_sign);
		}

		public static string GetSignature(string doc) {
			return GetSignature(doc, Merch_sign);
		}

		public static string GetSignature(XmlDocument doc, string merch_sign) {
			return GetSignature(doc.InnerXml, merch_sign);
		}

		public static string GetSignature(string doc, string merch_sign) {
			string id = merch_sign + doc + merch_sign;

			SHA1CryptoServiceProvider cryptoTransformSHA1 = new SHA1CryptoServiceProvider();
			byte[] b = cryptoTransformSHA1.ComputeHash(Encoding.UTF8.GetBytes(id));
			string hash = Convert.ToBase64String(b);

			return hash;
		}

		public static string GetOperationXmlFromAnswer(string operation_xml) {
			return Encoding.ASCII.GetString(Convert.FromBase64String(operation_xml));
		}

		public static string GetSignFromAnswer(string operation_xml) {
			return GetSignature(Encoding.ASCII.GetString(Convert.FromBase64String(operation_xml)));
		}

		public static XmlDocument LoadDocument(decimal amount) {
			XmlDocument doc = LiqPayManager.GetXmlDocument(amount,
				"UAH",
				"http:/local.workit.com.ua/payment/clientsuccess",
				"http:/local.workit.com.ua/payment/serversuccess",
				Guid.NewGuid().ToString(),
				"Payment Operation",
				"+380502942525",
				"card",
				11111);

			return doc;
		}

		public static PaymentResponse GetPaymentResponse(string operation_xml, string signature, long userId) {
			var response = new PaymentResponse();
			
			string xmlString = GetOperationXmlFromAnswer(operation_xml);
			string sign = LiqPayManager.GetSignFromAnswer(signature);
			//if(sign.Equals(signature)) {
				var doc = new XmlDocument();
				doc.LoadXml(xmlString);

				XmlNode node = doc.GetElementsByTagName("amount")[0];
				response.Amount = int.Parse(node.InnerText.Split('.')[0]);
				
				node = doc.GetElementsByTagName("transaction_id")[0];
				response.TransactionId = node.InnerText;
				
				node = doc.GetElementsByTagName("order_id")[0];
				response.OrderId =  node.InnerText;

				response.Description = "Поповнення рахунку";
				response.Created = DateTime.Now;
				response.UserId = userId;
			//}
			
			return response;
		}
	}
}