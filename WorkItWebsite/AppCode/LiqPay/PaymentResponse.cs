﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkItWebsite {
	public class PaymentResponse {
		public string OrderId { get; set; }
		public int Amount { get; set; }
		public string Description { get; set; }
		public string TransactionId { get; set; }
		public DateTime Created { get; set; }
		public long UserId { get; set; }
	}
}