﻿using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Security;
using Workit.Common;
using Workit.DataAccess;
using Workit.DataModel;
using WorkItWebsite.Helpers;
using WorkItWebsite.Models;

namespace WorkItWebsite {
	public class AuthenticationManager {
		#region Internal Authentication

		public static bool Login(string email, string password, bool rememberMe = false) {
			var db = new EntityProvider();
			User user = db.UserRepository.Get(u => u.Email == email).SingleOrDefault();

			if (user != null && user.Status != (int)UserStatus.Deleted && VerifyUserPassword(user.Password, password)) {
				SendAuthenticationCookie(user, db, rememberMe);
				HttpContext.Current.Session[Constants.CURRENT_USER] = user;
				return true;
			}

			return false;
		}

		public static void Logout() {
			FormsAuthentication.SignOut();
			HttpContext.Current.Session[Constants.CURRENT_USER] = null;
		}

		public static long Register(RegisterModel model) {
			var db = new EntityProvider();
			var user = new User {
				Email = model.Email,
				Password = CryptoHelper.MD5Hash(model.Password),
				FirstName = model.Name,
				LastName = model.Surname,
				Created = DateTime.UtcNow,
				Roles = new List<Role>(),
				Status = (int)UserStatus.Pending,
				ActivationKey = model.ActivationKey,
				Gender = model.Gender,
				BirthDate = model.BirthDate.SelectedDate,
				Phones = model.Phone
			};

			Role role = db.RoleRepository.Get(r => r.RoleName == model.RoleId).SingleOrDefault();
			user.Roles.Add(role);

			user = db.UserRepository.Insert(user);
			db.SaveChanges();

			return user.UserId;
		}

		public static void ProcessAuthenticationCookie() {
			HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
			if (authCookie != null) {
				FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
				SystemIdentity identity = new SystemIdentity(ticket.Name);

				string[] roleNames = ticket.UserData.Split(',');
				SystemUser user = new SystemUser(identity, roleNames);
				HttpContext.Current.User = user;
			}
		}

		public static bool ActivateUser(string activationKey) {
			var db = new EntityProvider();
			User user = db.UserRepository.Get(u => u.ActivationKey == activationKey).SingleOrDefault();

			if(user != null) {
				User newUser = user;
				newUser.Status = (int)UserStatus.Active;
				newUser.ActivationKey = string.Empty;
				db.UserRepository.Update(user, newUser);
				db.SaveChanges();
				var manager = new UserManager(db);
				manager.DisposeCurrentUser();

				if (string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name)) {
					SendAuthenticationCookie(user, db, false);
				}
			}

			return user != null;
		}

		public static void RollbackRegistration(long userId) {
			var db = new EntityProvider();
			ContentManager.DeleteFile(userId, Constants.PROFILE_PHOTO);
			
			var info = db.ExternalUserInfoRepository.Get(i => i.User.UserId == userId).SingleOrDefault();
			db.ExternalUserInfoRepository.Delete(info.ExternalUserInfoId);
			var histories = db.PaymentRepository.Get(i => i.Owner.UserId == userId).ToList();
			foreach(var item in histories) {
				db.PaymentRepository.Delete(item.PaymentHistoryId);
			}
			db.UserInfoRepository.Delete(userId);
			db.UserSettingsRepository.Delete(userId);
			db.UserRepository.Delete(userId);
			db.SaveChanges();
		}

		#endregion

		#region OAuth methods

		public static ExternalUserInfo OAuthLogin(string providerName, string providerUserId) {
			var db = new EntityProvider();
			ExternalUserInfo userInfo = db.ExternalUserInfoRepository.Get(i =>
					(i.ProviderUserId.Equals(providerUserId) && i.ProviderName.Equals(providerName))).FirstOrDefault();

			return userInfo;
		}

		public static void AssociateAccount(string providerName, string providerUserId, string email) {
			var db = new EntityProvider();
			var userInfo = new ExternalUserInfo();
			userInfo.ProviderName = providerName;
			userInfo.ProviderUserId = providerUserId;

			User user = db.UserRepository.Get(u => u.Email == email).FirstOrDefault();
			if (user != null) {
				userInfo.User = user;
			}

			db.SaveChanges();
		}

		public static long OAuthRegister(RegisterExternalLoginModel registerModel, SystemRoles systemRole) {
			var db = new EntityProvider();
			var user = new User {
				Email = registerModel.Email,
				FirstName = registerModel.FirstName,
				LastName = registerModel.LastName,
				Created = DateTime.UtcNow,
				Roles = new List<Role>(),
				Status = (int)UserStatus.Pending,
				ExternalLogins = new List<ExternalUserInfo>(),
				ActivationKey = registerModel.ActivationKey,
				Gender = registerModel.Gender,
				BirthDate = registerModel.BirthDate.SelectedDate
			};

			string rolename = systemRole.ToString();
			Role role = db.RoleRepository.Get(r => r.RoleName == rolename).SingleOrDefault();
			user.Roles.Add(role);

			var externalUserInfo = new ExternalUserInfo();
			externalUserInfo.Email = registerModel.Email;
			externalUserInfo.AvatarImageLink = registerModel.ImageUploadData.Url;
			externalUserInfo.ProviderName = registerModel.ProviderName;
			externalUserInfo.ProviderUserId = registerModel.ProviderUserId;

			externalUserInfo.User = user;
			user.ExternalLogins.Add(externalUserInfo);

			db.UserRepository.Insert(user);
			db.ExternalUserInfoRepository.Insert(externalUserInfo);

			db.SaveChanges();

			user = db.UserRepository.Get(u => u.Email == registerModel.Email).SingleOrDefault();

			return user.UserId;
		}

		public static void UpdateOAuthAccount(AuthenticationResult data) {
			var db = new EntityProvider();
			var userInfo = db.ExternalUserInfoRepository.Get(i => i.ProviderUserId == data.ProviderUserId).SingleOrDefault();

			var newUserInfo = new ExternalUserInfo {
				ProviderUserId = data.ProviderUserId,
				ProviderName = data.Provider,
				AvatarImageLink = data.ExtraData["photo"],
				Email = userInfo.Email,
				ExternalUserInfoId = userInfo.ExternalUserInfoId,
			};

			if (userInfo != null) {
				db.ExternalUserInfoRepository.Update(userInfo, newUserInfo);
				db.SaveChanges();
			}
		}

		public static void RegisterExternalUser(ExternalUserInfo info, bool persistentCookie = false) {
			var db = new EntityProvider();
			SendAuthenticationCookie(info.User, db, true);
			HttpContext.Current.Session[Constants.CURRENT_USER] = info.User;
		}

		public static RegisterExternalLoginModel PopulateExternalLoginData(AuthenticationResult data, CultureInfo info) {
			var registerModel = new RegisterExternalLoginModel() {
				FirstName = data.ExtraData["firstname"].ToString(),
				LastName = data.ExtraData["lastname"].ToString(),
				ProviderName = data.Provider,
				ProviderUserId = data.ProviderUserId,
				Gender = int.Parse(data.ExtraData["sex"])
			};

			if(!string.IsNullOrEmpty(data.ExtraData["email"])) {
				registerModel.Email = data.ExtraData["email"];
			}

			if(!string.IsNullOrEmpty(data.ExtraData["bdate"])) {
				var date = new DateTime();
				if(!DateTime.TryParse(data.ExtraData["bdate"], out date)) {
					date = DateTime.Parse(data.ExtraData["bdate"], new CultureInfo("en-US"));
				}
				
				registerModel.BirthDate = new BirthDateModel(date);
			} else {
				registerModel.BirthDate = new BirthDateModel();
			}

			if(!string.IsNullOrEmpty(data.ExtraData["photo"])) {
				registerModel.ImageUploadData = new ImageUploadModel();
				registerModel.ImageUploadData.Url = data.ExtraData["photo"].ToString();
			}

			return registerModel;
		}

		#endregion

		#region Password recovery

		public static User VerifyUserEmail(string email) {
			var db = new EntityProvider();
			var user = db.UserRepository.Get(u => u.Email == email).SingleOrDefault();
			return user;
		}

		public static User ProcessPasswordRecoveryRequest(string email) {
			var db = new EntityProvider();
			var guid = string.Empty;
			User user = db.UserRepository.Get(u => u.Email == email).SingleOrDefault();
			if (user != null) {
				User newUser = user;
				guid = Guid.NewGuid().ToString();
				newUser.RecoveryKey = guid;

				db.UserRepository.Update(user, newUser);
				db.SaveChanges();
			}

			return user;
		}

		public static bool VerifyPasswordRecoveryRequest(string secret) {
			var db = new EntityProvider();
			User user = db.UserRepository.Get(u => u.RecoveryKey == secret).SingleOrDefault();
			return user != null;
		}

		public static long ResetPassword(string guid, string password) {
			var db = new EntityProvider();
			var user = db.UserRepository.Get(u => u.RecoveryKey == guid).SingleOrDefault();
			User newUser = user;
			newUser.Password = CryptoHelper.MD5Hash(password);
			newUser.RecoveryKey = string.Empty;
			db.UserRepository.Update(user, newUser);
			db.SaveChanges();
			SendAuthenticationCookie(user, db, false);
			return user.UserId;
		}

		public static void ProcessAccountRecovery(string email) {
			var db = new EntityProvider();
			var user = db.UserRepository.Get(u => u.Email == email).SingleOrDefault();
			User newUser = user;

			var guid = Guid.NewGuid().ToString();
			newUser.RecoveryKey = guid;
			db.UserRepository.Update(user, newUser);
			db.SaveChanges();

			var message = new RecoverAccountMessage(email, guid);
			MailSender.SendMail(message);
		}

		#endregion

		public static User RecoverUser(string guid) {
			var db = new EntityProvider();
			User user = db.UserRepository.Get( u => u.RecoveryKey == guid).SingleOrDefault();
			if(user != null) {
				var newUser = user;
				newUser.RecoveryKey = string.Empty;
				newUser.Status = (int)UserStatus.Active;
				db.UserRepository.Update(user, newUser);
				db.SaveChanges();
			}

			return user;
		}

		#region Helpers

		public static bool VerifyUserPassword(string userPassword, string password) {
			string md5Password = CryptoHelper.MD5Hash(password);
			return string.Equals(userPassword, md5Password);
		}

		private static void SendAuthenticationCookie(User user, EntityProvider db, bool persistentCookie = false) {
			List<Role> roles = db.UserRepository.GetUserRoles(user.UserId);
			string[] roleNames = roles.Select(r => r.RoleName.ToString()).ToArray();

			Thread.CurrentPrincipal = new SystemUser(new SystemIdentity(user.Email), roleNames);

			var expires = DateTime.Now.AddMinutes(30);
			if(persistentCookie) {
				expires = DateTime.Now.AddYears(1);
			}

			FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, user.Email, DateTime.Now, expires, persistentCookie, string.Join(",",roleNames));
			string authTicket = FormsAuthentication.Encrypt(ticket);
			HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, authTicket);
			cookie.Expires = ticket.Expiration;
			HttpContext.Current.Response.Cookies.Add(cookie);
		}

		#endregion
	}
}