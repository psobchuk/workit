﻿using DotNetOpenAuth.AspNet.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using Workit.Common;

namespace WorkItWebsite {
	public static class VkAuthHelpers {
		static string[] UriRfc3986CharsToEscape = new string[] { "!", "*", "'", "(", ")" };

		public static IDictionary<string, string> ParseJSON(this string str) {
			return null;
		}

		public static void AppendQueryArgs(this UriBuilder builder, IEnumerable<KeyValuePair<string, string>> args) {
			if (builder == null) throw new ArgumentNullException("builder");
			if ((args != null) && (args.Count<KeyValuePair<string, string>>() > 0)) {
				StringBuilder builder2 = new StringBuilder(50 + (args.Count<KeyValuePair<string, string>>() * 10));
				if (!string.IsNullOrEmpty(builder.Query)) {
					builder2.Append(builder.Query.Substring(1));
					builder2.Append('&');
				}
				builder2.Append(CreateQueryString(args));
				builder.Query = builder2.ToString();
			}
		}

		public static string CreateQueryString(IEnumerable<KeyValuePair<string, string>> args) {
			if (args == null) throw new ArgumentNullException("args");
			if (!args.Any<KeyValuePair<string, string>>()) {
				return string.Empty;
			}
			StringBuilder builder = new StringBuilder(args.Count<KeyValuePair<string, string>>() * 10);
			foreach (KeyValuePair<string, string> pair in args) {
				if (string.IsNullOrEmpty(pair.Key))
					throw new ArgumentException("UnexpectedNullOrEmptyKey");
				if (pair.Value == null)
					throw new ArgumentException("UnexpectedNullValue");
				builder.Append(EscapeUriDataStringRfc3986(pair.Key));
				builder.Append('=');
				builder.Append(EscapeUriDataStringRfc3986(pair.Value));
				builder.Append('&');
			}
			builder.Length--;
			return builder.ToString();
		}

		public static string EscapeUriDataStringRfc3986(string value) {
			if (value == null) throw new ArgumentNullException("value");
			StringBuilder builder = new StringBuilder(Uri.EscapeDataString(value));
			for (int i = 0; i < UriRfc3986CharsToEscape.Length; i++) {
				builder.Replace(UriRfc3986CharsToEscape[i], Uri.HexEscape(UriRfc3986CharsToEscape[i][0]));
			}
			return builder.ToString();
		}

		public static void AddItemIfNotEmpty(this IDictionary<string, string> dictionary, string key, string value) {
			if (key == null) {
				throw new ArgumentNullException("key");
			}
			if (!string.IsNullOrEmpty(value)) {
				dictionary[key] = value;
			}
		}
	}

	public class VKontakteClient : OAuth2Client {
		#region Constants
		
		private const string AUTHORIZE_URL = "https://oauth.vk.com/authorize";
		private const string ACCESS_TOKEN_URL = "https://oauth.vk.com/access_token";
		private const string USER_GET_URL = "https://api.vk.com/method/users.get";
		private const string VK_API_VERSION = "5.5";
		private const string VK_SESSION_TOKEN = "VkontakteApiToken";

		#endregion
		
		private string appId;
		private string appSecret;

		public VKontakteClient(string appId, string appSecret): base("vkontakte") {
			this.appId = appId;
			this.appSecret = appSecret;
		}

		protected override Uri GetServiceLoginUrl(Uri returnUrl) {
			UriBuilder builder = new UriBuilder(AUTHORIZE_URL);
			var args = new Dictionary<string, string>() { 
				{"client_id", appId},
				{"redirect_uri", NormalizeHexEncoding(returnUrl.AbsoluteUri)},
				{"display", "page"},
				{"response_type", "code"},
				{"scope", "photos" },
				{"v", VK_API_VERSION }
			};
			builder.AppendQueryArgs(args);

			return builder.Uri;
		}

		protected override IDictionary<string, string> GetUserData(string accessToken) {
			var data = HttpContext.Current.Session[VK_SESSION_TOKEN] as VKontakteTokenResponse;
			if (data == null || data.AccessToken != accessToken) {
				Logger.LogError("Incorrect Vkontakte token");
				return null;
			}

			UriBuilder builder = new UriBuilder(USER_GET_URL);
			var args = new Dictionary<string, string> {
				{ "uids", data.UserId.ToString() },
				{ "fields", "uid,first_name,last_name,nickname,bdate,sex,photo_200_orig" },
				{ "access_token", accessToken }
			};
			builder.AppendQueryArgs(args);

			var res = new Dictionary<string, string>();
			try {
				using (WebClient client = new WebClient()) {
					using (var stream = client.OpenRead(builder.Uri)) {
						var converter = new SocialResponseConverter(stream, SocialResponseType.Vkontakte);
						res = converter.ConvertResponse();
					}
				}
			} catch(WebException ex) {
				Logger.LogErrorException("Could not read vkontakte data", ex);
			} catch (Exception ex) {
				Logger.LogErrorException("Unexpected error while getting Vkontakte Data", ex);
			}

			return res;
		}

		protected override string QueryAccessToken(Uri returnUrl, string authorizationCode) {
			UriBuilder builder = new UriBuilder(ACCESS_TOKEN_URL);
			var args = new Dictionary<string, string> {
				{ "client_id", this.appId },
				{ "redirect_uri", NormalizeHexEncoding(returnUrl.AbsoluteUri) },
				{ "client_secret", this.appSecret },
				{ "code", authorizationCode },
				{ "display", "popup" }
			};
			builder.AppendQueryArgs(args);

			try {
				using (WebClient client = new WebClient()) {
					using (var stream = client.OpenRead(builder.Uri)) {
						DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(VKontakteTokenResponse));
						var data = (VKontakteTokenResponse)serializer.ReadObject(stream);
						HttpContext.Current.Session[VK_SESSION_TOKEN] = data;
						return data.AccessToken;
					}
				}
			} catch (Exception ex) {
				Logger.LogErrorException("Could not receive vkontakte token", ex);
				return string.Empty;
			}
		}

		private static string NormalizeHexEncoding(string url) {
			char[] chArray = url.ToCharArray();
			for (int i = 0; i < (chArray.Length - 2); i++) {
				if (chArray[i] == '%') {
					chArray[i + 1] = char.ToUpperInvariant(chArray[i + 1]);
					chArray[i + 2] = char.ToUpperInvariant(chArray[i + 2]);
					i += 2;
				}
			}

			return new string(chArray);
		}
	}

	#region Serializable classes

	[DataContract, EditorBrowsable(EditorBrowsableState.Never)]
	internal class VKontakteTokenResponse {
		[DataMember(Name = "access_token")]
		public string AccessToken { get; set; }
		[DataMember(Name = "user_id")]
		public long UserId { get; set; }
		[DataMember(Name = "expires_in")]
		public long ExpiresIn { get; set; }
	}

	#endregion
}