﻿using DotNetOpenAuth.AspNet.Clients;
using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;
using DotNetOpenAuth.OpenId.RelyingParty;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;
using System.Linq;
using Workit.Common;

namespace WorkItWebsite {
	public class GooglePlusClient : OAuth2Client {
		/// <summary>
		/// The authorization endpoint.
		/// 
		/// </summary>
		private const string AuthorizationEndpoint = "https://accounts.google.com/o/oauth2/auth";

		/// <summary>
		/// The token endpoint.
		/// 
		/// </summary>
		private const string TokenEndpoint = "https://accounts.google.com/o/oauth2/token";

		/// <summary>
		/// The user info endpoint.
		/// 
		/// </summary>
		private const string UserInfoEndpoint = "https://www.googleapis.com/plus/v1/people/me";

		/// <summary>
		/// The client id.
		/// 
		/// </summary>
		private readonly string clientId;

		/// <summary>
		/// The client secret.
		/// 
		/// </summary>
		private readonly string clientSecret;

		public GooglePlusClient(string clientId, string clientSecret)
			: base("GooglePlus") {
			if (clientId == null)
				throw new ArgumentNullException("clientId");
			if (clientSecret == null)
				throw new ArgumentNullException("clientSecret");

			this.clientId = clientId;
			this.clientSecret = clientSecret;
		}

		protected override Uri GetServiceLoginUrl(Uri returnUrl) {
			UriBuilder uriBuilder = new UriBuilder(AuthorizationEndpoint);
			var query = new Dictionary<string, string> {
				{ "client_id", this.clientId },
				{ "redirect_uri", returnUrl.GetLeftPart(UriPartial.Path) },
				{ "response_type", "code" },
				{ "scope", "https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.profile email" },
				{ "state", returnUrl.Query.Substring(1) }
			};
			uriBuilder.AppendQueryArgs(query);

			return uriBuilder.Uri;
		}

		protected override IDictionary<string, string> GetUserData(string accessToken) {
			UriBuilder uriBuilder = new UriBuilder(UserInfoEndpoint);
			var query = HttpUtility.ParseQueryString(uriBuilder.Query);
			query["access_token"] = accessToken;

			WebRequest webRequest = WebRequest.Create(uriBuilder.Uri + "?access_token=" + accessToken);
			try {
				using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse()) {
					if (webResponse.StatusCode == HttpStatusCode.OK) {
						using (var responseStream = webResponse.GetResponseStream()) {
							if (responseStream == null) {
								Logger.LogDebug("Response stream is null");
								return null;
							}
							
							var converter = new SocialResponseConverter(responseStream, SocialResponseType.GooglePlus);
							return converter.ConvertResponse();
						}
					}
				}
			} catch (WebException exception) {
				string responseText;
				using(var reader = new StreamReader(exception.Response.GetResponseStream())) {
					responseText = reader.ReadToEnd();
					Logger.LogErrorException(responseText, exception);
				}
			}

			return null;
		}

		protected override string QueryAccessToken(Uri returnUrl, string authorizationCode) {
			// Build up the form post data
			Dictionary<string, string> values = new Dictionary<string, string>();
			values.Add("code", authorizationCode);
			values.Add("client_id", this.clientId);
			values.Add("client_secret", this.clientSecret);
			values.Add("redirect_uri", returnUrl.GetLeftPart(UriPartial.Path));
			values.Add("grant_type", "authorization_code");
			string postData = String.Join("&",
				values.Select(x => Uri.EscapeDataString(x.Key) + "=" + Uri.EscapeDataString(x.Value))
					  .ToArray());

			// Build up the request
			WebRequest webRequest = WebRequest.Create(TokenEndpoint);
			webRequest.ContentType = "application/x-www-form-urlencoded";
			webRequest.ContentLength = postData.Length;
			webRequest.Method = "POST";
			using (Stream requestStream = webRequest.GetRequestStream()) {
				StreamWriter streamWriter = new StreamWriter(requestStream);
				streamWriter.Write(postData);
				streamWriter.Flush();
			}

			// Process the response
			using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse()) {
				if (webResponse.StatusCode == HttpStatusCode.OK) {
					using (Stream responseStream = webResponse.GetResponseStream()) {
						StreamReader streamReader = new StreamReader(responseStream);

						dynamic response = JsonConvert.DeserializeObject<dynamic>(streamReader.ReadToEnd());
						return (string)response.access_token;
					}
				}
			}

			return null;
		}

		public static void RewriteRequest() {
			var ctx = HttpContext.Current;

			var stateString = HttpUtility.UrlDecode(ctx.Request.QueryString["state"]);
			if (stateString == null || !stateString.Contains("__provider__=GooglePlus"))
				return;

			var q = HttpUtility.ParseQueryString(stateString);
			q.Add(ctx.Request.QueryString);
			q.Remove("state");

			ctx.RewritePath(ctx.Request.Path + "?" + q);
		}
	}
}