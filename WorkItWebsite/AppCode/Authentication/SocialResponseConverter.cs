﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Workit.Common;

namespace WorkItWebsite {
	public class SocialResponseConverter {
		private SocialResponseType type;
		private StreamReader reader;
		private Dictionary<string, object> responseDictionary;

		public SocialResponseConverter(Stream responseStream, SocialResponseType responseType) {
			reader = new StreamReader(responseStream);
			responseDictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(reader.ReadToEnd());
			type = responseType;
		}
		
		public Dictionary<string, string> ConvertResponse() {
			var result = new Dictionary<string, string>();
			switch(type) {
				case SocialResponseType.Vkontakte:
					result = ConvertVkResponse();
					break;
				case SocialResponseType.Facebook:
					result = ConvertFacebookResponse();
					break;
				case SocialResponseType.GooglePlus:
					result = ConvertGoogleResponse();
					break;
			}

			return result;
		}

		private Dictionary<string, string> ConvertVkResponse() {
			var result = CreateResultDictionary();
			JArray arr = (JArray)responseDictionary["response"];

			result["id"] = GetObjectResult(arr[0]["uid"]);
			result["firstname"] = GetObjectResult(arr[0]["first_name"]);
			result["lastname"] = GetObjectResult(arr[0]["last_name"]);
			result["sex"] = GetObjectResult(arr[0]["sex"]);
			result["bdate"] = GetObjectResult(arr[0]["bdate"]);
			result["photo"] = GetObjectResult(arr[0]["photo_200_orig"]);

			return result;
		}

		private Dictionary<string, string> ConvertGoogleResponse() {
			var result = new Dictionary<string, string>();

			result["id"] = GetObjectResult("id");
			
			if (responseDictionary["name"] != null) {
				dynamic name = responseDictionary["name"];
				result["firstname"] = GetObjectResult(name.givenName);
				result["lastname"] = GetObjectResult(name.familyName);
			}

			var gender = responseDictionary["gender"];
			if(gender != null) {
				if(gender.ToString() == "male") {
					result["sex"] = "2";
				} else {
					result["sex"] = "1";
				}
			}

			if(responseDictionary["image"] != null) {
				dynamic image = responseDictionary["image"];
				result["photo"] = GetObjectResult(image.url);
			}

			if(responseDictionary["emails"] != null) {
				dynamic emails = responseDictionary["emails"];
				result["email"] = GetObjectResult((object)emails[0].value);
			}

			result.Add("bdate", string.Empty);

			return result;
		}

		private Dictionary<string, string> ConvertFacebookResponse() {
			var result = CreateResultDictionary();

			result["id"] = GetObjectResult(responseDictionary["id"]);
			result["firstname"] = GetObjectResult(responseDictionary["first_name"]);
			result["lastname"] = GetObjectResult(responseDictionary["last_name"]);
			result["email"] = GetObjectResult(responseDictionary["email"]);
			result["bdate"] = GetObjectResult(responseDictionary["birthday"]);

			var gender = responseDictionary["gender"];
			if (gender != null) {
				if (gender.ToString() == "male") {
					result["sex"] = "2";
				} else {
					result["sex"] = "1";
				}
			}

			dynamic picture = responseDictionary["picture"];
			if(picture != null) {
				result["photo"] = picture.data.url;
			}

			return result;
		}

		private string GetObjectResult(string key) {
			string result = "";
			if(responseDictionary[key] != null) {
				result = responseDictionary[key].ToString();
			}

			return result;
		}
		
		private string GetObjectResult(object item) {
			string result = "";
			if(item != null) {
				result = item.ToString();
			}

			return result;
		}

		private Dictionary<string, string> CreateResultDictionary() {
			var result = new Dictionary<string, string>();
			result.Add("id", "");
			result.Add("firstname", "");
			result.Add("lastname", "");
			result.Add("sex", "");
			result.Add("bdate", "");
			result.Add("photo", "");
			result.Add("email", "");

			return result;
		}
	}
}