﻿using DotNetOpenAuth.AspNet;
using DotNetOpenAuth.AspNet.Clients;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using Workit.Common;

namespace WorkItWebsite {
	public class FacebookClient: OAuth2Client {
		private const string AuthorizationEndpoint = "https://www.facebook.com/dialog/oauth";

		private const string TokenEndpoint = "https://graph.facebook.com/oauth/access_token";

		private const string UserInfoEndpoint = "https://graph.facebook.com/me?fields=id,first_name,last_name,email,birthday,picture.type(large),gender";

		private readonly string clientId;

		private readonly string clientSecret;

		public FacebookClient(string clientId, string clientSecret)
			: base("Facebook") {
			if (clientId == null)
				throw new ArgumentNullException("clientId");
			if (clientSecret == null)
				throw new ArgumentNullException("clientSecret");

			this.clientId = clientId;
			this.clientSecret = clientSecret;
		}

		protected override Uri GetServiceLoginUrl(Uri returnUrl) {
			UriBuilder uriBuilder = new UriBuilder(AuthorizationEndpoint);
			var query = new Dictionary<string, string> {
				{ "client_id", this.clientId },
				{ "redirect_uri", returnUrl.GetLeftPart(UriPartial.Path) },
				{ "response_type", "code" },
				{ "scope", "email,user_photos" },
				{ "state", returnUrl.Query.Substring(1) }
			};
			uriBuilder.AppendQueryArgs(query);

			return uriBuilder.Uri;
		}

		protected override IDictionary<string, string> GetUserData(string accessToken) {
			UriBuilder uriBuilder = new UriBuilder(UserInfoEndpoint);

			WebRequest webRequest = WebRequest.Create(uriBuilder.Uri + "&access_token=" + accessToken);
			try {
				using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse()) {
					if (webResponse.StatusCode == HttpStatusCode.OK) {
						using (var responseStream = webResponse.GetResponseStream()) {
							if (responseStream == null) {
								Logger.LogDebug("Response stream is null");
								return null;
							}

							var converter = new SocialResponseConverter(responseStream, SocialResponseType.Facebook);
							return converter.ConvertResponse();
						}
					}
				}
			} catch (WebException exception) {
				string responseText;
				using (var reader = new StreamReader(exception.Response.GetResponseStream())) {
					responseText = reader.ReadToEnd();
					Logger.LogErrorException(responseText, exception);
				}
			}

			return null;
		}

		protected override string QueryAccessToken(Uri returnUrl, string authorizationCode) {
			UriBuilder builder = new UriBuilder(TokenEndpoint);
			var args = new Dictionary<string, string> {
				{ "client_id", this.clientId },
				{ "redirect_uri", returnUrl.GetLeftPart(UriPartial.Path) },
				{ "client_secret", this.clientSecret },
				{ "code", authorizationCode }
			};
			builder.AppendQueryArgs(args);

			using (WebClient client = new WebClient()) {
				using (var stream = client.OpenRead(builder.Uri)) {
					var reader = new StreamReader(stream);
					string response = reader.ReadToEnd();
					string[] arr = response.Split('&');
					var at = arr[0].Split('=');

					return at[1];
				}
			}
		}

		public static void RewriteRequest() {
			var ctx = HttpContext.Current;

			var stateString = HttpUtility.UrlDecode(ctx.Request.QueryString["state"]);
			if (stateString == null || !stateString.Contains("__provider__=Facebook"))
				return;

			var q = HttpUtility.ParseQueryString(stateString);
			q.Add(ctx.Request.QueryString);
			q.Remove("state");

			ctx.RewritePath(ctx.Request.Path + "?" + q);
		}
	}
}