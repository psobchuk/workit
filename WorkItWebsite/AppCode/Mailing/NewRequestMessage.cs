﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Workit.DataModel;

namespace WorkItWebsite {
	public class NewRequestMessage: BaseMailMessage {
		protected override string TemplateName {
			get { return "NewRequest.html"; }
		}

		public string TaskTitle;

		public NewRequestMessage(string username, string email, Request request) {
			UserName = username;
			var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
			RedirectUri = urlHelper.Action("view", "task", new { id = request.Task.OrderId }, HttpContext.Current.Request.Url.Scheme);
			TaskTitle = request.Task.Name;

			this.To.Add(new MailAddress(email));
			this.Subject = "Нова заявка на виконання";
			this.Body = GetTemplateBodyHtml();
		}

		protected override string GetTemplateBodyHtml() {
			var collection = new Dictionary<string, string> {
				{ "##USERNAME##", UserName },
				{ "##REDIRECT_URI##", RedirectUri },
				{ "##TASK_TITLE##", TaskTitle }
			};

			string body = ReplaceTemplateVariables(collection);
			return body;
		}
	}
}