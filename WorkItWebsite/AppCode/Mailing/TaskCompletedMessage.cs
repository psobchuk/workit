﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace WorkItWebsite {
	public class TaskCompletedMessage: BaseMailMessage {
		protected override string TemplateName {
			get { return "TaskCompleted.html"; }
		}

		public TaskCompletedMessage(string username, string email): base() {
			UserName = username;
			var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
			RedirectUri = urlHelper.Action("index", "home", HttpContext.Current.Request.Url.Scheme);

			this.To.Add(new MailAddress(email));
			this.Subject = "Завдання завершено";
			this.Body = GetTemplateBodyHtml();
		}

		protected override string GetTemplateBodyHtml() {
			var collection = new Dictionary<string, string> {
				{ "##USERNAME##", UserName },
				{ "##REDIRECT_URI##", RedirectUri }
			};

			string body = ReplaceTemplateVariables(collection);
			return body;
		}
	}
}