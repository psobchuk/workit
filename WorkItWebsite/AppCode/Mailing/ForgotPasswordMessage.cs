﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace WorkItWebsite {
	public class ForgotPasswordMessage: BaseMailMessage {
		protected override string TemplateName {
			get { return "ResetPassword.html"; }
		}

		public ForgotPasswordMessage(string username, string email, string guid): base() {
			UserName = username;
			var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
			RedirectUri = urlHelper.Action("recoverpassword", "account", new { guid = guid }, HttpContext.Current.Request.Url.Scheme);

			this.To.Add(new MailAddress(email));
			this.Subject = "Відновлення паролю на RocketWizard";
			this.Body = GetTemplateBodyHtml();
		}

		protected override string GetTemplateBodyHtml() {
			var collection = new Dictionary<string, string> {
				{ "##USERNAME##", UserName },
				{ "##REDIRECT_URI##", RedirectUri }
			};

			string body = ReplaceTemplateVariables(collection);
			return body;
		}
	}
}