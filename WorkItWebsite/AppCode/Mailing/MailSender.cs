﻿using System;
using System.Net.Mail;
using System.Threading.Tasks;
using Workit.Common;
using Workit.DataModel;

namespace WorkItWebsite {
	public class MailSender {
		public static void SendMail(BaseMailMessage message) {
			var client = new SmtpClient();
			//Send email in separate thread
			Task sendMail = new Task(() => {
				try {
					client.Send(message);
				} catch (Exception ex) {
					Logger.LogErrorException("Error sending email", ex);
					throw;
				}
			});
			sendMail.Start();
		}

		public static void NotifyNewRequest(string username, string email, Request request) {
			var message = new NewRequestMessage(username, email, request);
			SendMail(message);
		}

		public static void NotifyRequestAccepted(string username, string email, Request request) {
			var message = new RequestAcceptedMessage(username, email, request);
			SendMail(message);
		}

		public static void NotifyTaskCompleted(string username, string email) {
			var message = new TaskCompletedMessage(username, email);
			SendMail(message);
		}
	}
}