﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using Workit.Common;

namespace WorkItWebsite {
	public abstract class BaseMailMessage: MailMessage {
		private const string BASE_TEMPLATE_PATH = "~/HtmlTemplates/";

		protected string UserName { get; set; }
		protected string RedirectUri { get; set; }

		public BaseMailMessage() {
			this.From = new MailAddress(Constants.DEFAULT_EMAIL, "Команда RocketWizard");
			this.IsBodyHtml = true;
		}

		protected abstract string TemplateName { get; }
		
		protected abstract string GetTemplateBodyHtml();

		protected string ReplaceTemplateVariables(Dictionary<string, string> templateCollection) {
			var body = string.Empty;
			string templatePath = HttpContext.Current.Server.MapPath(BASE_TEMPLATE_PATH + TemplateName);
			using (StreamReader reader = new StreamReader(templatePath)) {
				body = reader.ReadToEnd();
			}

			foreach(var item in templateCollection) {
				body = body.Replace(item.Key, item.Value);
			}

			return body;
		}
	}
}