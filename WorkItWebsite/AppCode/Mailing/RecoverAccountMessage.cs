﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace WorkItWebsite {
	public class RecoverAccountMessage: BaseMailMessage {
		protected override string TemplateName {
			get { return "RecoverAccount.html"; }
		}

		public RecoverAccountMessage(string email, string guid) {
			var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
			RedirectUri = urlHelper.Action("RecoverAccount", "Account", new { guid = guid }, HttpContext.Current.Request.Url.Scheme);

			this.To.Add(new MailAddress(email));
			this.Subject = "Відновлення облікового запису";
			this.Body = GetTemplateBodyHtml();
		}

		protected override string GetTemplateBodyHtml() {
			var collection = new Dictionary<string, string> {
				{ "##REDIRECT_URI##", RedirectUri }
			};

			string body = ReplaceTemplateVariables(collection);
			return body;
		}
	}
}