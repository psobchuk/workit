﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace WorkItWebsite {
	public sealed class EmailConfirmMessage: BaseMailMessage {
		protected override string TemplateName {
			get { return "EmailConfirmation.html"; }
		}

		public EmailConfirmMessage(string username, string email, string guid): base() {
			UserName = username;
			var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
			RedirectUri = urlHelper.Action("ConfirmEmail", "Account", new { guid = guid }, HttpContext.Current.Request.Url.Scheme);

			this.To.Add(new MailAddress(email));
			this.Subject = "Підтвердження електронної скриньки";
			this.Body = GetTemplateBodyHtml();
		}

		protected override string GetTemplateBodyHtml() {
			var collection = new Dictionary<string, string> {
				{ "##USERNAME##", UserName },
				{ "##REDIRECT_URI##", RedirectUri }
			};

			string body = ReplaceTemplateVariables(collection);
			return body;
		}
	}
}