﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkItWebsite {
	public class TaskFilter {
		public int PageIndex { get; set; }
		public int TaskStatus { get; set; }
		public int TaskType { get; set; }
		public long UserId { get; set; }
		public string DateStart { get; set; }
		public string DateEnd { get; set; }
		public string Category { get; set; }
		public int Step { get; set; }
		
	}
}