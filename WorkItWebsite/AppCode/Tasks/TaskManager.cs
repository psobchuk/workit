﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Workit.Common;
using Workit.DataAccess;
using Workit.DataModel;
using WorkItWebsite.Models;

namespace WorkItWebsite {
	public class TaskManager {
		private EntityProvider db;

		public TaskManager(EntityProvider entityProvider) {
			db = entityProvider;
		}

		#region Task Methods

		public bool ValidateTask(TaskViewModel model, ModelStateDictionary modelState) {
			if (!HttpContext.Current.Request.IsAuthenticated && !model.TermsAccepted) {
				modelState.AddModelError("TermsAccepted", "Неможливо створити завдання, якщо ви не згідні з правилами сайту.");
			}

			var date = new DateTime();
			if(!DateTimeUtils.ParseDateTime(model.DeadlineDate, model.DeadlineTime, out date)) {
				modelState.AddModelError("Deadline", "Некоректна дата");
			} else {
				if (date.Ticks < DateTime.Now.Ticks) {
					modelState.AddModelError("Deadline", "Некоректна дата");
				}
			}

			if(!model.CustomPrice && string.IsNullOrEmpty(model.Price)) {
				modelState.AddModelError("PriceRequired", "Вкажіть, будь ласка, ціну за послугу.");
			}

			return modelState.IsValid;
		}

		public Order AddTask(TaskViewModel model) {
			Order task = SetTaskData(model);
			task.Status = (int)TaskStatus.NotStarted;
			task = db.OrderRepository.Insert(task);
			db.SaveChanges();

			HttpContext.Current.Session["TEMP_TASK"] = null;

			return task;
		}

		public Order GetTask(long id) {
			Order order = db.OrderRepository.Get( o => o.OrderId == id).SingleOrDefault();
			return order;
		}

		public void SaveAsDraft(TaskViewModel model) {
			HttpContext.Current.Session["TEMP_TASK"] = model;
		}

		public TaskViewModel GetDraftedTask() {
			return HttpContext.Current.Session["TEMP_TASK"] as TaskViewModel;
		}

		public void CloseTask(long taskId, string rtext, int rType) {
			Order task = db.OrderRepository.Get(t => t.OrderId == taskId).SingleOrDefault();
			var newTask = task;
			newTask.Status = (int)TaskStatus.Finished;
			db.OrderRepository.Update(task, newTask);

			User performer = db.UserRepository.Get(u => u.UserId == task.PerformerId).SingleOrDefault();

			var response = new Response();
			response.Created = DateTime.Now;
			response.Description = rtext;
			response.Owner = performer;
			response.RespondentId = task.Client.UserId;
			response.ResponseType = rType;

			db.ResponseRepository.Insert(response);

			db.SaveChanges();

			var pm = new PaymentManager(db);
			pm.ProcessPayment(task.Client.UserId, task.PerformerId, task.Price);

			var manager = new UserManager(db);
			if(manager.VerifySettingsEnabled(performer, EmailTemplate.TaskCompleted)) {
				MailSender.NotifyTaskCompleted(performer.FirstName, performer.Email);
			}
		}

		public static string GetTaskStatusText(Order task) {
			switch (task.Status) {
				case (int)TaskStatus.NotStarted:
				return "Відкрите";
				case (int)TaskStatus.InProgress:
				return "На виконанні";
				case (int)TaskStatus.Finished:
				return "Завершене";
				default:
				return string.Empty;
			}
		}

		public void DeleteTask(long taskId) {
				db.OrderRepository.Delete(taskId);
				db.SaveChanges();
		}

		public bool IsTaskOwner(long taskId, long userId) {
			var order = db.OrderRepository.Get(t => t.OrderId == taskId && t.Client.UserId == userId).SingleOrDefault();

			return order != null ? true : false;
		}

		public List<Order> FilterTasks(TaskFilter filter) {
			int skip = filter.Step * filter.PageIndex;

			IQueryable<Order> filterQuery = db.OrderRepository.dbSet;

			int category = SelectCategory(filter.Category);
			if (category > 0) {
				filterQuery = filterQuery.Where(i => i.Category.CategoryId == category);
			}

			if(filter.UserId > 0) {
				switch ((TaskUserType)filter.TaskType) {
					case TaskUserType.Owner:
					filterQuery = filterQuery.Where(i => i.Client.UserId == filter.UserId);
					break;
					case TaskUserType.Performer:
					filterQuery = filterQuery.Where(i => i.PerformerId == filter.UserId);
					break;
				}
			}

			if (filter.TaskStatus > 0) {
				filterQuery = filterQuery.Where(i => i.Status == filter.TaskStatus);
			}

			if (!string.IsNullOrEmpty(filter.DateStart)) {
				var dateStart = DateTime.Parse(filter.DateStart);
				filterQuery = filterQuery.Where(i => i.Created >= dateStart);
			}

			if (!string.IsNullOrEmpty(filter.DateEnd)) {
				var dateEnd = DateTime.Parse(filter.DateEnd);
				filterQuery = filterQuery.Where(i => i.Created <= dateEnd);
			}

			var tasks = filterQuery.OrderByDescending(i => i.Created).Skip(skip).Take(filter.Step).ToList();

			return tasks;
		}

		public bool IsLocked(long taskId) {
			Order task = db.OrderRepository.Get(t => t.OrderId == taskId).SingleOrDefault();
			return task.PerformerId > 0;
		}

		#endregion

		#region Request methods

		public Request CreateRequest(RequestViewModel model) {
			var user = db.UserRepository.Get(u => u.UserId == model.RequestorId).SingleOrDefault();
			var tsk = db.OrderRepository.Get(o => o.OrderId == model.TaskId).SingleOrDefault();

			var request = new Request() {
				Question = model.Question,
				Requestor = user,
				RequestDate = DateTime.Now,
				Task = tsk,
				CustomPrice = model.Price
			};

			request = db.RequestRepository.Insert(request);
			db.SaveChanges();

			return request;
		}

		public void RemoveTaskRequest(long taskId, long userId) {
			var request = db.RequestRepository.Get(r => r.Requestor.UserId == userId && r.Task.OrderId == taskId).SingleOrDefault();
			if(request != null) {
				db.RequestRepository.Delete(request.RequestId);
				db.SaveChanges();
			}
		}

		public List<Request> GetTaskRequests(long id) {
			var requests = db.RequestRepository.Get(r => r.Task.OrderId == id).ToList();
			return requests;
		}

		#endregion

		public Comment SaveComment(CommentViewModel model) {
			var user = db.UserRepository.Get(u => u.UserId == model.CommenterId).SingleOrDefault();
			var request = db.RequestRepository.Get(r => r.RequestId == model.RequestId).SingleOrDefault();
			var comment = new Comment() {
				Commenter = user,
				Created = DateTime.Now,
				Text = model.Text,
				Request = request
			};

			comment = db.CommentRepository.Insert(comment);
			db.SaveChanges();

			return comment;
		}

		public bool IsRequested(long taskId, long userId) {
			var request = db.RequestRepository.Get(r => r.Task.OrderId == taskId && r.Requestor.UserId == userId)
							.SingleOrDefault();

			return request != null ? true : false;
		}

		public void AcceptAndLock(long taskId, long userId) {
			if(!IsLocked(taskId)) {
				Order task = db.OrderRepository.Get(t => t.OrderId == taskId).SingleOrDefault();
				Request request = db.RequestRepository.Get(r => r.Task.OrderId == taskId && r.Requestor.UserId == userId).SingleOrDefault();
				var newTask = task;
				newTask.PerformerId = userId;
				newTask.Status = (int)TaskStatus.InProgress;
				if(newTask.Price <= 0) {
					newTask.Price = int.Parse(request.CustomPrice);
				}

				db.OrderRepository.Update(task, newTask);
				db.SaveChanges();

				var pm = new PaymentManager(db);
				pm.LockMoney(task.Client.UserId, newTask.Price);

				var manager = new UserManager(db);
				if(manager.VerifySettingsEnabled(request.Requestor, EmailTemplate.RequestAccepted)) {
					MailSender.NotifyRequestAccepted(request.Requestor.FirstName, request.Requestor.Email, request);
				}
			}
		}

		#region Helpers

		private Order SetTaskData(TaskViewModel model) {
			var task = new Order();

			task.Name = model.Name;
			task.Description = model.Description;
			task.Created = DateTime.Now;
			task.Phone = model.Phone;


			var date = new DateTime();
			DateTimeUtils.ParseDateTime(model.DeadlineDate, model.DeadlineTime, out date);
			task.Deadline = date;

			if(model.User != null) {
				User user = db.UserRepository.Get(u => u.UserId == model.User.UserId).SingleOrDefault();
				task.Client = user;
			}

			//add category
			int cId = 1;
			int.TryParse(model.Category, out cId);
			var category = db.CategoryRepository.Get(c => c.CategoryId == cId).SingleOrDefault();
			task.Category = category;

			task.Price = model.CustomPrice ? 0 : int.Parse(model.Price);

			return task;
		}

		private int SelectCategory(string category) {
			switch(category) {
				case "courier":
					return 1;
				case "deliver":
					return 2;
				case "computer":
					return 3;
				case "photovideo":
					return 4;
				case "helper":
					return 5;
				case "repair":
					return 6;
				case "flyers":
					return 7;
				default:
					return 0;
			}
		}

		#endregion
	}
}