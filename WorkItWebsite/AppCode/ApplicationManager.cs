﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Workit.Common;

namespace WorkItWebsite {
	public class ApplicationManager {
		public static bool IsDevelopmentMode {
			get {
				#if DEBUG
					return true;
				#else
					return false;
				#endif
			}
		}

		public static void CreateCryptographyKeyFile() {
			var filePath = HttpContext.Current.Server.MapPath("~/keyfile.txt");
			if(!File.Exists(filePath)) {
				string aesKey = CryptoHelper.CreateKeyString();
				using(StreamWriter sWriter = new StreamWriter(filePath)) {
					sWriter.Write(aesKey);
				}
			}
		}
	}
}