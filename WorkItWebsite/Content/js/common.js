﻿function showModal(width, height, title, contentUrl, onSubmit) {
	//create a placeholder for modal on a page
	$("body").append("<div id='divModal'></div>");
	//load dialog html and display it
	$("#divModal").load(contentUrl, null, function () {
		$('#modalDialog').modal('show');
		//modal dialog submit
		if (onSubmit != null) {
			$("#divModal #submitData").on('click', onSubmit);
		}
	});

	//set event handlers
	//modal dialog closed
	$("#modalDialog").on('hidden.bs.modal', function () {
		$("#divModal").remove();
	});
}

function showStaticModal(selector, width, height, onSubmit) {
	$(".modal-dialog", selector).width(width);
	$(".modal-dialog", selector).height(height);
	$(selector).modal("show");
	if (onSubmit != null) {
		$(".modal-dialog #submitData", selector).on('click', onSubmit);
	}
}

function showConfirm(title, text, onConfirm) {
	var dialog = $("#modalDialogConfirm");
	$("#modalTitle", dialog).text(title);
	$(".confirm-content", dialog).text(text);
	$("#btnClose", dialog).text(StringResource.Yes);
	$("#btnClose", dialog).on('click', onConfirm);
	$("#btnCancel", dialog).text(StringResources.No);
	dialog.modal('show');
}

function showAlert(title, text) {
	var dialog = $("#modalDialogAlert");
	$("#btnClose", dialog).text(StringResources.Close);
	$("#modalTitle", dialog).text(title);
	$(".alert-content", dialog).text(text);
	dialog.modal('show');
}

//options {
//	pCount: int,
//	vCount: int
//}
(function ($) {
	$.fn.pager = function(options, fnIndexChanged) {
		var self = $(this);

		var opt = $.extend({
			sIndex: 1,
			cIndex: 1
		}, options);

		opt.vOffset = (opt.vCount - 1) / 2;

		var getCounter = function () {
			var counter = 1;
			var temp = opt.cIndex - opt.vOffset;
			if (opt.pCount - opt.cIndex >= opt.vCount) {
				counter = (temp > 1) ? temp : 1;
			} else if (opt.vCount < opt.pCount) {
				counter = (opt.cIndex + opt.vOffset < opt.pCount) ? temp : opt.pCount - opt.vCount + 1;
				if (counter <= 0) counter = 1;
			} else {
				opt.vCount = opt.pCount;
			}

			return counter;
		}

		var render = function () {
			self.html("");

			var pHtml = new StringBuilder();
			pHtml.append("<ul class='pagination'>");
			pHtml.append("<li><a href='#' class='p-first'>&laquo;</a></li>");
			pHtml.append("<li><a href='#' class='p-prev'>&lsaquo;</a></li>");
			var counter = getCounter();
			for (var i = counter; i < (counter + opt.vCount) ; i++) {
				pHtml.append("<li><a href='#' class='p-item'>" + i + "</a></li>");
			}
			pHtml.append("<li><a href='#' class='p-next'>&rsaquo;</a></li>");
			pHtml.append("<li><a href='#' class='p-last'>&raquo;</a></li>");
			pHtml.append("</ul>");

			self.append(pHtml.toString());

			$(self.selector + " .pagination li:contains('" + opt.cIndex + "')").addClass("active");

			//get paged data
			if (fnIndexChanged) {
				fnIndexChanged(opt.cIndex);
			}

			//event binding
			$(self.selector + " .pagination .p-first").off().on('click', function (e) { e.preventDefault(); opt.cIndex = 1; render(); });
			$(self.selector + " .pagination .p-last").off().on('click', function (e) { e.preventDefault(); opt.cIndex = opt.pCount; render(); });
			$(self.selector + " .pagination .p-prev").off().on('click', function (e) { e.preventDefault(); if (opt.cIndex > 1) opt.cIndex--; render(); });
			$(self.selector + " .pagination .p-next").off().on('click', function (e) { e.preventDefault(); if (opt.cIndex < opt.pCount) opt.cIndex++; render(); });
			$(self.selector + " .pagination .p-item").off().on('click', function (e) { e.preventDefault(); opt.cIndex = Number($(this).text()); render(); });

			//set disabled state
			$(".p-first, .p-last, .p-prev, .p-next", self.selector + " .pagination").removeClass("disabled");
			if (opt.cIndex == 1) {
				$(".p-first, .p-prev", self.selector + " .pagination").off('click').parent().addClass("disabled");
			}

			if (opt.cIndex == opt.pCount) {
				$(".p-last, .p-next", self.selector + " .pagination").off('click').parent().addClass("disabled");
			}
		}

		render();
	};
}(jQuery));

function StringBuilder() {
	this.strArr = new Array("");

	this.append = function (str) {
		if (str) {
			this.strArr.push(str);
		}
	}

	this.toString = function() {
		return this.strArr.join(" ");
	}
}

var common = cm = {
	temp: null,
	onAjaxSuccess: null,
	ajaxSuccessWrapper: function(result) {
		if(result.Status == AjaxStatusCode.Redirect) {
			window.location = result.ResponseMessage;
		} else {
			common.onAjaxSuccess(result);
		}
	},

	mapAjaxCallback: function(settings) {
		for(var item in settings) {
			if(item == "success") {
				common.onAjaxSuccess = settings[item];
				settings[item] = common.ajaxSuccessWrapper;
			}
		}
	},

	AjaxRequest: function(url, settings) {
		common.mapAjaxCallback(settings);
		$.ajax(url, settings);
	},

	validationError: function (e) {
		var valErrors = JSON.parse(e.responseText);
		for (var i = 0; i < valErrors.length; i++) {
			var spanVal = $("span[data-valmsg-for=" + valErrors[i].key + "]");
			spanVal.addClass("field-validation-error").parent().addClass("has-error");
			spanVal.removeClass("field-validation-valid");
			spanVal.text(valErrors[i].errors[0]);

			$("input[name=" + valErrors[i].key + "], " +
			  "textarea[name=" + valErrors[i].key + "], " +
			  "select[name=" + valErrors[i].key + "]").on('keypress', function () {
				$(this).parent().removeClass("has-error");
			});
		}
	},

	setSpinner: function (selector) {
		cm.temp = $(selector).html();
		$(selector).html(cm.generateSpinner(selector));
	},

	generateSpinner: function (selector) {
		var builder = new StringBuilder();
		builder.append("<div class='spinner2'></div>");
		return builder.toString();
	},

	removeSpinner: function (selector) {
		$(selector).html(cm.temp);
	}
}

var AjaxStatusCode = {
	Ok: 200,
	Redirect: 302,
	Error: 500
};

var TaskStatus = {
	NotStarted: 1,
	InProgress: 2,
	Finished: 3
};