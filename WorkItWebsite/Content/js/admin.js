﻿var GridFilter = function (defaultCol) {
	this.SortColumn = defaultCol;
	this.PageIndex = 0;
	this.SortOrder = "asc";
	this.PageSize = 10;
}

var UserViewModel = function () {
	var self = this;
	self.currSortOrder = "asc";
	self.currSortColumn = 'userid';
	self.currPageIndex = 0;
	self.currPageSize = 10;

	self.filter = new GridFilter(self.currSortColumn);

	self.users = ko.observableArray([]);

	self.selectedUser = ko.observable();

	self.selectUser = function () {
		self.selectedUser(this);
		$("#btnDelete").removeAttr("disabled");
	}

	self.addUser = function () {
		var data = {
			FirstName: $("#txtFirstName").val(),
			LastName: $("#txtLastName").val(),
			Email: $("#txtEmail").val(),
			Password: $("#txtPassword").val(),
			Status: $("#ddlStatus").val(),
			Type: $("#ddlType").val()
		}

		$.ajax({
			url: "/admin/adduser",
			type: "POST",
			data: data,
			success: function(result) {
				if (result.Status = "OK") {
					self.users.push(result.Data);
					$('#modalDialog').modal('hide');
				}
			},
			error: ua.validationError
		});
	}

	self.deleteUser = function () {
		$.ajax({
			url: "/Admin/DeleteUser",
			type: "POST",
			data: { userId: self.selectedUser().UserId },
			success: function (result) {
				self.users.remove(self.selectedUser());
				self.selectedUser(null);
				$("#btnDelete").attr("disabled", "disabled");
			}
		});
	}

	self.sortColumn = function (column) {
		$.ajax({
			url: '/admin/sortusers',
			type: "POST",
			data: { col: column },
			success: function (result) {
				
			}
		});
	}

	self.getPagedUsers = function () {
		var filter = self.filter;
		$.ajax({
			url: "/admin/getusers",
			type: "POST",
			data: { filter: filter },
			success: function (items) {
				self.users(items);
			},
			error: function (e) {
				alert(e.responseText);
			}
		})
	}
}

var TaskViewModel = function (items) {
	var self = this;
	self.tasks = ko.observableArray(items);
	self.selectedTask = ko.observable();
	self.selectTask = function () {
		self.selectedTask(this);
		$("#btnDelete").removeAttr("disabled");
		$("#btnAssignTo").removeAttr("disabled");
	}

	self.addTask = function () {
		var data = {
			Name: $("#txtName").val(),
			Description: $("#txtDescription").val(),
			Price: Number($("#txtPrice").val()),
			DeadlineDate: $("#txtDate").val(),
			DeadlineTime: $("#txtTime").val()
		}
		$.ajax({
			url: "/admin/addtask",
			type: "POST",
			data: data,
			success: function(result) {
				if (result.Status = "OK") {
					self.tasks.push(result.Data);
					$('#modalDialog').modal('hide');
				}
			},
			error: common.validationError
		});
	}

	self.deleteTask = function () {
	}

	self.assignTask = function() {
	}
}

var UserAdmin = ua = {
	userVM: null,
	taskViewModel: null,
	
	userAdminLoad: function () {
		ua.userVM = new UserViewModel();
		ko.applyBindings(ua.userVM);
		ua.userVM.getPagedUsers();

		$("#btnAdd").on('click', ua.showAddUserModal);
	},

	showAddUserModal: function () {
		showModal(400, 300, "Title", "/admin/addusermodal", ua.userViewModel.addUser);
	},

	taskAdminLoad: function () {
		$.ajax({
			url: "/admin/gettasks",
			type: "GET",
			success: function(items) {
				ua.taskViewModel = new TaskViewModel(items);
				ko.applyBindings(ua.taskViewModel);
			},
			error: function () {
				alert("shit happened. referesh the page, please and try again");
			}
		});

		$("#btnAdd").on('click', ua.showAddTaskModal);
	},

	showAddTaskModal: function () {
		showModal(400, 300, "Title", "/admin/addtaskmodal", ua.taskViewModel.addTask);
	}
}

