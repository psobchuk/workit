﻿var tasks = tsk = {
	taskId: null,
	filter: null,
	taskVM: null,
	requestVM: null,
	pageIndex: 1,
	currUserId: null,
	isActive: null,
	performerId: null,
	currCategory: 1,
	totalCount: 0,
	balance: 0,
	price: 0,
	init: function () {
		$('.txtDateEnd').datepicker({
			format: 'dd/mm/yyyy',
			language: 'uk'
		});

		$('#txtPrice').mask("99?999");
		$('#txtTime').mask("99:99");
		$('#txtPhone').mask("(999) 999 99 99");

		$("#chkCustomPrice").on('change', tsk.customPriceSelected);
		$("#chkAcceptTerms").on('change', tsk.termsAccepted);
		tsk.termsAccepted();

		$("#btnAddTask").on('click', function () {
			if ($("#frmCreateTask").valid()) {
				cm.setSpinner("#btnAddTask");
			}
		});
		$("#ddlCategory").on('change', tsk.categoryDescription);
		tsk.categoryDescription();
	},

	categoryDescription: function() {
		var val = Number($("#ddlCategory").val());
		var desc = $("#divCategoryDescription");
		switch (val) {
			case 1:
				desc.text(StringResources.CourierCategoryDescription);
				break;
			case 2:
				desc.text(StringResources.DeliverCategoryDescription);
				break;
			case 3:
				desc.text(StringResources.ComputerCategoryDescription);
				break;
			case 4:
				desc.text(StringResources.PhotoVideoCategoryDescription);
				break;
			case 5:
				desc.text(StringResources.HelperCategoryDescription);
				break;
			case 6:
				desc.text(StringResources.RepairCategoryDescription);
				break;
			case 7:
				desc.text(StringResources.FlyersCategoryDescription);
				break;
		}
	},

	subscribe: function () {
		$(".subscribe-block").slideToggle();
		$("#txtPrice").on('keypress', function () {
			$("#subPvalmsg").hide();
		})
	},

	onSubscribe: function () {
		cm.setSpinner(".spinner-wrapper");
		if ($("#txtPrice").length && $("#txtPrice").val() == "") {
			$("#subPvalmsg").show();
			return;
		}

		var data = {
			Question: $("#txtSubQuestion").val(),
			TaskId: tsk.taskId,
			Price: $("#txtPrice").val()
		};

		common.AjaxRequest(task_subscribe, {
			type: "POST",
			data: data,
			success: function (result) {
				if (result.status == AjaxStatusCode.Ok) {
					$("#btnSubscribe").hide();
					$("#btnUnsubscribe").show();
					$(".subscribe-block").slideToggle();

					tsk.requestVM.requests.push(new Request(result.data));
				}
				cm.removeSpinner(".spinner-wrapper");
			}
		});
	},

	unsubscribe: function () {
		cm.setSpinner(".spinner-wrapper");
		common.AjaxRequest(task_unsubscribe, {
			type: "POST",
			data: { id: tsk.taskId },
			success: function (result) {
				if (result.status == AjaxStatusCode.Ok) {
					$("#btnSubscribe").show();
					$("#btnUnsubscribe").hide();
					tsk.requestVM.requests.remove(function (item) { return item.RequestorId == result.data });
				}
				cm.removeSpinner(".spinner-wrapper");
			}
		});
	},

	initAllTasks: function (currCategory) {
		tsk.currCategory = currCategory;
		tsk.taskVM = new TaskViewModel();
		ko.applyBindings(tsk.taskVM);
		$(".btnFilter").on('click', function () { tsk.filterTasks(true); });
		$(".category-item").on('click', tsk.changeCategory);
		$(".categories").find("[data-id=" + tsk.currCategory + "]").addClass("active");
		$(window).scroll(function () {
			if ($(window).scrollTop() >= $(document).height() - $(window).height()) {
				tsk.loadMoreTasks();
			}
		});

		tsk.filterTasks(false);
	},

	filterTasks: function (refreshView) {
		tsk.filter = {
			TaskStatus: $(".ddlTaskStatus").val(),
			DateStart: $(".txtDateStart").val(),
			DateEnd: $(".txtDateEnd").val(),
			Category: tsk.currCategory
		};
		
		if (refreshView) {
			cm.setSpinner(".tasks .spinner-wrapper");
			tsk.taskVM.tasks.removeAll();
			tsk.pageIndex = 1;
		}
		tsk.taskVM.getPagedTasks(tsk.pageIndex, tsk.filter);
	},

	loadMoreTasks: function() {
		tsk.pageIndex++;
		tsk.filterTasks(false);
	},

	customPriceSelected: function () {
		if ($(this).is(":checked")) {
			$("#txtPrice").val("").attr("disabled", "disabled");
		} else {
			$("#txtPrice").removeAttr("disabled");
		}
	},

	termsAccepted: function () {
		if (!$("#chkAcceptTerms").length || $("#chkAcceptTerms").is(":checked")) {
			$("#btnAddTask").removeAttr("disabled");
		} else {
			$("#btnAddTask").attr("disabled", "disabled");
		}
	},

	initTaskView: function (taskId, userId, isActive, performerId) {
		tsk.taskId = taskId;
		tsk.currUserId = userId;
		tsk.isActive = isActive;
		tsk.requestVM = new RequestViewModel();
		tsk.performerId = performerId;
		ko.applyBindings(tsk.requestVM);

		$("#btnSubscribe").on('click', tsk.subscribe);
		$("#btnSaveSubscription").on('click', tsk.onSubscribe);
		$("#btnUnsubscribe").on('click', tsk.unsubscribe);
		$("#btnDelete").on('click', tsk.deleteTask);
		$("#btnCloseTask").on('click', function () { $(".finish-block").slideToggle(); });
		$("#btnFinishTask").on('click', tsk.closeTask);
		$('#txtPrice').mask("99?999");
		tsk.renderRequests();
	},

	renderRequests: function () {
		cm.setSpinner(".spinner-wrapper");
		common.AjaxRequest(task_getrequests, {
			type: "GET",
			data: { id: tsk.taskId },
			success: function (result) {
				if (result.status == AjaxStatusCode.Ok) {
					for (var i = 0; i < result.data.length; i++) {
						var request = new Request(result.data[i]);
						tsk.requestVM.requests.push(request);
					}
				}

				cm.removeSpinner(".spinner-wrapper");
			}
		});
	},

	showAcceptConfirm: function (request) {
		if (!tsk.isActive) {
			showAlert(StringResources.Warning, StringResources.CannotSelectExecutorEmailNotActivated);
		} else if (tsk.balance < Number(tsk.price)) {
			showAlert(StringResource.Warning, StringResources.NotEnoughMoney);
		} else {
			tsk.checkCanAcceptPerformer(request);
		}
	},

	checkCanAcceptPerformer: function(request) {
		cm.setSpinner(".accept .spinner-wrapper");
		cm.AjaxRequest("/task/canaccept", {
			type: "POST",
			data: { requestorId: request.RequestorId, taskId: tsk.taskId },
			success: function(result) {
				if (result.status == "ok") {
					showConfirm(StringResources.UserSelection, StringResources.AreYouSureToSelectThisUser,
					function () {
						$("#hidd_tId").val(tsk.taskId);
						$("#hidd_rId").val(request.RequestorId);
						$("#frmAcceptUser").submit();
					});
				} else {
					showAlert(StringResources.Warning, StringResources.NotEnoughMoney);
				}
				cm.removeSpinner(".accept .spinner-wrapper");
			}
		});
	},

	deleteTask: function() {
		showConfirm(StringResources.TaskDeletion, StringResources.AreYouSureDeleteTask,
		function () {
			common.AjaxRequest(task_delete, {
				type: "POST",
				data: { id: tsk.taskId },
				success: function (result) {
					if (result.status == AjaxStatusCode.Ok) {
						window.location = result.url;
					}
				}
			});
		});
	},

	closeTask: function () {
		cm.setSpinner(".spinner-wrapper");
		common.AjaxRequest(task_close, {
			type: "POST",
			data: { id: tsk.taskId, response: $("#txtSubResponse").val(), rtype: $("#ddlResponseType").val() },
			success: function (result) {
				if (result.status == AjaxStatusCode.Ok) {
					window.location = result.url;
				}

				cm.removeSpinner(".spinner-wrapper");
			}
		});
	},

	changeCategory: function (e) {
		e.preventDefault();
		var self = $(this).parent();
		tsk.currCategory = self.attr("data-id");
		tsk.filterTasks(true);
		$(".category-item").parent().removeClass("active");
		self.addClass("active");
	}
}

var TaskViewModel = function () {
	var self = this;
	self.tasks = ko.observableArray([]);
	
	self.getPagedTasks = function (pageIndex, filter, onSuccess) {
		var data = {
			UserId: 0,
			PageIndex: Number(pageIndex - 1),
			TaskStatus: Number(filter.TaskStatus),
			DateStart: filter.dStart,
			DateEnd: filter.DateEnd,
			Category: filter.Category
		}

		common.AjaxRequest(task_loadall, {
			data: data,
			type: "POST",
			success: function (result) {
				if (result.tasks) {
					var arr = new Array();
					for (i = 0; i < result.tasks.length; i++) {
						if (result.tasks[i].Price == "0 грн.") {
							result.tasks[i].Price = StringResources.PriceSetByExecutor;
						}
						self.tasks.push(result.tasks[i]);
					}

					tsk.totalCount = result.count;
				}
				cm.removeSpinner(".tasks .spinner-wrapper");
			}
		});
	}

	self.taskStatus = function (task) {
		switch (task.Status) {
			case TaskStatus.NotStarted:
				return "label-success";
			case TaskStatus.InProgress:
				return "label-warning";
			case TaskStatus.Finished:
				return "label-default";
		}
	}
};

var RequestViewModel = function () {
	var self = this;
	self.requests = ko.observableArray([]);

	self.respond = function (request) {
		var wrapper = $("div").find("[id=" + request.RequestId + "]");
		$(".respond-block", wrapper).slideToggle();
		$("#txtSubComment", wrapper).on('keypress', function () {
			$("#subCvalmsg", wrapper).hide();
		})
	};

	self.saveResponse = function (request) {
		var wrapper = $("div").find("[id=" + request.RequestId + "]");

		if ($("#txtSubComment", wrapper).val().length <= 0) {
			$("#subCvalmsg").show();
			return;
		}

		var data = {
			RequestId: request.RequestId,
			Text: $("#txtSubComment", wrapper).val(),
			CommenterId: tsk.currUserId,
			TaskId: tsk.taskId
		};

		common.AjaxRequest(task_savecomment, {
			type: "POST",
			data: data,
			success: function (result) {
				if (result.status == AjaxStatusCode.Ok) {
					var wrapper = $("div").find("[id=" + request.RequestId + "]");
					$(".respond-block", wrapper).slideToggle()
					request.Comments.push(result.data);
				}
			}
		});
	}

	self.acceptPerformer = function (request) {
		tsk.showAcceptConfirm(request);
	};

	self.allowComment = function (request) {
		return tsk.currUserId == request.RequestorId;
	},

	self.selectedRequest = function (request) {
		if (tsk.performerId > 0 && request.RequestorId == tsk.performerId) {
			return "selected-request";
		}
	}
};

var Request = function (request) {
	this.Comments = ko.observableArray([]);
	this.RequestId = request.RequestId;
	this.Question = request.Question;
	this.Created = request.Created;
	this.Comments(request.Comments);
	this.ProfileUrl = request.ProfileUrl;
	this.DisplayName = request.FirstName + " " + request.LastName;
	this.ProfileImage = request.RequestorImage;
	this.RequestorId = request.RequestorId;
	this.Price = request.Price;

	this.addComment = function (comment) {
		this.Comments.push(comment);
	}
}