﻿var register = reg = {
	init: function () {
		$('#txtPhone').mask("(999) 999 99 99");
		initImgSelect();

		reg.acceptTerms();
		$("#chkAcceptTerms").on('change', reg.acceptTerms);

		$("#btnRegister").on('click', function () {
			if ($("#frmRegister").valid()) {
				cm.setSpinner("#btnRegister");
			}
		});
	},

	acceptTerms: function () {
		if (!$("#chkAcceptTerms").length || $("#chkAcceptTerms").is(":checked")) {
			$("#btnRegister").removeAttr("disabled");
		} else {
			$("#btnRegister").attr("disabled", "disabled");
		}
	}
};