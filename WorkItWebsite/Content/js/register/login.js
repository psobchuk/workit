﻿//login logic
var login = {
	showPopup: false,

	init: function () {
		$("#btnLogin").click(function () {
			showStaticModal('#modalDialogLogin', 600, 300, login.onLoginSubmit);
			return false;
		});

		$("#btnVk, #btnPlus, #btnFacebook").click(function () {
			cm.setSpinner("#modalDialogLogin #submitData");
		});

		if (login.showPopup) {
			showStaticModal('#modalDialogLogin', 600, 300, login.onLoginSubmit);
		}
	},

	onLoginSubmit: function () {
		var remember = $("#chkRemember").val();
		var token = $('input[name="__RequestVerificationToken"]', "#frmLogin").val();
		var data = {
			__RequestVerificationToken: token,
			LoginEmail: $("#txtLoginEmail").val(),
			Password: $("#txtPassword").val(),
			RememberMe: remember.toLowerCase() == 'true'
		};

		$.ajax({
			url: '/Account/Login',
			type: "POST",
			data: data,
			beforeSend: function () { cm.setSpinner("#modalDialogLogin #submitData"); },
			success: function (result) {
				if (result.status == "error") {
					$("#validation-message").html(result.responseMessage);
					$("#txtEmail, #txtPassword").parent().addClass("has-error");
					$("#txtEmail, #txtPassword").on('keypress', function () {
						$("#txtEmail, #txtPassword").parent().removeClass("has-error");
						$("#validation-message").html("");
					});

					cm.removeSpinner("#modalDialogLogin #submitData");
				} else {
					window.location = result.responseMessage;
				}
			}
		});
	}
}