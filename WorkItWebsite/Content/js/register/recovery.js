﻿$(function() {
	$("#sendMail").click(sendMail);
	$("#sendAccountEmail").click(sendAccountEmail);
});

function sendMail() {
	$.ajax({
		url: "/account/sendrecoveremail",
		data: { email: $("#txtEmail").val() },
		type: "POST",
		success: function (result) {
			if (result.Status == AjaxStatusCode.Error) {
				$("#email-valmsg").html(result.ResponseMessage);
			} else {
				$("#email-form").html(result.ResponseMessage);
			}
		}
	});
}

function sendAccountEmail() {
	$.ajax({
		url: "/account/sendrecoveraccountemail",
		data: { email: $("#txtEmail").val() },
		type: "POST",
		success: function (result) {
			if (result.Status == AjaxStatusCode.Error) {
				$("#email-valmsg").html(result.ResponseMessage);
			} else {
				$("#email-form").html(result.ResponseMessage);
			}
		}
	});
}

