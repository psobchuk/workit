﻿var initImgSelect = null;
var ias = null;
//bootstrap styling for fileupload button
$(document).on('change', '.btn-file :file', function () {
	var input = $(this),
		numFiles = input.get(0).files ? input.get(0).files.length : 1,
		label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [numFiles, label]);
});

$(function () {
	//Set the 4 coordinates for the cropping
	var setPreview = function (x, y, w, h) {
		$('#ImageUploadData_X').val(x || 0);
		$('#ImageUploadData_Y').val(y || 0);
		var width = w;
		var height = h
		if (preview[0].naturalWidth < w || w == null) width = preview[0].naturalWidth;
		if (preview[0].naturalHeight < h || h == null) height = preview[0].naturalHeight;
		$('#ImageUploadData_Width').val(width);
		$('#ImageUploadData_Height').val(height);
	};

	initImgSelect = function () {
		//Initialize the image area select plugin
		ias = preview.imgAreaSelect({
			handles: true,
			instance: true,
			parent: 'body',
			aspectRatio: '1:1',
			onSelectEnd: function (s, e) {
				var scale = preview[0].naturalWidth / preview.width();
				var _f = Math.floor;
				setPreview(_f(scale * e.x1), _f(scale * e.y1), _f(scale * e.width), _f(scale * e.height));
			}
		});

		ias.setOptions({
			x1: 0,
			y1: 0,
			x2: 150,
			y2: 150,
			show: true,
			resizable: false,
			persistent: true
		});

		//Initial state of X, Y, Width and Height is 0 0 1 1
		setPreview(0, 0, 150, 150);
		remoteUrl.val(preview.attr('src'));
	};

	//Get the preview image and set the onload event handler
	var preview = $('#preview');
	var iud_url = $("#ImageUploadData_Url");
	iud_url.val('');

	var isFormValid = true;
	var uploadFileType = $("#ImageUploadData_UploadFileType");
	uploadFileType.val(UploadFileType.RemoteUrl);
	var remoteUrl = $("#ImageUploadData_Url");

	//What happens if the URL changes?
	$("#btnImageUrl").on('click', function () {
		var imgUrl = $("#ImageUploadData_remoteImageUrl").val();

		var valid_extensions = /(.jpg|.jpeg|.png)$/i;
		isFormValid = valid_extensions.test(imgUrl);
		if (isFormValid) {
			preview.attr('src', imgUrl);
			remoteUrl.val(imgUrl);
			uploadFileType.val(UploadFileType.RemoteUrl);
			//remove lolal file selection
			$(".localFile-group .text-file").val('');
			$(".localFile-group").removeClass("has-error");
			$(".localFile-validation").hide();
		} else {
			$("#ImageUploadData_remoteImageUrl").parent().addClass("has-error");
			$(".remoteUrl-validation").show();
		}
	});

	$("#ImageUploadData_remoteImageUrl").on('change', function () {
		$(this).parent().removeClass("has-error");
		$(".remoteUrl-validation").hide();
	});

	//What happens if the File changes?
	$('#ProfilePhoto').change(function (evt) {
		if (evt.target.files === undefined)
			return filePreview();

		if (checkFileSizeNormal(evt)) {

			var f = evt.target.files[0];
			var reader = new FileReader();

			reader.onload = function (e) {
				if (isFormValid) {
					preview.attr('src', e.target.result);
					uploadFileType.val(UploadFileType.LocalFile);
					//remove remote url selection
					$("#ImageUploadData_remoteImageUrl").val('');
					$("#ImageUploadData_remoteImageUrl").parent().removeClass("has-error");
					$(".remoteUrl-validation").hide();
				} else {
					$(".localFile-group").addClass("has-error");
					$(".localFile-validation").text("Некоректне розширення файлу").show();
				}
			};
			reader.readAsDataURL(f);

			var valid_extensions = /(.jpg|.jpeg|.png)$/i;
			isFormValid = valid_extensions.test($(this).val());
			$(".localFile-group").removeClass("has-error");
			$(".localFile-validation").hide();
		} else {
			isFormValid = false;
			$(".localFile-group").addClass("has-error");
			$(".localFile-validation").text("Розмір файлу більше допустимого").show();
		}
	});

	$("#frmRegister").submit(function (e) {
		if (!isFormValid) e.preventDefault();
	});

	//fallback for IE9 to localfile preview
	var filePreview = function () {
		//clear validation errors
		$(".localFile-group").removeClass("has-error");
		$(".localFile-validation").hide();
		$("#ImageUploadData_remoteImageUrl").val('');
		$("#ImageUploadData_remoteImageUrl").parent().removeClass("has-error");
		$(".remoteUrl-validation").hide();
		isFormValid = true;

		//disable jquery validation
		$("input[type=submit]").addClass('cancel');
		$("#frmRegister").data("validator").cancelSubmit = true;

		//prepare iframe for form submission
		$('body').append('<iframe id="preview-iframe" onload="callback();" name="preview-iframe" style="display:none" />');
		var action = $('#frmRegister').attr('target', 'preview-iframe').attr('action');
		$('#frmRegister').attr('action', '/Media/PreviewImage');
		$('#frmRegister').attr('method', 'POST');

		//prepare callback after form submit
		window.callback = function () {
			//get result data
			var result = $('#preview-iframe').contents().find('img').attr('src');
			var valid = $('#preview-iframe').contents().find('img').attr('data-valid');
			
			$('#preview-iframe').remove();

			//validate result
			if (valid && valid.toLowerCase() == 'true') {
				preview.attr('src', result);
			} else {
				$(".localFile-group").addClass("has-error");
				$(".localFile-validation").show();
			}
		};

		//submit data
		$('#frmRegister').submit().attr('action', action).attr('target', '');
	};

	function checkFileSizeNormal(evt) {
		var f = evt.target.files[0];
		var filesize = f.size / (1024 * 1024);
		return filesize <= 10;
	};

	$('.btn-file :file').on('fileselect', function (event, numFiles, label) {
		$(".text-file").val(label);
	});
});

var UploadFileType = {
	RemoteUrl: 1,
	LocalFile: 2
}