﻿var EditProfile = ep = {
	init: function () {
		$("#lnkChangePassword").on('click', ep.openChangePasswordDialog);
		$("#lnkPhotoEdit").on('click', ep.openEditPhotoDialog);
		$('#txtPhone1, #txtPhone2').mask("(999) 999 99 99");
		ep.initEmployeeFields();
		$("#chkEmployed").on('change', ep.initEmployeeFields);

		$("#ciModalWrapper #modalDialog").on('shown.bs.modal', function () {
			initImgSelect();
		});

		$("#ciModalWrapper #modalDialog").on('hide.bs.modal', function () {
			ias.setOptions({ hide: true });
			ias.update();
		});

	},

	openEditPhotoDialog: function (e) {
		e.preventDefault();
		showStaticModal("#ciModalWrapper #modalDialog", 700, 300, ep.changePhoto);

		$("#ciModalWrapper #modalDialog").on('hidden.bs.modal', function () {
			$(".localFile-validation, .remoteUrl-validation").hide();
			$("#ImageUploadData_remoteImageUrl").val("").parent().removeClass("has-error");
			//remove lolal file selection
			$(".localFile-group .text-file").val('');
			$(".localFile-group").removeClass("has-error");

			$(".remote-wrapper").addClass("col-md-10");

			var photoUrl = $(".profile-img").attr("src");
			$("#preview").attr("src", photoUrl);
		});

		
	},

	openChangePasswordDialog: function (e) {
		e.preventDefault();
		showStaticModal("#cpModalWrapper #modalDialog", 600, 400, ep.changePassword);

		$("#cpModalWrapper #modalDialog").on('show.bs.modal', function () {
			$("#txtOldPassword, #txtNewPassword, #txtConfirmPassword", this).val("").removeClass("input-validation-error");
			$("#resultmsg", this).text("");

			var spanVal = $("span[data-valmsg-for=OldPassword], span[data-valmsg-for=NewPassword], span[data-valmsg-for=ConfirmPassword]");
			spanVal.removeClass("field-validation-error").addClass("field-validation-valid").parent().removeClass("has-error");
			spanVal.text("");
		});
	},

	changePassword: function () {
		var data = {
			OldPassword: $("#txtOldPassword").val(),
			NewPassword: $("#txtNewPassword").val(),
			ConfirmPassword: $("#txtConfirmPassword").val()
		};

		common.AjaxRequest("/user/changepassword", {
			type: "POST",
			data: data,
			success: function (result) {
				if (result.Status == AjaxStatusCode.Ok) {
					$("#resultmsg").text(result.ResponseMessage).removeClass("text-danger").addClass("text-success");
				} else {
					$("#resultmsg").text(result.ResponseMessage).removeClass("text-success").addClass("text-danger");
				}
			},

			error: common.validationError
		});
	},

	changePhoto: function () {
		$("#frmRegister").submit();
	},

	initEmployeeFields: function () {
		var self = $("#chkEmployed");
		if (self.is(":checked")) {
			$("#txtCompany").removeAttr("disabled");
			$("#txtPosition").removeAttr("disabled");
		} else {
			$("#txtCompany").attr("disabled", "disabled").val("");
			$("#txtPosition").attr("disabled", "disabled").val("");
		}
	}
}