﻿var UserProfile = up = {
	userId: null,
	profileVM: null,
	visibleCount: 5,
	responsesCount: 0,
	ownerTaskCount: 0,
	performerTaskCount: 0,
	notificationSettings: null,
	userAlerts: null,
	pfilter: null,
	ofilter: null,

	init: function (userId) {
		up.userId = userId;
		up.profileVM = new ProfileViewModel();
		ko.applyBindings(up.profileVM);

		up.initNotificationTab();

		if (up.responsesCount > 0) {
			cm.setSpinner("#response .spinner-wrapper");
			var pagerOpt = {
				pCount: Math.ceil(up.responsesCount / up.visibleCount),
				vCount: up.visibleCount
			};

			$("#response-pager").pager(pagerOpt, up.getPagedResponses);
		}

		$("#ownertask").on('click', function () { up.initTaskTab(TaskType.Owner, false); });
		$("#performertask").on('click', function () { up.initTaskTab(TaskType.Performer, false); });

		$("#lnkProfileDelete").on('click', up.lnkProfileDelete);

		$("#ownertasks .btnFilter").on('click', function() { up.filterTasks(TaskType.Owner) });
		$("#performertasks .btnFilter").on('click', function() { up.filterTasks(TaskType.Performer) });
	},

	loadResponses: function (responses) {
		if (responses) {
			up.profileVM.responses.removeAll();
			for (i = 0; i < responses.length; i++) {
				var item = new UserResponse(responses[i]);
				up.profileVM.responses.push(item);
			}
		}
	},

	getPagedResponses: function (pageIndex) {
		var settings = {
			data: { userId: Number(up.userId), pIndex: Number(pageIndex - 1) },
			type: "POST",
			success: function (result) {
				up.loadResponses(result.responses);
				cm.removeSpinner("#response .spinner-wrapper");
			}
		};

		cm.AjaxRequest(profile_get_responses, settings);
	},

	initTaskTab: function (taskType, renderOnly) {
		var pagerId = "";
		var count = 0;
		var filter = null;

		if (taskType == TaskType.Performer) {
			if ($("#ptask-pager").html() != "") return;
			pagerId = "ptask-pager";
			count = up.performerTaskCount;
			up.pfilter = {
				tStatus: $(".ddlTaskStatus", "#performertasks").val(),
				dStart: $(".txtDateStart", "#performertasks").val(),
				dEnd: $(".txtDateEnd", "#performertasks").val(),
			};

			filter = up.pfilter;
		} else {
			if ($("#otask-pager").html() != "") return;
			pagerId = "otask-pager";
			count = up.ownerTaskCount;
			up.ofilter = {
				tStatus: $(".ddlTaskStatus", "#ownertasks").val(),
				dStart: $(".txtDateStart", "#ownertasks").val(),
				dEnd: $(".txtDateEnd", "#ownertasks").val(),
			};

			filter = up.ofilter;
		}

		var pagerOptions = {
			pCount: Math.ceil(count / up.visibleCount),
			vCount: up.visibleCount
		}

		if (count > 0) {
			$("#" + pagerId).pager(pagerOptions, function (pageIndex) {
				if (!renderOnly) {
					up.profileVM.getPagedTasks(pageIndex, taskType, filter);
				}

				renderOnly = false;
			});
		}
	},

	initNotificationTab: function () {
		$("#notifications .notify-buttons button").hide();
		$("#notifications input[type=checkbox]").attr("disabled", "disabled");

		$("#notifications .notify-edit").on('click', function (e) {
			e.preventDefault();
			$("#notifications input[type=checkbox]").removeAttr("disabled");
			$(this).hide();
			$("#notifications .notify-buttons button").show();

			up.notificationSettings = {
				chkNews: $("#chkNews").is(":checked"),
				chkAssigned: $("#chkAssigned").is(":checked"),
				chkAccepted: $("#chkAccepted").is(":checked"),
				chkFinished: $("#chkFinished").is(":checked"),
				chkComment: $("#chkComment").is(":checked")
			}
		});

		$("#notifications #btnCancelNotify").on('click', function () {
			$("#notifications .notify-buttons button").hide();
			$("#notifications input[type=checkbox]").attr("disabled", "disabled");
			$("#notifications .notify-edit").show();

			if (up.notificationSettings.chkNews) $("#chkNews").attr('checked', 'checked');
			if (up.notificationSettings.chkAssigned) $("#chkAssigned").attr('checked', 'checked');
			if (up.notificationSettings.chkAccepted) $("#chkAccepted").attr('checked', 'checked');
			if (up.notificationSettings.chkFinished) $("#chkFinished").attr('checked', 'checked');
			if (up.notificationSettings.chkComment) $("#chkComment").attr('checked', 'checked');
		});

		$("#notifications #btnSaveNotify").on('click', up.saveNotificationSettings);
	},

	saveNotificationSettings: function () {
		var data = {
			NotifyNews: $("#chkNews").is(":checked"),
			NotifyTaskAssigned: $("#chkAssigned").is(":checked"),
			NotifyTaskFinished: $("#chkFinished").is(":checked"),
			NotifyNewTaskComment: $("#chkComment").is(":checked"),
			NotifyTaskAccepted: $("#chkAccepted").is(":checked")
		};

		common.AjaxRequest("/user/savenotifications", {
			data: data,
			type: "POST",
			success: function (result) {
				if (result.status == "ok") {
					$("#notifications .notify-buttons button").hide();
					$("#notifications input[type=checkbox]").attr("disabled", "disabled");
					$("#notifications .notify-edit").show();
				}
			}
		});
	},

	lnkProfileDelete: function (e) {
		e.preventDefault();
		showConfirm(StringResources.ProfileDeletion, StringResources.AreYouSureDeleteProfile,
		function () {
			$("#frmDeleteProfile").submit();
		});
	},

	filterTasks: function (taskType) {
		var filter = null;
		if (taskType == TaskType.Performer) {
			cm.setSpinner("#performertasks .spinner-wrapper");
			$("#ptask-pager").html("");
			up.pfilter = {
				TaskStatus: $(".ddlTaskStatus", "#performertasks").val(),
				DateStart: $(".txtDateStart", "#performertasks").val(),
				DateEnd: $(".txtDateEnd", "#performertasks").val(),
			};
			filter = up.pfilter;
		} else {
			cm.setSpinner("#ownertasks .spinner-wrapper");
			$("#otask-pager").html("");
			up.ofilter = {
				TaskStatus: $(".ddlTaskStatus", "#ownertasks").val(),
				DateStart: $(".txtDateStart", "#ownertasks").val(),
				DateEnd: $(".txtDateEnd", "#ownertasks").val(),
			};
			filter = up.ofilter;
		}
		up.profileVM.getPagedTasks(1, taskType, filter, function () {
			up.initTaskTab(taskType, true);
		});
	}
};

var ProfileViewModel = function () {
	var self = this;
	self.responses = ko.observableArray([]);
	self.otasks = ko.observableArray([]);
	self.ptasks = ko.observableArray([]);

	self.taskStatus = function (task) {
		switch (task.Status) {
			case TaskStatus.NotStarted:
				return "label-success";
			case TaskStatus.InProgress:
				return "label-warning";
			case TaskStatus.Finished:
				return "label-default";
		}
	}
	
	self.getPagedTasks = function(pageIndex, taskType, filter, onSuccess) {
		var data = {
			UserId: Number(up.userId),
			PageIndex: Number(pageIndex - 1),
			TaskType: taskType,
			TaskStatus: Number(filter.TaskStatus),
			DateStart: filter.DateStart,
			DateEnd: filter.DateEnd
		}

		common.AjaxRequest(profile_get_tasks, {
			data: data,
			type: "POST",
			success: function (result) {
				if (result.tasks) {
					var tasks = new Array();
					for (i = 0; i < result.tasks.length; i++) {
						if (result.tasks[i].Price == "0 грн.") {
							result.tasks[i].Price = StringResources.PriceSetByExecutor;
						}
						tasks.push(result.tasks[i]);
					}

					if (taskType == TaskType.Performer) {
						self.ptasks(tasks);
						up.performerTaskCount = result.count;
					}
					if (taskType == TaskType.Owner) {
						self.otasks(tasks);
						up.ownerTaskCount = result.count;
					}
				}

				if (onSuccess) onSuccess();
				cm.removeSpinner(".spinner-wrapper");
			}
		});
	}
};

var UserResponse = function (response) {
	this.responseType = ko.computed(function () {
		switch (response.ResponseType) {
			case 1:
				return ResponseType.Good;
			case 2:
				return ResponseType.Neutral;
			case 3:
				return ResponseType.Bad;
		}
	});

	this.profileUrl = response.ProfileUrl;
	this.displayName = response.RespondentName;
	this.createdDate = response.CreatedDate;
	this.responseContent = response.Description;
	this.profileImage = response.RespondentImage;
};

var UserTask = function(task) {
};

var ResponseType = {
	Good: "response-good",
	Neutral: "response-neutral",
	Bad: "response-bad"
}

var TaskType = {
	Owner: 1,
	Performer: 2
}
