﻿using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using Workit.Common;
using Workit.DataAccess;
using Workit.DataModel;
using Workit.Resources;
using WorkItWebsite.Filters;
using WorkItWebsite.Helpers;
using WorkItWebsite.Models;

namespace WorkItWebsite.Controllers {
	[Authorize]
	public class AccountController :  BaseController {
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public JsonResult Login(LoginModel model) {
			var db = new EntityProvider();
			var user = db.UserRepository.Get(u => u.Email == model.LoginEmail).SingleOrDefault();
			if(user != null && user.Status == (int)UserStatus.Deleted) {
				return Json(new { status = "ok", responseMessage = Url.Action("recoveraccount", "account") });
			}
			
			if (ModelState.IsValid && AuthenticationManager.Login(model.LoginEmail, model.Password, model.RememberMe)) {
				//var returnUrl = GetReturnUrl();
				var returnUrl = GetQSReturnUrl();
				if(string.IsNullOrEmpty(returnUrl)) {
					returnUrl = Url.Action("profile", "user", new { id = CurrentUser.UserId });
				}
				return Json(new { status = "ok", responseMessage = returnUrl });
			}

			// If we got this far, something failed, redisplay form
			return Json(new { status = "error", responseMessage = StringResources.IncorrectPasswordOrEmail });
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult LogOff() {
			AuthenticationManager.Logout();
			return RedirectToAction("index", "home");
		}

		[AllowAnonymous]
		public ActionResult Register(string returnurl) {
		if(Request.IsAuthenticated) {
			return RedirectToAction("index", "home");
		}

		var model = new RegisterModel();
		model.ImageUploadData.Url = Url.Content("~" + Constants.DEFAULT_PHOTO_PATH);
		model.ReturnUrl = GetQSReturnUrl();
		ViewBag.ReturnUrl = model.ReturnUrl;
		return View(model);
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public ActionResult Register(RegisterModel model) {
			if (ModelState.IsValid) {
				var user = AuthenticationManager.VerifyUserEmail(model.Email);
				if(user != null && user.Status == (int)UserStatus.Deleted) {
					return RedirectToAction("recoverAccount", "account");
				}

				long userId = -1;
				// Attempt to register user
				try {
					//Set additional data for registration
					model.ActivationKey = Guid.NewGuid().ToString();
					model.RoleId = SystemRoles.Customer.ToString();

					//Register user
					userId = AuthenticationManager.Register(model);
					
					//upload profile photo
					if(Request.Files.Count > 0) {
						model.ImageUploadData.ProfilePhoto = Request.Files[0];
					}

					bool isFileUploaded = ContentManager.UploadFile(userId, model.ImageUploadData);
					if (!isFileUploaded) {
						AuthenticationManager.RollbackRegistration(userId);
						model.ImageUploadData.Url = string.Empty;
						ModelState.AddModelError("ImageUploadData.ProfilePhoto", StringResources.ImageUploadValidationMessage);
						return View(model);
					}
				}
				catch {
					//revert changes
					if (userId > 0) {
						AuthenticationManager.RollbackRegistration(userId);
					}

					return RedirectToAction("externaloginfailure", "account");
				}

				//Login
				AuthenticationManager.Login(model.Email, model.Password);
				//Send confirmation email
				var message = new EmailConfirmMessage(model.Name, model.Email, model.ActivationKey);
				MailSender.SendMail(message);

				if(string.IsNullOrEmpty(model.ReturnUrl)) {
					return RedirectToAction("confirmemail", "account");
				} else {
					return Redirect(model.ReturnUrl);
				}
			}

			// If we got this far, something failed, redisplay form
			model.ImageUploadData.Url = ContentManager.DEFAULT_PHOTO_PATH;
			return View(model);
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public ActionResult ExternalLogin(string provider)
		{
			//var returnUrl = GetReturnUrl();
			var returnUrl = GetQSReturnUrl();
			return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
		}

		[AllowAnonymous]
		public ActionResult ExternalLoginCallback(string returnurl) {
			// Rewrite request before it gets passed on to the OAuth Web Security classes
			GooglePlusClient.RewriteRequest();
			FacebookClient.RewriteRequest();

			AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnurl }));
			if (!result.IsSuccessful) {
				Logger.LogDebug("Authentication result was unsuccessfull");
				return RedirectToAction("ExternalLoginFailure");
			}

			//If user performs login via third party service
			ExternalUserInfo info = AuthenticationManager.OAuthLogin(result.Provider, result.ProviderUserId);
			if (info != null) {
				if(info.User.Status != (int)UserStatus.Deleted) {
				AuthenticationManager.RegisterExternalUser(info, false);
				AuthenticationManager.UpdateOAuthAccount(result);

				return RedirectToLocal(returnurl);
				} else {
					return RedirectToAction("recoverAccount", "account");
				}
			} else {
				// User is new, ask for their desired membership name
				AuthenticationClientData clientData = OAuthWebSecurity.GetOAuthClientData(result.Provider);
				var registerModel = AuthenticationManager.PopulateExternalLoginData(result, CurrentCulture);
				ViewBag.Provider = result.Provider;
				return View("externalloginconfirmation", registerModel);
			}
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public ActionResult ExternalLoginConfirmation(RegisterExternalLoginModel model, string returnUrl) {
			if (ModelState.IsValid) {
				var user = AuthenticationManager.VerifyUserEmail(model.Email);
				if (user != null && user.Status == (int)UserStatus.Deleted) {
					return RedirectToAction("recoverAccount", "account");
				}

				long userId = -1;
				try {
					// Insert a new user into the database
					//Set additional data for registration
					model.ActivationKey = Guid.NewGuid().ToString();
					userId = AuthenticationManager.OAuthRegister(model, SystemRoles.Customer);

					//upload profile photo and login
					if (Request.Files.Count > 0) {
						model.ImageUploadData.ProfilePhoto = Request.Files[0];
					}

					bool isFileUploaded = ContentManager.UploadFile(userId, model.ImageUploadData);
					if(!isFileUploaded) {
						AuthenticationManager.RollbackRegistration(userId);
						ModelState.AddModelError("ProfilePhoto", StringResources.InvalidPhotoUpload);
						return View(model);
					}
				} catch(Exception ex) {
					//revert changes
					if(userId > 0) {
						AuthenticationManager.RollbackRegistration(userId);
					}

					Logger.LogErrorException("Error registrating external user", ex);
					return RedirectToAction("externalloginfailure", "account");
				}

				//Login
				ExternalUserInfo info =AuthenticationManager.OAuthLogin(model.ProviderName, model.ProviderUserId);
				//If user performs login via third party service
				if (info != null) {
					AuthenticationManager.RegisterExternalUser(info, false);
				}
				//Send confirmation email
				string url = this.Url.Action("confirmemail", "account", new { guid = model.ActivationKey }, Request.Url.Scheme);
				var message = new EmailConfirmMessage(model.FirstName, model.Email, url);
				MailSender.SendMail(message);

				return RedirectToAction("confirmemail", "account");
			}

			ViewBag.ReturnUrl = returnUrl;
			return View(model);
		}

		[AllowAnonymous]
		public ActionResult ExternalLoginFailure()
		{
			return View();
		}

		[AllowAnonymous]
		public ActionResult ConfirmEmail(string guid) {
			ViewBag.InitialMessage = false;
			ViewBag.ActivatedMessage = false;
			ViewBag.AlreadyActivated = false;

			//Initial confirm email view
			if(string.IsNullOrWhiteSpace(guid)) {
				if (!Request.IsAuthenticated) {
					return RedirectToAction("Index", "Home");
				}

				ViewBag.InitialMessage = true;
			} else {
				if(AuthenticationManager.ActivateUser(guid)) {
					ViewBag.UserId = CurrentUser.UserId;
					ViewBag.ActivatedMessage = true;
				} else {
					if (!Request.IsAuthenticated) {
						return RedirectToAction("Index", "Home");
					}

					ViewBag.AlreadyActivated = true;
				}
			}

			return View();
		}

		[AllowAnonymous]
		public ActionResult RecoverPassword(string guid) {
			ViewBag.ShowChangePassword = false;
			ViewBag.ShowExpired = false;

			if (!string.IsNullOrEmpty(guid)) {
				if(AuthenticationManager.VerifyPasswordRecoveryRequest(guid)) {
					ViewBag.ShowChangePassword = true;
					TempData["user_guid"] = guid;

					return View(new ResetPasswordModel());

				} else {
					ViewBag.ShowExpired = true;
				}
			}

			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		public ActionResult RecoverPassword(ResetPasswordModel model) {
			if(ModelState.IsValid) {
				var guid = TempData["user_guid"] == null ? string.Empty: TempData["user_guid"].ToString();
				long userId = AuthenticationManager.ResetPassword(guid, model.NewPassword);
				return RedirectToAction("profile", "user", new { id = userId });
			}

			return View(model);
		}

		[AllowAnonymous]
		public JsonResult SendRecoverEmail(string email) {
			var user = AuthenticationManager.ProcessPasswordRecoveryRequest(email);
			if(user != null && user.Status != (int)UserStatus.Deleted) {
				var message = new ForgotPasswordMessage(user.FirstName, email, user.RecoveryKey);
				MailSender.SendMail(message);

				return Json(new JsonResponse { Status = (int)HttpStatusCode.OK, ResponseMessage = StringResources.MessageSuccessfullySent });
			}

			return Json(new JsonResponse { Status = (int)HttpStatusCode.InternalServerError, ResponseMessage = StringResources.NoUserWithSuchEmail });
		}

		[AjaxSession]
		public void ResendConfirmationEmail() {
			var message = new EmailConfirmMessage(CurrentUser.FirstName, CurrentUser.Email, CurrentUser.ActivationKey);
			MailSender.SendMail(message);
		}

		[AllowAnonymous]
		public ActionResult GetPartialLogin() {
			var model = new PartialLoginModel();
			if(CurrentUser != null) {
				model.DisplayName = CurrentUser.FirstName + " " + CurrentUser.LastName;
				model.UserLink = Url.Action("profile", "user", new { id = CurrentUser.UserId });
			}
			return PartialView("_LoginPartial", model);
		}

		[AllowAnonymous]
		public ActionResult RecoverAccount(string guid) {
			ViewBag.IncorrectGuid = false;
			if (!string.IsNullOrEmpty(guid)) {
				var user = AuthenticationManager.RecoverUser(guid);
				if(user != null) {
					TempData[Constants.LOGIN_MESSAGE] = StringResources.AccountRecovered;
					return RedirectToAction("profile", "user", new { id = user.UserId });
				}

				ViewBag.IncorrectGuid = true;
			}
			
			return View(new RecoverAccountModel());
		}

		[AllowAnonymous]
		[HttpPost]
		public JsonResult SendRecoverAccountEmail(string email) {
			var user = AuthenticationManager.ProcessPasswordRecoveryRequest(email);
			if (user != null && user.Status == (int)UserStatus.Deleted) {
				AuthenticationManager.ProcessAccountRecovery(email);

				return Json(new JsonResponse { Status = (int)HttpStatusCode.OK, ResponseMessage = StringResources.MessageSuccessfullySent });
			}

			return Json(new JsonResponse { Status = (int)HttpStatusCode.InternalServerError, ResponseMessage = StringResources.NoUserWithSuchEmail });
		}

		#region Helpers

		private ActionResult RedirectToLocal(string returnUrl)
		{
			if (Url.IsLocalUrl(returnUrl))
			{
				return Redirect(returnUrl);
			}
			else
			{
				return RedirectToAction("Profile", "User", new { id = CurrentUser.UserId });
			}
		}

		private string GetReturnUrl() {
			var returnUrl = string.Empty;
			if (TempData[Constants.RETURN_URL] != null) {
				returnUrl = TempData[Constants.RETURN_URL].ToString();
			}

			return returnUrl;
		}

		private string GetQSReturnUrl() {
			var returnUrl = Request.QueryString[Constants.RETURN_URL];
			return returnUrl;
		}

		internal class ExternalLoginResult : ActionResult
		{
			public ExternalLoginResult(string provider, string returnUrl)
			{
				Provider = provider;
				ReturnUrl = returnUrl;
			}

			public string Provider { get; private set; }
			public string ReturnUrl { get; private set; }

			public override void ExecuteResult(ControllerContext context)
			{
				OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
			}
		}

		#endregion
	}
}
