﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Workit.Common;

namespace WorkItWebsite.Controllers {
	public class HomeController : BaseController {
		public ActionResult Index() {
			string returnUrl = Request.QueryString[Constants.RETURN_URL];
			if (!string.IsNullOrEmpty(returnUrl)) {
				ViewBag.ShowLoginPopup = true;
				ViewBag.LoginMessage = "";
				ViewBag.ReturnUrl = returnUrl;
				if (TempData[Constants.LOGIN_MESSAGE] != null) {
					ViewBag.LoginMessage = TempData[Constants.LOGIN_MESSAGE];
				}
				TempData[Constants.RETURN_URL] = returnUrl;
			}

			return View();
		}

		public ActionResult About() {
			return View();
		}

		public ActionResult Contact() {
			return View();
		}

		public ActionResult Licensing() {
			return View();
		}

		public ActionResult Executors() {
			return View();
		}

		public ActionResult Terms() {
			return View();
		}

		public ActionResult PrivacyPolicy() {
			return View();
		}

		public ActionResult HowTo() {
			return View();
		}
	}
}
