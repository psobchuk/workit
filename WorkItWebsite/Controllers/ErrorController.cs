﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WorkItWebsite.Controllers {
	public class ErrorController : Controller {
		public ActionResult Error404(HandleErrorInfo model) {
			return View(model);
		}

		public ActionResult Error500(HandleErrorInfo model) {
			return View(model);
		}
	}
}
