﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WorkItWebsite.Models;

namespace WorkItWebsite.Controllers
{
	[AllowAnonymous]
	public class MediaController : BaseController {

		//Image preview for IE8+
		[HttpPost]
		public ActionResult PreviewImage() {
			var bytes = new byte[0];
			ViewBag.Mime = "image/png";
			ViewBag.Valid = true;

			if (Request.Files.Count == 1) {
				if(ContentManager.ValidateFileExtension(Request.Files[0].FileName)) {
					bytes = new byte[Request.Files[0].ContentLength];
					Request.Files[0].InputStream.Read(bytes, 0, bytes.Length);
					ViewBag.Mime = Request.Files[0].ContentType;
				} else {
					ViewBag.Valid = false;
				}
			}
			
			ViewBag.Message = Convert.ToBase64String(bytes, Base64FormattingOptions.InsertLineBreaks);
			return PartialView("_PreviewImage");
		}
	}
}
