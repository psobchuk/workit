﻿using AutoMapper;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Workit.Common;
using Workit.DataAccess;
using Workit.DataModel;
using WorkItWebsite.Filters;
using WorkItWebsite.Models;

namespace WorkItWebsite.Controllers {
	[Authorize]
	[IsAdmin]
	public class AdminController : BaseController {
		private EntityProvider db = new EntityProvider();

		public ActionResult Index() {
			return View();
		}

		public ActionResult Users() {
			return View();
		}

		public ActionResult Tasks() {
			return View();
		}

		#region User Admin Methods

		public JsonResult GetUsers([DataSourceRequest]DataSourceRequest request) {
			int totalCount = 0;
			List<User> users = UserManager.FilterUsers(request, out totalCount);

			//map admin models
			var lst = new List<UserAdminModel>();
			foreach (var user in users) {
				lst.Add(new UserAdminModel(user));
			}
			
			//init datasource result
			var result = new DataSourceResult {
				Data = lst,
				Total = totalCount
			};

			return Json(result, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult DeleteUser(long userId) {
			try {
				db.UserRepository.Delete(userId);
				db.SaveChanges();
			} catch(Exception ex) {
				Logger.LogErrorException("Error deleting user", ex);
				return Json("Delete operation aborted");
			}

			return Json("OK");
		}

		public ActionResult AddUserModal() {
			ViewBag.Title = "Add user";
			ViewBag.CloseBtn = "Close";
			ViewBag.OkBtn = "Add user";

			return PartialView("_AddUser", new UserAdminModel());
		}

		[HttpPost]
		[ValidateAjax]
		public JsonResult AddUser(UserAdminModel model) {
			var user = Mapper.Map<User>(model);
			try {
				user.Roles = new List<Role>();
				var roleId = Convert.ToInt32(model.Type);
				var role = db.RoleRepository.Get(r => r.RoleId == roleId).FirstOrDefault();
				user.Roles.Add(role);

				user = db.UserRepository.Insert(user);
				db.SaveChanges();
			} catch(Exception ex) {
				Logger.LogErrorException("Error adding user from admin panel", ex);
				return Json(new { Status = "Error", Message = "Error adding user from admin panel" });
			}

			return Json(new { Status = "OK", Data = new UserViewModel(user) });
		}

		#endregion

		#region Task Admin Methods

		public JsonResult GetTasks() {
			var tasks = db.OrderRepository.Get().ToList();
			var lst = new List<TaskViewModel>();

			return Json(lst, JsonRequestBehavior.AllowGet);
		}

		public ActionResult AddTaskModal() {
			ViewBag.Title = "Add task";
			ViewBag.CloseBtn = "Close";
			ViewBag.OkBtn = "Add task";
			var categories = db.CategoryRepository.Get().ToList();

			return PartialView("_AddTask", new TaskAdminModel(categories));
		}

		[HttpPost]
		[ValidateAjax]
		public JsonResult AddTask(TaskAdminModel model) {
			var order = Mapper.Map<Order>(model);
			
			try {
				long deadlineTicks = DateTime.Parse(model.DeadlineDate).Ticks + DateTime.Parse(model.DeadlineTime).Ticks;
				order.Deadline = new DateTime(deadlineTicks);
				order.Client = CurrentUser;
				db.OrderRepository.Insert(order);
				db.SaveChanges();
			} catch (Exception ex) {
				Logger.LogErrorException("Error adding order from admin panel", ex);
				return Json(new { Status = "Error", Message = "Error adding order from admin panel" });
			}

			return Json(new { Status = "OK", Data = new TaskViewModel() });
		}

		#endregion
	}
}
