﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Workit.Common;
using Workit.DataAccess;
using Workit.DataModel;
using Workit.Resources;
using WorkItWebsite.Filters;
using WorkItWebsite.Helpers;
using WorkItWebsite.Models;

namespace WorkItWebsite.Controllers {
	[Authorize]
	public class UserController : BaseController {

		[AllowAnonymous]
		[OutputCache(NoStore=true, Duration= 0, VaryByParam="none")]
		public new ActionResult Profile(long id) {
			User user = UserManager.GetUser(id);
			if(user == null || UserManager.ValidateUserStatus(user, UserStatus.Deleted)) {
				//user not found. throw exception
				throw new HttpException(404, StringResources.ProfileNotFound);
			} 

			var profile = ProfileManager.GetProfileModel(user, CurrentUser);

			return View(profile);
		}

		[AllowAnonymous]
		public JsonResult GetUserTasks(TaskFilter filter) {
			filter.Step = 5;
			var tasks = TaskManager.FilterTasks(filter);
			var tasksVM = Mapper.Map<List<Order>, List<TaskViewModel>>(tasks);

			return Json(new { tasks = tasksVM, count = tasks.Count() }, JsonRequestBehavior.AllowGet);
		}

		[AllowAnonymous]
		public JsonResult GetUserResponses(long userId, int pIndex) {
			int step = 5;
			int skip = step * pIndex;
			List<Response> responses = EntityProvider.ResponseRepository.Get(i => i.Owner.UserId == userId).Skip(skip).Take(step).ToList();
			var responsesVM = new List<ResponseViewModel>();
			var urlHelper = new UrlHelper(Request.RequestContext);
			foreach(var item in responses) {
				var responseVM = new ResponseViewModel {
					RespondentId = item.RespondentId,
					CreatedDate = item.Created.Value.ToShortDateString(),
					Description = item.Description,
					ResponseType = item.ResponseType,
					ProfileUrl = urlHelper.Action("profile", "user", new { id = item.RespondentId })
				};

				var user = UserManager.GetUser(item.RespondentId);
				responseVM.RespondentName = user.FirstName + " " + user.LastName;
				responseVM.RespondentImage = ContentManager.GetUserFilePath(user.UserId, Constants.PROFILE_PHOTO);
				responsesVM.Add(responseVM);
			}

			return Json(new { responses = responsesVM }, JsonRequestBehavior.AllowGet);
		}

		public JsonResult SaveNotifications(NotificationViewModel model) {
			var newSettings = Mapper.Map<UserSettings>(model);
			UserManager.UpdateUserSettings(CurrentUser, newSettings);

			return Json(new { status = "ok" });
		}

		public ActionResult DeleteProfile() {
			AuthenticationManager.Logout();
			UserManager.DeleteUser(CurrentUser.UserId);

			return RedirectToAction("index", "home");
		}

		[OutputCache(NoStore = true, Duration = 0, VaryByParam = "none")]
		public ActionResult EditProfile() {
			UserEditViewModel model = ProfileManager.GetUserEditModel(CurrentUser);

			return View(model);
		}

		[HttpPost]
		public ActionResult EditProfile(UserEditViewModel model) {
			ProfileManager.SaveUserData(CurrentUser.UserId, model);

			return RedirectToAction("profile", new { id = CurrentUser.UserId });
		}

		[ValidateAjax]
		public JsonResult ChangePassword(ChangePasswordModel model) {
			var response = new JsonResponse();
			if(AuthenticationManager.VerifyUserPassword(CurrentUser.Password, model.OldPassword)) {
				UserManager.ChangePassword(CurrentUser, model.NewPassword);

				response.Status = (int)HttpStatusCode.OK;
				response.ResponseMessage = StringResources.PasswordWasChanged;

				return Json(response);
			}
			
			response.Status = (int)HttpStatusCode.InternalServerError;
			response.ResponseMessage = StringResources.IncorrectOldPassword;

			return Json(response);
		}

		[HttpPost]
		public ActionResult ChangePhoto(UserEditViewModel model) {
			//upload profile photo
			if (Request.Files.Count > 0) {
				model.ImageUploadData.ProfilePhoto = Request.Files[0];
			}

			ContentManager.UploadFile(CurrentUser.UserId, model.ImageUploadData);
			UserManager.DisposeCurrentUser();

			return RedirectToAction("editprofile");
		}
	}
}
