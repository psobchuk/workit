﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Workit.Common;
using Workit.DataModel;

namespace WorkItWebsite.Controllers {
	public class PaymentController : BaseController {
		public ActionResult CheckIn() {
			return View();
		}

		public ActionResult History() {
			List<PaymentHistory> historyList = UserManager.GetPaymentHistory(CurrentUser.UserId);
			ViewBag.UserId = CurrentUser.UserId;
			return View(historyList);
		}

		public ActionResult ClientSuccess(string operation_xml, string signature) {
			UserManager.ProcessPayment(operation_xml, signature, CurrentUser.UserId);
			ViewBag.UserId = CurrentUser.UserId;

			return View();
		}

		[HttpPost]
		public void ServerSuccess(string operation_xml, string signature) { }

		[HttpPost]
		public JsonResult LoadOperation(string amount) {
			decimal value = 0;
			if(decimal.TryParse(amount, out value)) {
				try {
					XmlDocument doc = LiqPayManager.LoadDocument(value);
					var operationXml = LiqPayManager.GetOperation_xml(doc);
					var signature = LiqPayManager.GetSignature(doc);

					return Json(new { status = "ok", operation = operationXml, signature = signature });
				} catch(Exception ex) {
					Logger.LogErrorException("Error generating payment", ex);
					throw;
				}

			}

			return Json(new { status = "error" });
		}
	}
}
