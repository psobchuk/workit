﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Workit.Common;
using Workit.DataAccess;
using Workit.DataModel;
using WorkItWebsite.Filters;

namespace WorkItWebsite.Controllers {
	[ValidateInput(false)]
	//[RequireHttps]
	public class BaseController : Controller {
		private EntityProvider db = new EntityProvider();

		public User CurrentUser {
			get {
				if(Session[Constants.CURRENT_USER] == null) {
					Session[Constants.CURRENT_USER] = UserManager.GetUser(User.Identity.Name);
				}

				return (User)Session[Constants.CURRENT_USER];
			}

		}

		public CultureInfo CurrentCulture { 
			get { return System.Threading.Thread.CurrentThread.CurrentCulture; }
		}

		public EntityProvider EntityProvider {
			get { return db; }
		}

		public TaskManager TaskManager {
			get { 
				//return CacheHelper.Instance.GetObject<TaskManager>(Constants.TASK_MANAGER, () => { return new TaskManager(db); });
				return new TaskManager(db);
			}
		}

		public UserManager UserManager {
			get {
				//return CacheHelper.Instance.GetObject<UserManager>(Constants.USER_MANAGER, () => { return new UserManager(db); });
				return new UserManager(db);
			}
		}

		public ProfileManager ProfileManager {
			get {
				//return CacheHelper.Instance.GetObject<ProfileManager>(Constants.PROFILE_MANAGER, () => { return new ProfileManager(db); });
				return new ProfileManager(db);
			}
		}

		public PaymentManager PaymentManager {
			get {
				//return CacheHelper.Instance.GetObject<PaymentManager>(Constants.PAYMENT_MANAGER, () => { return new PaymentManager(db); });
				return new PaymentManager(db);
			}
		}
	}
}
