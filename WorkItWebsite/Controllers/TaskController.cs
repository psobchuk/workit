﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Workit.Common;
using Workit.DataAccess;
using Workit.DataModel;
using Workit.Resources;
using WorkItWebsite.Filters;
using WorkItWebsite.Helpers;
using WorkItWebsite.Models;

namespace WorkItWebsite.Controllers {
	public class TaskController : BaseController {
		public ActionResult View(long id) {
			TaskViewModel tempTask = TaskManager.GetDraftedTask();

			if(tempTask != null) {
				tempTask.User = CurrentUser;
				Order order = TaskManager.AddTask(tempTask);
				return RedirectToAction("view", new { id = order.OrderId });
			}

			Order task = TaskManager.GetTask(id);

			if(task == null) {
				throw new HttpException(404, StringResources.TaskNotFound);
			}

			ViewBag.TaskId = id;
			ViewBag.UserId = -1;
			ViewBag.PerformerId = task.PerformerId;
			if(CurrentUser != null) {
				ViewBag.UserId = CurrentUser.UserId;
			}
			
			var model = new DisplayTaskViewModel(task, CurrentUser);

			return View(model);
		}

		public ActionResult All(string id) {
			ViewBag.PreselectedCategory = id ?? string.Empty;
			ViewBag.Categories = EntityProvider.CategoryRepository.Get().ToList();
			return View();
		}

		public ActionResult CreateTask() {
			var model = new TaskViewModel(setCategories: true);
			if(CurrentUser != null && !string.IsNullOrEmpty(CurrentUser.Phones)) {
				model.Phone = CurrentUser.Phones.Split(',')[0];
			}
			return View(model);
		}

		[HttpPost]
		public ActionResult CreateTask(TaskViewModel model) {
			if(TaskManager.ValidateTask(model, ModelState)) {
				if(CurrentUser != null) {
					model.User = CurrentUser;
					model.Status = (int)TaskStatus.NotStarted;
					var task = TaskManager.AddTask(model);
					return RedirectToAction("view", new { id = task.OrderId.ToString() });
				} else {
					model.Status = (int)TaskStatus.Drafted;
					TaskManager.SaveAsDraft(model);
					string url = Url.Action("view", "task", new { id = 0 });
					TempData[Constants.LOGIN_MESSAGE] = StringResources.TaskDraftSuccess;
					return RedirectToAction("Index", "Home", new { returnurl = url });
				}
			} else {
				//fix: repopulate categories when sending back the same form
				model.PopulateCategories();
				return View(model);
			}
		}

		public JsonResult Subscribe(RequestViewModel request) {
			if(!TaskManager.IsRequested(request.TaskId, CurrentUser.UserId) && !TaskManager.IsLocked(request.TaskId)) {
				request.RequestorId = CurrentUser.UserId;
				Request item = TaskManager.CreateRequest(request);
				
				User user = item.Task.Client;
				if(UserManager.VerifySettingsEnabled(user, EmailTemplate.NewRequest)) {
					MailSender.NotifyNewRequest(user.FirstName, user.Email, item);
				}

				request = Mapper.Map<RequestViewModel>(item);

				return Json(new { status = (int)HttpStatusCode.OK, data = request });
			}

			return Json("");
		}

		public JsonResult Unsubscribe(long id) {
			if(!TaskManager.IsLocked(id)) {
				TaskManager.RemoveTaskRequest(id, CurrentUser.UserId);

				return Json(new { status = (int)HttpStatusCode.OK, data = CurrentUser.UserId });
			}

			return Json("");
		}

		public JsonResult SaveComment(CommentViewModel comment) {
			if(CurrentUser.UserId == comment.CommenterId || TaskManager.IsTaskOwner(comment.TaskId, CurrentUser.UserId)) {
				comment.CommenterId = CurrentUser.UserId;
				Comment item = TaskManager.SaveComment(comment);
				comment = Mapper.Map<CommentViewModel>(item);

				return Json(new { status = (int)HttpStatusCode.OK, data = comment });
			}

			return Json("");
		}

		public JsonResult LoadAllTasks(TaskFilter filter) {
			filter.Step = 10;
			List<Order> tasks = TaskManager.FilterTasks(filter);
			var tasksVM = Mapper.Map<List<Order>, List<TaskViewModel>>(tasks);

			return Json(new { tasks = tasksVM, count = tasks.Count() }, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetRequests(long id) {
			List<Request> requests = TaskManager.GetTaskRequests(id);
			var url = Url.Action("profile", "user", new { id = 1 });
			var lst = Mapper.Map<List<RequestViewModel>>(requests);

			return Json( new { data = lst, status = (int)HttpStatusCode.OK }, JsonRequestBehavior.AllowGet);
		}


		//Method is fucked up. Refactor!
		[HttpPost]
		public ActionResult AcceptPerformer(long taskId, long requestorId) {
			var order = TaskManager.GetTask(taskId);

			if (TaskManager.IsTaskOwner(taskId, CurrentUser.UserId) && 
				TaskManager.IsRequested(taskId, requestorId) &&
				CurrentUser.Status == (int)UserStatus.Active) {
				Request request = EntityProvider.RequestRepository.Get(r => r.Requestor.UserId == requestorId && r.Task.OrderId == taskId).SingleOrDefault();
				long price = order.Price > 0 ? order.Price : long.Parse(request.CustomPrice);
				if(PaymentManager.CheckBalance(order.Client.UserId, price)) {
					TaskManager.AcceptAndLock(taskId, requestorId);
				}
			}

			return RedirectToAction("view", "task", new { id = taskId });
		}

		[HttpPost]
		public JsonResult CanAccept(long requestorId, long taskId) {
			var order = TaskManager.GetTask(taskId);

			if (TaskManager.IsTaskOwner(taskId, CurrentUser.UserId) && 
				TaskManager.IsRequested(taskId, requestorId) &&
				CurrentUser.Status == (int)UserStatus.Active) {
				Request request = EntityProvider.RequestRepository.Get(r => r.Requestor.UserId == requestorId && r.Task.OrderId == taskId).SingleOrDefault();
				long price = order.Price > 0 ? order.Price : long.Parse(request.CustomPrice);
				if(PaymentManager.CheckBalance(order.Client.UserId, price)) {
					return Json(new { status = "ok", canAccept = true });
				}
			}

			return Json(new { status = "error" });
		}

		public JsonResult Delete(long id) {
			if(TaskManager.IsTaskOwner(id, CurrentUser.UserId) && !TaskManager.IsLocked(id)) {
				TaskManager.DeleteTask(id);
				return Json(new { status = (int)HttpStatusCode.OK, url = ContentManager.GetUrl("profile", "user", new { id = CurrentUser.UserId })});
			}

			return Json("");
		}

		public JsonResult CloseTask(long id, string response, int rtype) {
			if(TaskManager.IsTaskOwner(id, CurrentUser.UserId)) {
				TaskManager.CloseTask(id, response, rtype);
				return Json(new { status = (int)HttpStatusCode.OK, url = ContentManager.GetUrl("view", "task", new { id = id }) });
			}

			return Json("");
		}
	}
}
