﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Workit.Common;
using Workit.DataAccess;
using Workit.DataModel;

namespace WorkItWebsite.Filters {
	public class IsAdminAttribute: ActionFilterAttribute {
		public override void OnActionExecuting(ActionExecutingContext filterContext) {
			string name = filterContext.HttpContext.User.Identity.Name;
			EntityProvider db = new EntityProvider();
			User user = db.UserRepository.Get(u => u.Email == name).SingleOrDefault();
			Role role = user.Roles.Where(r => r.RoleName == SystemRoles.Administrator.ToString()).SingleOrDefault();
			if(role != null) {
				base.OnActionExecuting(filterContext);
			} else {
				filterContext.Result = new HttpNotFoundResult("Сторінку не знайдено");
			}
		}
	}
}