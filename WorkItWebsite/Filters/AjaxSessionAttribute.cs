﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Workit.Common;
using WorkItWebsite.Helpers;

namespace WorkItWebsite.Filters {
	public class AjaxSessionAttribute: ActionFilterAttribute {
		public override void OnActionExecuting(ActionExecutingContext filterContext) {
			HttpRequestBase request = filterContext.HttpContext.Request;
			if (!request.IsAjaxRequest()) return;

			if (!request.IsAuthenticated || filterContext.HttpContext.Session[Constants.CURRENT_USER] == null) {
				var jsonResponse = new JsonResponse {
					Status = (int)HttpStatusCode.Redirect,
					ResponseMessage = "/home/index?returnUrl=" + request.UrlReferrer
				};

				filterContext.Result = new JsonResult { Data = jsonResponse };
			}
		}
	}
}