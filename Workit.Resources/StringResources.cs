﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Workit.Resources {
	public static class StringResources {
		public static string AboutProject {
			get { return Resources.Resources.AboutProject; }
		}

		public static string Yes {
			get { return Resources.Resources.Yes; }
		}

		public static string No {
			get { return Resources.Resources.No; }
		}

		public static string CourierCategoryDescription {
			get { return Resources.Resources.CourierCategoryDescription; }
		}

		public static string DeliverCategoryDescription {
			get { return Resources.Resources.DeliverCategoryDescription; }
		}

		public static string ComputerCategoryDescription {
			get { return Resources.Resources.ComputerCategoryDescription; }
		}

		public static string HelperCategoryDescription {
			get { return Resources.Resources.HelperCategoryDescription; }
		}

		public static string RepairCategoryDescription {
			get { return Resources.Resources.RepairCategoryDescription; }
		}

		public static string PhotoVideoCategoryDescription {
			get { return Resources.Resources.PhotoVideoCategoryDescription; }
		}

		public static string FlyersCategoryDescription {
			get { return Resources.Resources.FlyersCategoryDescription; }
		}

		public static string ProfileNotFound {
			get { return Resources.Resources.ProfileNotFound; }
		}

		public static string ProfileDeletion {
			get { return Resources.Resources.ProfileDeletion; }
		}

		public static string AreYouSureDeleteProfile {
			get { return Resources.Resources.AreYouSureDeleteProfile; }
		}

		public static string Basic {
			get { return Resources.Resources.Basic; }
		}

		public static string PreMiddle {
			get { return Resources.Resources.PreMiddle; }
		}

		public static string Middle {
			get { return Resources.Resources.Middle; }
		}

		public static string PreFull {
			get { return Resources.Resources.PreFull; }
		}

		public static string Full {
			get { return Resources.Resources.Full; }
		}

		public static string Warning {
			get { return Resources.Resources.Warning; }
		}

		public static string CannotSelectExecutorEmailNotActivated {
			get { return Resources.Resources.CannotSelectExecutorEmailNotActivated; }
		}

		public static string NotEnoughMoney {
			get { return Resources.Resources.NotEnoughMoney; }
		}

		public static string UserSelection {
			get { return Resources.Resources.UserSelection; }
		}

		public static string AreYouSureToSelectThisUser {
			get { return Resources.Resources.AreYouSureToSelectThisUser; }
		}

		public static string TaskDeletion {
			get { return Resources.Resources.TaskDeletion; }
		}

		public static string AreYouSureDeleteTask {
			get { return Resources.Resources.AreYouSureDeleteTask; }
		}

		public static string TaskDraftSuccess {
			get { return Resources.Resources.TaskDraftSuccess; }
		}

		public static string PasswordWasChanged {
			get { return Resources.Resources.PasswordWasChanged; }
		}

		public static string IncorrectOldPassword {
			get { return Resources.Resources.IncorrectOldPassword; }
		}

		public static string IncorrectPasswordOrEmail {
			get { return Resources.Resources.IncorrectPasswordOrEmail; }
		}

		public static string InvalidPhotoUpload {
			get { return Resources.Resources.InvalidPhotoUpload; }
		}

		public static string MessageSuccessfullySent {
			get { return Resources.Resources.MessageSuccessfullySent; }
		}

		public static string NoUserWithSuchEmail {
			get { return Resources.Resources.NoUserWithSuchEmail; }
		}

		public static string AccountRecovered {
			get { return Resources.Resources.AccountRecovered; }
		}

		public static string TaskNotFound {
			get { return Resources.Resources.TaskNotFound; }
		}

		public static string AboutMeDescription {
			get { return Resources.Resources.AboutMeDescription; }
		}
		
		public static string PriceSetByExecutor {
			get { return Resources.Resources.PriceSetByExecutor; }
		}

		public static string TaskIsOpenedForRequests {
			get { return Resources.Resources.TaskIsOpenedForRequests; }
		}

		public static string TaskIsInProgress {
			get { return Resources.Resources.TaskIsInProgress; }
		}

		public static string TaskCompleted {
			get { return Resources.Resources.TaskCompleted; }
		}

		public static string ChangePersonalData {
			get { return Resources.Resources.ChangePersonalData; }
		}

		public static string SetIfHaveVehicle {
			get { return Resources.Resources.SetIfHaveVehicle; }
		}

		public static string Job {
			get { return Resources.Resources.Job; }
		}

		public static string LanguagesYouKnow {
			get { return Resources.Resources.LanguagesYouKnow; }
		}

		public static string CityLiving {
			get { return Resources.Resources.CityLiving; }
		}

		public static string ProfileEditor {
			get { return Resources.Resources.ProfileEditor; }
		}

		public static string ProfileEditing {
			get { return Resources.Resources.ProfileEditing; }
		}

		public static string AboutUs {
			get { return Resources.Resources.AboutUs; }
		}

		public static string LoginBtn {
			get { return Resources.Resources.LoginBtn; }
		}

		public static string Hello {
			get { return Resources.Resources.Hello; }
		}

		public static string NoAccountQuestion {
			get { return Resources.Resources.NoAccountQuestion; }
		}

		public static string RegisterLoginText {
			get { return Resources.Resources.RegisterLoginText; }
		}

		public static string ForgotPasswordQuestion {
			get { return Resources.Resources.ForgotPasswordQuestion; }
		}

		public static string AccountBalanceRenewal {
			get { return Resources.Resources.AccountBalanceRenewal; }
		}

		public static string ActualAddress {
			get { return Resources.Resources.ActualAddress; }
		}

		public static string AdditionalInformation {
			get { return Resources.Resources.AdditionalInformation; }
		}

		public static string AdditionalInformationDangerPayment {
			get { return Resources.Resources.AdditionalInformationDangerPayment; }
		}

		public static IHtmlString AdditionalInformationPayment {
			get { return new HtmlString(Resources.Resources.AdditionalInformationPayment); }
		}

		public static string AdditionalProfit {
			get { return Resources.Resources.AdditionalProfit; }
		}

		public static IHtmlString AdditionalProfitDescription {
			get { return new HtmlString(Resources.Resources.AdditionalProfitDescription); }
		}

		public static string Address {
			get { return Resources.Resources.Address; }
		}

		public static string AllowedFileTypes {
			get { return Resources.Resources.AllowedFileTypes; }
		}

		public static string AllRightsReserved {
			get { return Resources.Resources.AllRightsReserved; }
		}

		public static string TotalResponses {
			get { return Resources.Resources.TotalResponses; }
		}

		public static string CreatedTasks {
			get { return Resources.Resources.CreatedTasks; }
		}

		public static string CompletedTasks {
			get { return Resources.Resources.CompletedTasks; }
		}

		public static string EditProfile {
			get { return Resources.Resources.EditProfile; }
		}

		public static string DeleteProfile {
			get { return Resources.Resources.DeleteProfile; }
		}

		public static string CurrentBalance {
			get { return Resources.Resources.CurrentBalance; }
		}

		public static string OperationHistory {
			get { return Resources.Resources.OperationHistory; }
		}

		public static string GetMoneyFromBalance {
			get { return Resources.Resources.GetMoneyFromBalance; }
		}

		public static string Languages {
			get { return Resources.Resources.Languages; }
		}

		public static string Education {
			get { return Resources.Resources.Education; }
		}

		public static string Level {
			get { return Resources.Resources.Level; }
		}

		public static string EducationEstatement {
			get { return Resources.Resources.EducationEstatement; }
		}

		public static string Specialty {
			get { return Resources.Resources.Specialty; }
		}

		public static string Work {
			get { return Resources.Resources.Work; }
		}

		public static string SheEmployed {
			get { return Resources.Resources.SheEmployed; }
		}

		public static string HeEmployed {
			get { return Resources.Resources.HeEmployed; }
		}

		public static string CompanyName {
			get { return Resources.Resources.CompanyName; }
		}

		public static string Position {
			get { return Resources.Resources.Position; }
		}

		public static string NoJob {
			get { return Resources.Resources.NoJob; }
		}

		public static string Vehicle {
			get { return Resources.Resources.Vehicle; }
		}

		public static string HasCar {
			get { return Resources.Resources.HasCar; }
		}

		public static string NoCar {
			get { return Resources.Resources.NoCar; }
		}

		public static string AboutMe {
			get { return Resources.Resources.AboutMe; }
		}

		public static string NoDescription {
			get { return Resources.Resources.NoDescription; }
		}

		public static string Responses {
			get { return Resources.Resources.Responses; }
		}

		public static string TasksForMe {
			get { return Resources.Resources.TasksForMe; }
		}

		public static string MyTasks {
			get { return Resources.Resources.MyTasks; }
		}

		public static string NotificationSettings {
			get { return Resources.Resources.NotificationSettings; }
		}

		public static string DoneTasks {
			get { return Resources.Resources.DoneTasks; }
		}

		public static string NoResponses {
			get { return Resources.Resources.NoResponses; }
		}

		public static string NoTasksCreatedYet {
			get { return Resources.Resources.NoTasksCreatedYet; }
		}

		public static string NoTasksCompleted {
			get { return Resources.Resources.NoTasksCompleted; }
		}

		public static string SendFollowingMessages {
			get { return Resources.Resources.SendFollowingMessages; }
		}

		public static string NotifyLastNews {
			get { return Resources.Resources.NotifyLastNews; }
		}

		public static string NotifyTasksAssigned {
			get { return Resources.Resources.NotifyTasksAssigned; }
		}

		public static string NotifyTasksFinished {
			get { return Resources.Resources.NotifyTasksFinished; }
		}

		public static string NotifyTasksAccepted {
			get { return Resources.Resources.NotifyTasksAccepted; }
		}

		public static string Edit {
			get { return Resources.Resources.Edit; }
		}

		public static string Cancel {
			get { return Resources.Resources.Cancel; }
		}

		public static string DateOfBirth {
			get { return Resources.Resources.DateOfBirth; }
		}

		public static string ResponsesAndTasks {
			get { return Resources.Resources.ResponsesAndTasks; }
		}

		public static string Task {
			get { return Resources.Resources.Task; }
		}

		public static string All {
			get { return Resources.Resources.All; }
		}

		public static string OldPassword {
			get { return Resources.Resources.OldPassword; }
		}

		public static string RepeatNewPassword {
			get { return Resources.Resources.RepeatNewPassword; }
		}

		public static string ChangingPhoto {
			get { return Resources.Resources.ChangingPhoto; }
		}

		public static string Profile {
			get { return Resources.Resources.Profile; }
		}

		public static string Age {
			get { return Resources.Resources.Age; }
		}

		public static string ProfileAccount {
			get { return Resources.Resources.ProfileAccount; }
		}

		public static string OpenTasksFilter {
			get { return Resources.Resources.OpenTasksFilter; }
		}

		public static string ChangingPassword {
			get { return Resources.Resources.ChangingPassword; }
		}

		public static string Close {
			get { return Resources.Resources.Close; }
		}

		public static string SaveChanges {
			get { return Resources.Resources.SaveChanges; }
		}

		public static string InProgressTasksFilter {
			get { return Resources.Resources.InProgressTasksFilter; }
		}

		public static string ClosedTasksFilter {
			get { return Resources.Resources.ClosedTasksFilter; }
		}

		public static string From {
			get { return Resources.Resources.From; }
		}

		public static string To {
			get { return Resources.Resources.To; }
		}

		public static string FilterBtn {
			get { return Resources.Resources.FilterBtn; }
		}

		public static string AllTasks {
			get { return Resources.Resources.AllTasks; }
		}

		public static string Price {
			get { return Resources.Resources.Price; }
		}

		public static string WriteSimpleResponse {
			get { return Resources.Resources.WriteSimpleResponse; }
		}

		public static string SetAdditionalQuestion {
			get { return Resources.Resources.SetAdditionalQuestion; }
		}

		public static string AllowSetPrice {
			get { return Resources.Resources.AllowSetPrice; }
		}

		public static string HowMuchWouldYouPay {
			get { return Resources.Resources.HowMuchWouldYouPay; }
		}

		public static string Time {
			get { return Resources.Resources.Time; }
		}

		public static string SetDeadline {
			get { return Resources.Resources.SetDeadline; }
		}

		public static string DescribeTaskDescription {
			get { return Resources.Resources.DescribeTaskDescription; }
		}

		public static string DescribeTask {
			get { return Resources.Resources.DescribeTask; }
		}

		public static string SelectCategory {
			get { return Resources.Resources.SelectCategory; }
		}

		public static string WhatToDoDescription {
			get { return Resources.Resources.WhatToDoDescription; }
		}

		public static string WhatToDo {
			get { return Resources.Resources.WhatToDo; }
		}

		public static string NewTask {
			get { return Resources.Resources.NewTask; }
		}

		public static string AssociateYourAccount(string arg0) {
			return string.Format(Resources.Resources.AssociateYourAccount, arg0);
		}

		public static string AssociationForm {
			get { return Resources.Resources.AssociationForm; }
		}

		public static string AssociationFormDescription(string arg0) {
			return string.Format(Resources.Resources.AssociationFormDescription, arg0);
		}

		public static IHtmlString AuthorizationErrorDescription {
			get { return new HtmlString(Resources.Resources.AuthorizationErrorDescription); }
		}

		public static IHtmlString AuthorizationErrorExcuse(string arg0) {
			return new HtmlString(string.Format(Resources.Resources.AuthorizationErrorExcuse, arg0));
		}

		public static string BackToMainPage {
			get { return Resources.Resources.BackToMainPage; }
		}

		public static string BackToProfile {
			get { return Resources.Resources.BackToProfile; }
		}

		public static string BecomeAnExecutor {
			get { return Resources.Resources.BecomeAnExecutor; }
		}

		public static string BirthDate {
			get { return Resources.Resources.BirthDate; }
		}

		public static string ChangePassword {
			get { return Resources.Resources.ChangePassword; }
		}

		public static string ChangePasswordEmailSend {
			get { return Resources.Resources.ChangePasswordEmailSend; }
		}

		public static string ChangePhoto { 
			get { return Resources.Resources.ChangePhoto; }
		}

		public static string CiteFiveBottom { 
			get { return Resources.Resources.CiteFiveBottom; }
		}
		
		public static string CiteFiveText {
			get { return Resources.Resources.CiteFiveText; }
		}

		public static string CiteFourBottom {
			get { return Resources.Resources.CiteFourBottom; }
		}

		public static string CiteFourText {
			get { return Resources.Resources.CiteFourText; }
		}

		public static string CiteOneBottom {
			get { return Resources.Resources.CiteOneBottom; }
		}

		public static string CiteOneText {
			get { return Resources.Resources.CiteOneText; }
		}

		public static string CiteThreeBottom {
			get { return Resources.Resources.CiteThreeBottom; }
		}

		public static string CiteThreeText {
			get { return Resources.Resources.CiteThreeText; }
		}

		public static string CiteTwoBottom {
			get { return Resources.Resources.CiteTwoBottom; }
		}

		public static string CiteTwoText {
			get { return Resources.Resources.CiteTwoText; }
		}

		public static string ComparePasswordValidationMessage {
			get { return Resources.Resources.ComparePasswordValidationMessage; }
		}

		public static string CompleteYourTaskLeaveResponse {
			get { return Resources.Resources.CompleteYourTaskLeaveResponse; }
		}

		public static string CompleteYourTaskLeaveResponseDescription {
			get { return Resources.Resources.CompleteYourTaskLeaveResponseDescription; }
		}

		public static string ComputerTask {
			get { return Resources.Resources.ComputerTask; }
		}

		public static string ConfirmNewPassword {
			get { return Resources.Resources.ConfirmNewPassword; }
		}

		public static string ConfirmPassword {
			get { return Resources.Resources.ConfirmPassword; }
		}

		public static string ContactInformation {
			get { return Resources.Resources.ContactInformation; }
		}

		public static string Contacts {
			get { return Resources.Resources.Contacts; }
		}

		public static string ConvenientWayToFindExecutor {
			get { return Resources.Resources.ConvenientWayToFindExecutor; }
		}

		public static string CourierTask {
			get { return Resources.Resources.CourierTask; }
		}

		public static string Created {
			get { return Resources.Resources.Created; }
		}

		public static string CreateNewAccount {
			get { return Resources.Resources.CreateNewAccount; }
		}

		public static string CreateTask {
			get { return Resources.Resources.CreateTask; }
		}

		public static string CurrentUserAccountWasAlreadyDeleted {
			get { return Resources.Resources.CurrentUserAccountWasAlreadyDeleted; }
		}

		public static string CurrentAccountWasAlreadyActivated {
			get { return Resources.Resources.CurrentAccountWasAlreadyActivated; }
		}

		public static string CustomerPhoneNumber {
			get { return Resources.Resources.CustomerPhoneNumber; }
		}

		public static string Date {
			get { return Resources.Resources.Date; }
		}

		public static string Deadline {
			get { return Resources.Resources.Deadline; }
		}

		public static string Delete {
			get { return Resources.Resources.Delete; }
		}

		public static string FinishTask {
			get { return Resources.Resources.FinishTask; }
		}

		public static string DeliveryTask {
			get { return Resources.Resources.DeliveryTask; }
		}

		public static string ResponseAboutExecutor {
			get { return Resources.Resources.ResponseAboutExecutor; }
		}

		public static string ValueQualityOrder {
			get { return Resources.Resources.ValueQualityOrder; }
		}

		public static string Good {
			get { return Resources.Resources.Good; }
		}

		public static string Neutral {
			get { return Resources.Resources.Neutral; }
		}

		public static string Bad {
			get { return Resources.Resources.Good; }
		}

		public static string Save {
			get { return Resources.Resources.Save; }
		}

		public static string RequestForm {
			get { return Resources.Resources.RequestForm; }
		}

		public static string SetTaskPrice {
			get { return Resources.Resources.SetTaskPrice; }
		}

		public static string InvalidPrice {
			get { return Resources.Resources.InvalidPrice; }
		}

		public static string Cost {
			get { return Resources.Resources.Cost; }
		}

		public static string CommentBtn {
			get { return Resources.Resources.CommentBtn; }
		}

		public static string SelectThisExecutor {
			get { return Resources.Resources.SelectThisExecutor; }
		}

		public static string Comment {
			get { return Resources.Resources.Comment; }
		}

		public static string CommentCannotBeEmpty {
			get { return Resources.Resources.CommentCannotBeEmpty; }
		}

		public static string DescribeWhatYouNeed {
			get { return Resources.Resources.DescribeWhatYouNeed; }
		}

		public static string DescribeWhatYouNeedDescription {
			get { return Resources.Resources.DescribeWhatYouNeedDescription; }
		}

		public static string Description {
			get { return Resources.Resources.Description; }
		}

		public static string DontWasteYourTime {
			get { return Resources.Resources.DontWasteYourTime; }
		}

		public static string EarnMoney {
			get { return Resources.Resources.EarnMoney; }
		}

		public static string EarnMoneyDescription {
			get { return Resources.Resources.EarnMoneyDescription; }
		}

		public static string Email {
			get { return Resources.Resources.Email; }
		}

		public static string EmailAddressShort {
			get { return Resources.Resources.EmailAddressShort; }
		}

		public static string EmailBoxConfirmation {
			get { return Resources.Resources.EmailBoxConfirmation; }
		}

		public static string EmailConfirmation {
			get { return Resources.Resources.EmailConfirmation; }
		}

		public static string EmailConfirmationDescription {
			get { return Resources.Resources.EmailConfirmationDescription; }
		}

		public static string EmailConfirmationNotSent {
			get { return Resources.Resources.EmailConfirmationNotSent; }
		}

		public static string EmailInUse {
			get { return Resources.Resources.EmailInUse; }
		}

		public static string EmailVerificationComplete {
			get { return Resources.Resources.EmailVerificationComplete; }
		}

		public static string ExecutorWorkService {
			get { return Resources.Resources.ExecutorWorkService; }
		}

		public static string FacebookLoginTooltip {
			get { return Resources.Resources.FacebookLoginTooltip; }
		}

		public static string Female {
			get { return Resources.Resources.Female; }
		}

		public static string FillRecoverPasswordForm {
			get { return Resources.Resources.FillRecoverPasswordForm; }
		}

		public static string FirstName {
			get { return Resources.Resources.FirstName; }
		}

		public static string FlyersTask {
			get { return Resources.Resources.FlyersTask; }
		}

		public static string ForExecutors {
			get { return Resources.Resources.ForExecutors; }
		}

		public static string ForWhomProject {
			get { return Resources.Resources.ForWhomProject; }
		}

		public static IHtmlString ForWhomProjectDescription {
			get { return new HtmlString(Resources.Resources.ForWhomProjectDescription); }
		}

		public static string FreeScheduling {
			get { return Resources.Resources.FreeScheduling; }
		}

		public static IHtmlString FreeSchedulingDescription {
			get { return new HtmlString(Resources.Resources.FreeSchedulingDescription); }
		}

		public static string Gender {
			get { return Resources.Resources.Gender; }
		}

		public static string HelperTask {
			get { return Resources.Resources.HelperTask; }
		}

		public static string HePublished {
			get { return Resources.Resources.HePublished; }
		}

		public static string HowToOrderTask {
			get { return Resources.Resources.HowToOrderTask; }
		}

		public static IHtmlString IAcceptTermsAndPolicy(string arg0, string arg1) {
			return new HtmlString(string.Format(Resources.Resources.IAcceptTermsAndPolicy, arg0, arg1));
		}

		public static string ImageUploadValidationMessage {
			get { return Resources.Resources.ImageUploadValidationMessage; }
		}

		public static string InvalidEmail {
			get { return Resources.Resources.InvalidEmail; }
		}

		public static string InvalidLink {
			get { return Resources.Resources.InvalidLink; }
		}

		public static string InvalidPhotoLink {
			get { return Resources.Resources.InvalidPhotoLink; }
		}

		public static string It { 
			get { return Resources.Resources.It; }
		}

		public static string LastName {
			get { return Resources.Resources.LastName; }
		}

		public static string Login {
			get { return Resources.Resources.Login; }
		}

		public static string LoginForm {
			get { return Resources.Resources.LoginForm; }
		}

		public static string LoginUsingAnotherService {
			get { return Resources.Resources.LoginUsingAnotherService; }
		}

		public static string LoginUsingServiceTitle(string arg0) {
			return string.Format(Resources.Resources.LoginUsingServiceTitle, arg0);
		}

		public static string Logout {
			get { return Resources.Resources.Logout; }
		}

		public static string Male {
			get { return Resources.Resources.Male; }
		}

		public static IHtmlString MandatoryFieldsMarkedStar {
			get { return new HtmlString(Resources.Resources.MandatoryFieldsMarkedStar); }
		}

		public static string MaxFileSize {
			get { return Resources.Resources.MaxFileSize; }
		}

		public static string MinSize {
			get {return Resources.Resources.MinSize; }
		}

		public static string MoneyBalanceRenewal {
			get { return Resources.Resources.MoneyBalanceRenewal; }
		}

		public static string MostPopularCategories {
			get { return Resources.Resources.MostPopularCategories; }
		}

		public static string NeedHelpCreateTask {
			get { return Resources.Resources.NeedHelpCreateTask; }
		}

		public static string NewPassword {
			get { return Resources.Resources.NewPassword; }
		}

		public static string NoPaymentHistory {
			get { return Resources.Resources.NoPaymentHistory; }
		}

		public static string NotRegisteredClickHere {
			get { return Resources.Resources.NotRegisteredClickHere; }
		}

		public static string OrSetLinkedPhoto {
			get { return Resources.Resources.OrSetLinkedPhoto; }
		}

		public static string OurContacts {
			get { return Resources.Resources.OurContacts; }
		}

		public static string PageNotFoundTitle {
			get { return Resources.Resources.PageNotFoundTitle; }
		}

		public static string Password {
			get { return Resources.Resources.Password; }
		}

		public static string PasswordValidationMessage {
			get { return Resources.Resources.PasswordValidationMessage; }
		}

		public static string PaymentAccountHistory {
			get { return Resources.Resources.PaymentAccountHistory; }
		}

		public static string PaymentOperationSuccess {
			get { return Resources.Resources.PaymentOperationSuccess; }
		}

		public static IHtmlString PersonalInfoOnProfilePage(string arg0) {
			return new HtmlString(string.Format(Resources.Resources.PersonalInfoOnProfilePage, arg0));
		}

		public static string PhoneNumber {
			get { return Resources.Resources.PhoneNumber; }
		}

		public static string PhoneNumbers {
			get { return Resources.Resources.PhoneNumbers; }
		}

		public static string PhotoVideoTask {
			get { return Resources.Resources.PhotoVideoTask; }
		}

		public static string PlanYourDay {
			get { return Resources.Resources.PlanYourDay; }
		}

		public static string PlanYourDayDescription {
			get { return Resources.Resources.PlanYourDayDescription; }
		}

		public static string PlusLoginTooltip {
			get { return Resources.Resources.PlusLoginTooltip; }
		}

		public static string QualityWarranty {
			get { return Resources.Resources.QualityWarranty; }
		}

		public static IHtmlString QualityWarrantyDescription {
			get { return new HtmlString(Resources.Resources.QualityWarrantyDescription); }
		}

		public static string RecoverAccountMessageSend {
			get { return Resources.Resources.RecoverAccountMessageSend; }
		}

		public static string RecoveringAccount {
			get { return Resources.Resources.RecoveringAccount; }
		}

		public static string RecoverPassword {
			get { return Resources.Resources.RecoverPassword; }
		}

		public static IHtmlString RecoverPasswordDescription {
			get { return new HtmlString(Resources.Resources.RecoverPasswordDescription); }
		}

		public static string Register {
			get { return Resources.Resources.Register; }
		}

		public static string RegisterBtn {
			get { return Resources.Resources.RegisterBtn; }
		}

		public static string RegisterError {
			get { return Resources.Resources.RegisterError; }
		}

		public static string RegistrationCompleteGreeting {
			get { return Resources.Resources.RegistrationCompleteGreeting; }
		}

		public static string RegistrationForm {
			get { return Resources.Resources.RegistrationForm; }
		}

		public static string RememberMe {
			get { return Resources.Resources.RememberMe; }
		}

		public static string RemoveRequest {
			get { return Resources.Resources.RemoveRequest; }
		}

		public static string RenewBalance {
			get { return Resources.Resources.RenewBalance; }
		}

		public static string RepairTask {
			get { return Resources.Resources.RepairTask; }
		}

		public static string RequiredValidationMessage {
			get { return Resources.Resources.RequiredValidationMessage; }
		}

		public static string Resend {
			get { return Resources.Resources.Resend; }
		}

		public static string ResendComplete {
			get { return Resources.Resources.ResendComplete; }
		}

		public static string RocketWizardDescription {
			get { return Resources.Resources.RocketWizardDescription; }
		}

		public static string RocketWizardDetailsDescription {
			get { return Resources.Resources.RocketWizardDetailsDescription; }
		}

		public static string SafeWork {
			get { return Resources.Resources.SafeWork; }
		}

		public static string SafeWorkDescription {
			get { return Resources.Resources.SafeWorkDescription; }
		}

		public static string SaveYourMoney {
			get { return Resources.Resources.SaveYourMoney; }
		}

		public static string SaveYourMoneyDescription {
			get { return Resources.Resources.SaveYourMoneyDescription; }
		}

		public static string SaveYourTime {
			get { return Resources.Resources.SaveYourTime; }
		}

		public static string SaveYourTimeDescription {
			get { return Resources.Resources.SaveYourTimeDescription; }
		}

		public static string SearchExecutorsForYourWork {
			get { return Resources.Resources.SearchExecutorsForYourWork; }
		}

		public static string SelectPerformerForTask {
			get { return Resources.Resources.SelectPerformerForTask; }
		}

		public static string SelectPerformerForTaskDescription {
			get { return Resources.Resources.SelectPerformerForTaskDescription; }
		}

		public static string SelectProfilePhoto {
			get { return Resources.Resources.SelectProfilePhoto; }
		}

		public static string SendEmail {
			get { return Resources.Resources.SendEmail; }
		}

		public static string SendRequest {
			get { return Resources.Resources.SendRequest; }
		}

		public static IHtmlString ServerErrorDescription {
			get { return new HtmlString(Resources.Resources.ServerErrorDescription); }
		}

		public static string ServerErrorTitle {
			get { return Resources.Resources.ServerErrorTitle; }
		}

		public static IHtmlString ServiceDescription {
			get { return new HtmlString(Resources.Resources.ServiceDescription); }
		}

		public static string ServiceForFindingExecutors {
			get { return Resources.Resources.ServiceForFindingExecutors; }
		}

		public static string SetEmailAddress {
			get { return Resources.Resources.SetEmailAddress; }
		}

		public static string SetYourSumInHryvnas {
			get { return Resources.Resources.SetYourSumInHryvnas; }
		}

		public static string ShePublished {
			get { return Resources.Resources.ShePublished; }
		}

		public static string SkabVasyl {
			get { return Resources.Resources.SkabVasyl; }
		}

		public static string SobchukPavlo {
			get { return Resources.Resources.SobchukPavlo; }
		}

		public static string SumHryvna {
			get { return Resources.Resources.SumHryvna; }
		}

		public static string TellToFriends {
			get { return Resources.Resources.TellToFriends; }
		}

		public static string ThisTask {
			get { return Resources.Resources.ThisTask; }
		}

		public static string TransactionId {
			get { return Resources.Resources.TransactionId; }
		}

		public static string UnableToCompleteAuthorization {
			get { return Resources.Resources.UnableToCompleteAuthorization; }
		}

		public static string UploadFile {
			get { return Resources.Resources.UploadFile; }
		}

		public static string ViewAllTasks {
			get { return Resources.Resources.ViewAllTasks; }
		}

		public static string ViewTask {
			get { return Resources.Resources.ViewTask; }
		}

		public static string VkLoginTooltip {
			get { return Resources.Resources.VkLoginTooltip; }
		}

		public static string WeAreSocial {
			get { return Resources.Resources.WeAreSocial; }
		}

		public static string WhatExecutorMeans {
			get { return Resources.Resources.WhatExecutorMeans; }
		}

		public static string WhatIsThis {
			get { return Resources.Resources.WhatIsThis; }
		}

		public static string WhoWeAre {
			get { return Resources.Resources.WhoWeAre; }
		}

		public static string WhoWeAreDescription {
			get { return Resources.Resources.WhoWeAreDescription; }
		}

		public static string WhySafe {
			get { return Resources.Resources.WhySafe; }
		}

		public static IHtmlString WhySafeDescription {
			get { return new HtmlString(Resources.Resources.WhySafeDescription); }
		}

		public static string WillingToDoTasks {
			get { return Resources.Resources.WillingToDoTasks; }
		}

		public static string WillingToDoTasksDescription {
			get { return Resources.Resources.WillingToDoTasksDescription; }
		}

		public static string YourPaymentRenewed {
			get { return Resources.Resources.YourPaymentRenewed; }
		}
	}
}
