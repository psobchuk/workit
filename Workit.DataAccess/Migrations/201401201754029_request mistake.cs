namespace Workit.DataAccess
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class requestmistake : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Comments", name: "Request_ReuqestId", newName: "Request_RequestId");
            RenameColumn(table: "dbo.Requests", name: "ReuqestId", newName: "RequestId");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.Requests", name: "RequestId", newName: "ReuqestId");
            RenameColumn(table: "dbo.Comments", name: "Request_RequestId", newName: "Request_ReuqestId");
        }
    }
}
