namespace Workit.DataAccess
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class comments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        CommentId = c.Long(nullable: false),
                        Text = c.String(nullable: false),
                        Created = c.DateTime(),
                        Commenter_UserId = c.Long(),
                        Request_ReuqestId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.CommentId)
                .ForeignKey("dbo.Users", t => t.Commenter_UserId)
                .ForeignKey("dbo.Requests", t => t.Request_ReuqestId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.CommentId)
                .Index(t => t.Commenter_UserId)
                .Index(t => t.Request_ReuqestId)
                .Index(t => t.CommentId);
            
            AddColumn("dbo.Requests", "Question", c => c.String());
            AddColumn("dbo.Requests", "Task_OrderId", c => c.Long());
            AlterColumn("dbo.Requests", "ReuqestId", c => c.Long(nullable: false, identity: true));
            CreateIndex("dbo.Requests", "Task_OrderId");
            CreateIndex("dbo.Requests", "ReuqestId");
            AddForeignKey("dbo.Requests", "Task_OrderId", "dbo.Orders", "OrderId");
            AddForeignKey("dbo.Requests", "ReuqestId", "dbo.Orders", "OrderId");
            DropColumn("dbo.Requests", "Comment");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Requests", "Comment", c => c.String());
            DropForeignKey("dbo.Requests", "ReuqestId", "dbo.Orders");
            DropForeignKey("dbo.Comments", "CommentId", "dbo.Users");
            DropForeignKey("dbo.Requests", "Task_OrderId", "dbo.Orders");
            DropForeignKey("dbo.Comments", "Request_ReuqestId", "dbo.Requests");
            DropForeignKey("dbo.Comments", "Commenter_UserId", "dbo.Users");
            DropIndex("dbo.Requests", new[] { "ReuqestId" });
            DropIndex("dbo.Comments", new[] { "CommentId" });
            DropIndex("dbo.Requests", new[] { "Task_OrderId" });
            DropIndex("dbo.Comments", new[] { "Request_ReuqestId" });
            DropIndex("dbo.Comments", new[] { "Commenter_UserId" });
            AlterColumn("dbo.Requests", "ReuqestId", c => c.Long(nullable: false, identity: true));
            DropColumn("dbo.Requests", "Task_OrderId");
            DropColumn("dbo.Requests", "Question");
            DropTable("dbo.Comments");
        }
    }
}
