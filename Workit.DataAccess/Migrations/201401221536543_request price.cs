namespace Workit.DataAccess
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class requestprice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Requests", "CustomPrice", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Requests", "CustomPrice");
        }
    }
}
