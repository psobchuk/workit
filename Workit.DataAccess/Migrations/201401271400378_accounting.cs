namespace Workit.DataAccess
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class accounting : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PaymentHistories",
                c => new
                    {
                        PaymentHistoryId = c.Long(nullable: false, identity: true),
                        Created = c.DateTime(),
                        Owner_UserId = c.Long(),
                    })
                .PrimaryKey(t => t.PaymentHistoryId)
                .ForeignKey("dbo.Users", t => t.Owner_UserId)
                .Index(t => t.Owner_UserId);
            
            AddColumn("dbo.UserInfoes", "PaymentBalance", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PaymentHistories", "Owner_UserId", "dbo.Users");
            DropIndex("dbo.PaymentHistories", new[] { "Owner_UserId" });
            DropColumn("dbo.UserInfoes", "PaymentBalance");
            DropTable("dbo.PaymentHistories");
        }
    }
}
