namespace Workit.DataAccess
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editcomments : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "Commenter_UserId", "dbo.Users");
            DropForeignKey("dbo.Comments", "CommentId", "dbo.Users");
            DropIndex("dbo.Comments", new[] { "Commenter_UserId" });
            DropIndex("dbo.Comments", new[] { "CommentId" });
            AlterColumn("dbo.Comments", "CommentId", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Comments", "Commenter_UserId", c => c.Long(nullable: false));
			CreateIndex("dbo.Comments", "CommentId");
            CreateIndex("dbo.Comments", "Commenter_UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "Commenter_UserId", "dbo.Users");
            DropIndex("dbo.Comments", new[] { "Commenter_UserId" });
			DropIndex("dbo.Comments", new[] { "CommentId" });
            AlterColumn("dbo.Comments", "Commenter_UserId", c => c.Long());
            AlterColumn("dbo.Comments", "CommentId", c => c.Long(nullable: false, identity: true));
            CreateIndex("dbo.Comments", "CommentId");
            CreateIndex("dbo.Comments", "Commenter_UserId");
            AddForeignKey("dbo.Comments", "CommentId", "dbo.Users", "UserId");
        }
    }
}
