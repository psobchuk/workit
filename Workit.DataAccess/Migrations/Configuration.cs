namespace Workit.DataAccess
{
    using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Workit.Common;
using Workit.DataModel;

    internal sealed class Configuration : DbMigrationsConfiguration<Workit.DataAccess.DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Workit.DataAccess.DatabaseContext context)
        {
			context.Roles.AddOrUpdate(r =>r.RoleName,
				new Role { RoleName = SystemRoles.Administrator.ToString() },
				new Role { RoleName = SystemRoles.Customer.ToString() },
				new Role { RoleName = SystemRoles.Performer.ToString() }
			);

			context.Categories.AddOrUpdate(r => r.Name,
				new Category { Name = "���'����� �������" },
				new Category { Name = "������ � ���������" },
				new Category { Name = "����'������ ��������" },
				new Category { Name = "����/���� �������" },
				new Category { Name = "³��������� �������" },
				new Category { Name = "������� �������" },
				new Category { Name = "������� �������" }
			);

			User admin = context.Users.Where(u => u.Email == "admin@rocketwizard.com.ua").FirstOrDefault();
			if(admin == null) {
				var user = new User();
				user.BirthDate = new DateTime(1991,5,13);
				user.Created = new DateTime(2014,1,30);
				user.Email = "admin@rocketwizard.com.ua";
				user.FirstName = "������������";
				user.LastName = "RocketWizard";
				user.Password = CryptoHelper.MD5Hash("@#9hKMP7");
				user.Status = (int)UserStatus.Active;
				user.Roles = context.Roles.ToList();
				user.UserInfo = new UserInfo();
				user.UserSettings = new UserSettings();
				user.PaymentHistory = new System.Collections.Generic.List<PaymentHistory>();
				user.ExternalLogins = new System.Collections.Generic.List<ExternalUserInfo>();

				context.Users.AddOrUpdate(user);
			}
        }
    }
}
