namespace Workit.DataAccess
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class accountingdetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentHistories", "OrderId", c => c.String());
            AddColumn("dbo.PaymentHistories", "Amount", c => c.Int(nullable: false));
            AddColumn("dbo.PaymentHistories", "Description", c => c.String());
            AddColumn("dbo.PaymentHistories", "TransactionId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentHistories", "TransactionId");
            DropColumn("dbo.PaymentHistories", "Description");
            DropColumn("dbo.PaymentHistories", "Amount");
            DropColumn("dbo.PaymentHistories", "OrderId");
        }
    }
}
