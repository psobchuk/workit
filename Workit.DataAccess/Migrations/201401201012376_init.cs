namespace Workit.DataAccess
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderId = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 150),
                        Description = c.String(nullable: false),
                        PerformerId = c.Long(),
                        Comments = c.String(),
                        Status = c.Int(nullable: false),
                        Price = c.Int(nullable: false),
                        Created = c.DateTime(),
                        Deadline = c.DateTime(),
                        Client_UserId = c.Long(nullable: false),
                        Category_CategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderId)
                .ForeignKey("dbo.Users", t => t.Client_UserId, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.Category_CategoryId, cascadeDelete: true)
                .Index(t => t.Client_UserId)
                .Index(t => t.Category_CategoryId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Long(nullable: false, identity: true),
                        Password = c.String(),
                        Email = c.String(nullable: false, maxLength: 100),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        IdentificationCode = c.String(),
                        Created = c.DateTime(),
                        LastModified = c.DateTime(),
                        LastLogin = c.DateTime(),
                        Status = c.Int(nullable: false),
                        ActivationKey = c.String(),
                        RecoveryKey = c.String(),
                        Gender = c.Int(nullable: false),
                        BirthDate = c.DateTime(),
                        Phones = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.ExternalUserInfoes",
                c => new
                    {
                        ExternalUserInfoId = c.Long(nullable: false, identity: true),
                        ProviderUserId = c.String(nullable: false),
                        Email = c.String(),
                        ProviderName = c.String(),
                        AvatarImageLink = c.String(),
                        User_UserId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.ExternalUserInfoId)
                .ForeignKey("dbo.Users", t => t.User_UserId, cascadeDelete: true)
                .Index(t => t.User_UserId);
            
            CreateTable(
                "dbo.Responses",
                c => new
                    {
                        ResponseId = c.Long(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 100),
                        Description = c.String(nullable: false),
                        ResponseType = c.Int(nullable: false),
                        RespondentId = c.Long(nullable: false),
                        Created = c.DateTime(),
                        Owner_UserId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.ResponseId)
                .ForeignKey("dbo.Users", t => t.Owner_UserId, cascadeDelete: true)
                .Index(t => t.Owner_UserId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        RoleId = c.Int(nullable: false, identity: true),
                        RoleName = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.RoleId);
            
            CreateTable(
                "dbo.UserInfoes",
                c => new
                    {
                        UserInfoId = c.Long(nullable: false),
                        ExtraDescription = c.String(),
                        HasCar = c.Boolean(nullable: false),
                        EducationType = c.String(),
                        EducationInstitute = c.String(),
                        Specialty = c.String(),
                        Languages = c.String(),
                        IsEmployed = c.Boolean(nullable: false),
                        CompanyName = c.String(),
                        Position = c.String(),
                        City = c.String(),
                    })
                .PrimaryKey(t => t.UserInfoId)
                .ForeignKey("dbo.Users", t => t.UserInfoId)
                .Index(t => t.UserInfoId);
            
            CreateTable(
                "dbo.UserSettings",
                c => new
                    {
                        UserSettingsId = c.Long(nullable: false),
                        NotifyNews = c.Boolean(nullable: false),
                        NotifyTaskAssigned = c.Boolean(nullable: false),
                        NotifyTaskFinished = c.Boolean(nullable: false),
                        NotifyNewTaskCategory = c.Boolean(nullable: false),
                        NotifyNewTaskComment = c.Boolean(nullable: false),
                        NotifyTaskAccepted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserSettingsId)
                .ForeignKey("dbo.Users", t => t.UserSettingsId)
                .Index(t => t.UserSettingsId);
            
            CreateTable(
                "dbo.Requests",
                c => new
                    {
                        ReuqestId = c.Long(nullable: false, identity: true),
                        Comment = c.String(),
                        RequestDate = c.DateTime(nullable: false),
                        Requestor_UserId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.ReuqestId)
                .ForeignKey("dbo.Users", t => t.Requestor_UserId, cascadeDelete: true)
                .Index(t => t.Requestor_UserId);
            
            CreateTable(
                "dbo.RoleUsers",
                c => new
                    {
                        Role_RoleId = c.Int(nullable: false),
                        User_UserId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Role_RoleId, t.User_UserId })
                .ForeignKey("dbo.Roles", t => t.Role_RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.User_UserId, cascadeDelete: true)
                .Index(t => t.Role_RoleId)
                .Index(t => t.User_UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Requests", "Requestor_UserId", "dbo.Users");
            DropForeignKey("dbo.Orders", "Category_CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Orders", "Client_UserId", "dbo.Users");
            DropForeignKey("dbo.UserSettings", "UserSettingsId", "dbo.Users");
            DropForeignKey("dbo.UserInfoes", "UserInfoId", "dbo.Users");
            DropForeignKey("dbo.RoleUsers", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.RoleUsers", "Role_RoleId", "dbo.Roles");
            DropForeignKey("dbo.Responses", "Owner_UserId", "dbo.Users");
            DropForeignKey("dbo.ExternalUserInfoes", "User_UserId", "dbo.Users");
            DropIndex("dbo.Requests", new[] { "Requestor_UserId" });
            DropIndex("dbo.Orders", new[] { "Category_CategoryId" });
            DropIndex("dbo.Orders", new[] { "Client_UserId" });
            DropIndex("dbo.UserSettings", new[] { "UserSettingsId" });
            DropIndex("dbo.UserInfoes", new[] { "UserInfoId" });
            DropIndex("dbo.RoleUsers", new[] { "User_UserId" });
            DropIndex("dbo.RoleUsers", new[] { "Role_RoleId" });
            DropIndex("dbo.Responses", new[] { "Owner_UserId" });
            DropIndex("dbo.ExternalUserInfoes", new[] { "User_UserId" });
            DropTable("dbo.RoleUsers");
            DropTable("dbo.Requests");
            DropTable("dbo.UserSettings");
            DropTable("dbo.UserInfoes");
            DropTable("dbo.Roles");
            DropTable("dbo.Responses");
            DropTable("dbo.ExternalUserInfoes");
            DropTable("dbo.Users");
            DropTable("dbo.Orders");
            DropTable("dbo.Categories");
        }
    }
}
