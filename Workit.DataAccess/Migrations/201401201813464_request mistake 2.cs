namespace Workit.DataAccess
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class requestmistake2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Requests", "Task_OrderId", "dbo.Orders");
            DropForeignKey("dbo.Requests", "RequestId", "dbo.Orders");
            DropIndex("dbo.Requests", new[] { "Task_OrderId" });
            DropIndex("dbo.Requests", new[] { "RequestId" });
            AlterColumn("dbo.Requests", "RequestId", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Requests", "Task_OrderId", c => c.Long(nullable: false));
            CreateIndex("dbo.Requests", "Task_OrderId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Requests", new[] { "Task_OrderId" });
            AlterColumn("dbo.Requests", "Task_OrderId", c => c.Long());
            AlterColumn("dbo.Requests", "RequestId", c => c.Long(nullable: false));
            CreateIndex("dbo.Requests", "RequestId");
            CreateIndex("dbo.Requests", "Task_OrderId");
            AddForeignKey("dbo.Requests", "RequestId", "dbo.Orders", "OrderId");
            AddForeignKey("dbo.Requests", "Task_OrderId", "dbo.Orders", "OrderId");
        }
    }
}
