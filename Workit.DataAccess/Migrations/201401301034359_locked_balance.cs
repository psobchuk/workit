namespace Workit.DataAccess
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class locked_balance : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserInfoes", "LockedBalance", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserInfoes", "LockedBalance");
        }
    }
}
