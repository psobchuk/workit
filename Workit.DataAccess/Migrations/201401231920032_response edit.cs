namespace Workit.DataAccess
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class responseedit : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Responses", "Title");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Responses", "Title", c => c.String(nullable: false, maxLength: 100));
        }
    }
}
