﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using Workit.Common;

namespace Workit.DataAccess {
	public class EntityProvider {
		#region Private Fields
		
		private DatabaseContext context = new DatabaseContext();
		private UserRepository userRepository;
		private OrderRepository orderRepository;
		private CategoryRepository categoryRepository;
		private RoleRepository roleRepository;
		private ExternalUserInfoRepository externalUserInfoRepository;
		private ResponseRepository responseRepository;
		private UserInfoRepository userInfoRepository;
		private UserSettingsRepository userSettingsRepository;
		private CommentRepository commentRepository;
		private RequestRepository requestRepository;
		private PaymentHistoryRepository paymentRepository;

		#endregion

		#region Public Properties

		public UserRepository UserRepository {
			get {
				if (userRepository == null) {
					userRepository = new UserRepository(context);
				}

				return userRepository;
			}
		}
		public OrderRepository OrderRepository {
			get {
				if(orderRepository == null) {
					orderRepository = new OrderRepository(context);
				}

				return orderRepository;
			}
		}
		public CategoryRepository CategoryRepository {
			get {
				if (categoryRepository == null) {
					categoryRepository = new CategoryRepository(context);
				}

				return categoryRepository;
			}
		}
		public RoleRepository RoleRepository {
			get {
				if (roleRepository == null) {
					roleRepository = new RoleRepository(context);
				}

				return roleRepository;
			}
		}
		public ExternalUserInfoRepository ExternalUserInfoRepository {
			get {
				if (externalUserInfoRepository == null) {
					externalUserInfoRepository = new ExternalUserInfoRepository(context);
				}

				return externalUserInfoRepository;
			}
		}
		public ResponseRepository ResponseRepository {
			get {
				if(responseRepository == null) {
					responseRepository = new ResponseRepository(context);
				}

				return responseRepository;
			}
		}
		public UserInfoRepository UserInfoRepository {
			get {
				if(userInfoRepository == null) {
					userInfoRepository = new UserInfoRepository(context);
				}

				return userInfoRepository;
			}
		}
		public UserSettingsRepository UserSettingsRepository {
			get {
				if (userSettingsRepository == null) {
					userSettingsRepository = new UserSettingsRepository(context);
				}

				return userSettingsRepository;
			}
		}
		public CommentRepository CommentRepository { 
			get {
				if(commentRepository == null) {
					commentRepository = new CommentRepository(context);
				}

				return commentRepository;
			}
		}
		public RequestRepository RequestRepository {
			get {
				if(requestRepository == null) {
					requestRepository = new RequestRepository(context);
				}

				return requestRepository;
			}
		}
		public PaymentHistoryRepository PaymentRepository {
			get {
				if(paymentRepository == null) {
					paymentRepository = new PaymentHistoryRepository(context);
				}

				return paymentRepository;
			}
		}


		#endregion

		public void SaveChanges() {
			context.SaveChanges();
		}
	}
}
