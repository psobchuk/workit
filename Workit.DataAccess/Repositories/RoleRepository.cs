﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workit.DataModel;

namespace Workit.DataAccess {
	public class RoleRepository: Repository<Role> {
		public RoleRepository(DatabaseContext context): base(context) { }
	}
}
