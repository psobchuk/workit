﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workit.DataModel;

namespace Workit.DataAccess {
	public class RequestRepository: Repository<Request> {
		public RequestRepository(DatabaseContext context): base(context) { }
	}
}
