﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workit.DataModel;

namespace Workit.DataAccess {
	public class UserInfoRepository: Repository<UserInfo> {
		public UserInfoRepository(DatabaseContext context): base(context) { }
	}
}
