﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workit.DataModel;

namespace Workit.DataAccess {
	public class ResponseRepository: Repository<Response> {
		public ResponseRepository(DatabaseContext context): base(context) { }
	}
}
