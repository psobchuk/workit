﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workit.DataModel;

namespace Workit.DataAccess {
	public class ExternalUserInfoRepository: Repository<ExternalUserInfo> {
		public ExternalUserInfoRepository(DatabaseContext context): base(context) { }
	}
}
