﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workit.DataModel;

namespace Workit.DataAccess {
	public class UserSettingsRepository: Repository<UserSettings> {
		public UserSettingsRepository(DatabaseContext context): base(context) { }
	}
}
