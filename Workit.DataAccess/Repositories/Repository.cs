﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Workit.DataAccess {
	public class Repository<TEntity> where TEntity : class {
		protected DatabaseContext db;
		public DbSet<TEntity> dbSet;
		
		public Repository(DatabaseContext context) {
			this.db = context;
			this.dbSet = db.Set<TEntity>();
		}

		public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null,
												Func<IQueryable<TEntity>, 
												IOrderedQueryable<TEntity>> orderBy = null,
												string includeProperties = "") {
			IQueryable<TEntity> query = dbSet;
			if (filter != null) {
				query = query.Where(filter);
			}

			foreach (var includeProperty in includeProperties.Split
				(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)) {
				query = query.Include(includeProperty);
			}

			if (orderBy != null) {
				return orderBy(query).ToList();
			} else {
				return query.ToList();
			}
		}

		public virtual TEntity Insert(TEntity entity) {
			return dbSet.Add(entity);
		}

		public virtual void Delete(object id) {
			TEntity entityToDelete = dbSet.Find(id);
			Delete(entityToDelete);
		}

		public virtual void Delete(TEntity entity) {
			if (db.Entry(entity).State == EntityState.Detached) {
				dbSet.Attach(entity);
			}

			dbSet.Remove(entity);
		}

		public virtual void Update(TEntity entityToUpdate, TEntity newValues) {
			db.Entry(entityToUpdate).CurrentValues.SetValues(newValues);
		}
	}
}
