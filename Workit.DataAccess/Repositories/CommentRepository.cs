﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workit.DataModel;

namespace Workit.DataAccess {
	public class CommentRepository: Repository<Comment> {
		public CommentRepository(DatabaseContext context): base(context) { }
	}
}
