﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workit.DataModel;

namespace Workit.DataAccess {
	public class CategoryRepository: Repository<Category> {
		public CategoryRepository(DatabaseContext context): base(context) { }
	}
}
