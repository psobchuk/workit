﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workit.DataModel;

namespace Workit.DataAccess {
	public class OrderRepository: Repository<Order> {
		public OrderRepository(DatabaseContext context): base(context) { }
	}
}
