﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workit.DataModel;

namespace Workit.DataAccess {
	public class PaymentHistoryRepository: Repository<PaymentHistory> {
		public PaymentHistoryRepository(DatabaseContext context): base(context) { }
	}
}
