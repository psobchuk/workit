﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Workit.DataModel;

namespace Workit.DataAccess {
	public class UserRepository: Repository<User> {

		public UserRepository(DatabaseContext context): base(context) { }

		public List<Role> GetUserRoles(long userId) {
			User user = dbSet.Include("Roles").Where(i => i.UserId == userId).SingleOrDefault();
			return user.Roles;
		}

		public override User Insert(User entity) {
			if(entity.UserSettings == null) {
				entity.UserSettings = new UserSettings();
			}

			if(entity.UserInfo == null) {
				entity.UserInfo = new UserInfo();
			}

			return base.Insert(entity);
		}
	}
}
