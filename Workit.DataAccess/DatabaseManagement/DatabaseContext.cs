﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Workit.DataModel;

namespace Workit.DataAccess {
	public class DatabaseContext: DbContext {
		public DbSet<User> Users { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<Category> Categories { get; set; }
		public DbSet<Role> Roles { get; set; }
		public DbSet<ExternalUserInfo> ExternalLogins { get; set; }
		public DbSet<Request> Requests { get; set; }
		public DbSet<Response> Responses { get; set; }
		public DbSet<UserInfo> UserInfoes { get; set; }
		public DbSet<UserSettings> UserSettings { get; set; }
		public DbSet<Comment> Comments { get; set; }
		public DbSet<PaymentHistory> PaymentHistory { get; set; }

		public DatabaseContext(): base(DbManager.ConnectionString) { }

		protected override void OnModelCreating(DbModelBuilder modelBuilder) {
			modelBuilder.Configurations.Add(new UserTypeConfiguration());
			modelBuilder.Configurations.Add(new OrderTypeConfiguration());
			modelBuilder.Configurations.Add(new CategoryTypeConfiguration());
			modelBuilder.Configurations.Add(new RoleTypeConfiguration());
			modelBuilder.Configurations.Add(new ExternalUserTypeConfiguration());
			modelBuilder.Configurations.Add(new RequestTypeConfiguration());
			modelBuilder.Configurations.Add(new ResponseTypeConfiguration());
			modelBuilder.Configurations.Add(new UserInfoTypeConfiguration());
			modelBuilder.Configurations.Add(new UserSettingsTypeConfiguration());
			modelBuilder.Configurations.Add(new CommentTypeConfiguration());
			modelBuilder.Configurations.Add(new PaymentHistoryTypeConfiguration());


			base.OnModelCreating(modelBuilder);
		}
	}
}
