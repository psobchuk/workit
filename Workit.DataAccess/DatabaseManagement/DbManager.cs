﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using Workit.Common;

namespace Workit.DataAccess {
	public class DbManager {
		public static string ConnectionString {
			get {
				return ConfigurationManager.ConnectionStrings[Constants.CONNECTION_STRING].ConnectionString;
			}
		}

		public static void InitializeDatabaseMigration() {
			Database.SetInitializer(
				new MigrateDatabaseToLatestVersion<DatabaseContext, Configuration>()
			);

			var dbMigrator = new DbMigrator(new Configuration());
			try {
				dbMigrator.Update();
			} catch (Exception ex) {
				Logger.LogErrorException("Error updating database: ", ex);
				throw;
			}
		}
	}
}
