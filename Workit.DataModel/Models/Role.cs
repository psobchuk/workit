﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Workit.DataModel {
	public class Role {
		public int RoleId { get; set; }
		public string RoleName { get; set; }

		public virtual List<User> Users { get; set; }
	}
	
	public class RoleTypeConfiguration: EntityTypeConfiguration<Role> {
		public RoleTypeConfiguration() {
			HasKey(key => key.RoleId);
			Property(p => p.RoleName).HasMaxLength(50).IsRequired();
		}
	}
}
