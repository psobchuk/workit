﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Workit.DataModel {
	public class Category {
		public int CategoryId { get; set; }
		public string Name { get; set; }

		public virtual List<Order> Orders { get; set; }
	}

	public class CategoryTypeConfiguration: EntityTypeConfiguration<Category> {
		public CategoryTypeConfiguration() {
			HasKey(key => key.CategoryId);
			Property(p => p.Name).HasMaxLength(100).IsRequired();
			HasMany(p => p.Orders).WithRequired(p => p.Category);
		}
	}
}
