﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Workit.DataModel {
	public class UserSettings {
		public long UserSettingsId { get; set; }
		public bool NotifyNews { get; set; }
		public bool NotifyTaskAssigned { get; set; }
		public bool NotifyTaskFinished { get; set; }
		public bool NotifyNewTaskCategory { get; set; }
		public bool NotifyNewTaskComment { get; set; }
		public bool NotifyTaskAccepted { get; set; }

		public virtual User User { get; set; }

		public UserSettings() {
			NotifyNews = false;
			NotifyNewTaskCategory = false;
			NotifyTaskAssigned = true;
			NotifyTaskFinished = true;
			NotifyNewTaskComment = true;
			NotifyTaskAccepted = true;
		}
	}

	public class UserSettingsTypeConfiguration: EntityTypeConfiguration<UserSettings> {
		public UserSettingsTypeConfiguration() {
			HasKey(k => k.UserSettingsId);
			HasRequired(p => p.User).WithRequiredDependent(p => p.UserSettings);
		}
	}
}
