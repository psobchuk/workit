﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Workit.DataModel {
	public class User {
		public long UserId { get; set; }
		public string Password { get; set; }
		public string Email { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string IdentificationCode { get; set; }
		public Nullable<DateTime> Created { get; set; }
		public Nullable<DateTime> LastModified { get; set; }
		public Nullable<DateTime> LastLogin { get; set; }
		public int Status { get; set; }
		public string ActivationKey { get; set; }
		public string RecoveryKey { get; set; }
		public int Gender { get; set; }
		public Nullable<DateTime> BirthDate { get; set; }
		public string Phones { get; set; }

		public virtual List<Order> Orders { get; set; }
		public virtual List<Role> Roles { get; set; }
		public virtual List<ExternalUserInfo> ExternalLogins { get; set; }
		public virtual List<Response> Responses { get; set; }
		public virtual UserInfo UserInfo { get; set; }
		public virtual UserSettings UserSettings { get; set; }
		public virtual List<Request> Requests { get; set; }
		public virtual List<Comment> Comments { get; set; }
		public virtual List<PaymentHistory> PaymentHistory { get; set; }
	}

	public class UserTypeConfiguration: EntityTypeConfiguration<User> {
		public UserTypeConfiguration() {
			HasKey(key => key.UserId);
			Property(p => p.Email).HasMaxLength(100).IsRequired();
			Property(p => p.FirstName).HasMaxLength(100).IsRequired();
			Property(p => p.LastName).HasMaxLength(100).IsRequired();
			Property(p => p.Status).IsRequired();
			HasMany(f =>f.Orders).WithRequired(f => f.Client);
			HasOptional(f => f.UserInfo).WithRequired(f => f.User);
			HasRequired(p => p.UserSettings).WithRequiredPrincipal(p => p.User);
			HasMany(p => p.Requests).WithRequired(p => p.Requestor);
		}
	}
}
