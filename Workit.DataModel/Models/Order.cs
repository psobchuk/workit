﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Workit.DataModel {
	public class Order {
		public long OrderId { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public long PerformerId { get; set; }
		public string Comments { get; set; }
		public int Status { get; set; }
		public int Price { get; set; }
		public Nullable<DateTime> Created { get; set; }
		public Nullable<DateTime> Deadline { get; set; }
		public string Phone { get; set; }


		public virtual User Client { get; set; }
		public virtual Category Category { get; set; }
		public virtual List<Request> Requests { get; set; }
	}

	public class OrderTypeConfiguration: EntityTypeConfiguration<Order> {
		public OrderTypeConfiguration() {
			HasKey(key => key.OrderId);
			Property(p => p.Name).HasMaxLength(150).IsRequired();
			Property(p => p.Description).IsRequired();
			Property(p => p.PerformerId).IsOptional();
			HasRequired(p => p.Client).WithMany(p => p.Orders);
		}
	}
}
