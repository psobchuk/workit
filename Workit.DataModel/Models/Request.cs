﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Workit.DataModel {
	public class Request {
		public long RequestId { get; set; }
		public string Question { get; set; }
		public Nullable<DateTime> RequestDate { get; set; }
		public string CustomPrice { get; set; }

		public virtual User Requestor { get; set; }
		public virtual Order Task { get; set; }
		public virtual List<Comment> Comments { get; set; }
	}

	public class RequestTypeConfiguration: EntityTypeConfiguration<Request> {
		public RequestTypeConfiguration() {
			HasKey(k => k.RequestId);
			Property(p => p.RequestDate).IsRequired();
			HasRequired(p => p.Requestor).WithMany(p => p.Requests);
			HasRequired(p => p.Task).WithMany(p => p.Requests);
		}
	}
}
