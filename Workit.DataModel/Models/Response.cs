﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Workit.DataModel {
	public class Response {
		public long ResponseId { get; set; }
		public string Description { get; set; }
		public int ResponseType { get; set; }
		public long RespondentId { get; set; }
		public Nullable<DateTime> Created { get; set; }

		public virtual User Owner { get; set; }
	}

	public class ResponseTypeConfiguration: EntityTypeConfiguration<Response> {
		public ResponseTypeConfiguration() {
			HasKey(k => k.ResponseId);
			Property(p => p.Description).IsRequired();
			Property(p => p.ResponseType).IsRequired();
			Property(p => p.RespondentId).IsRequired();
			HasRequired(p => p.Owner).WithMany(p => p.Responses);
		}
	}
}
