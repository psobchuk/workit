﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Workit.DataModel {
	public class PaymentHistory {
		public long PaymentHistoryId { get; set; }
		public Nullable<DateTime> Created { get; set; }
		public string OrderId { get; set; }
		public int Amount { get; set; }
		public string Description { get; set; }
		public string TransactionId { get; set; }

		public virtual User Owner { get; set; }
	}

	public class PaymentHistoryTypeConfiguration: EntityTypeConfiguration<PaymentHistory> {
		public PaymentHistoryTypeConfiguration() {
			HasKey(k => k.PaymentHistoryId);
		}
	}
}
