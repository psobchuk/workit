﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Workit.DataModel {
	public class UserInfo {
		public long UserInfoId { get; set; }
		public string ExtraDescription { get; set; }
		public bool HasCar { get; set; }
		public string EducationType { get; set; }
		public string EducationInstitute { get; set; }
		public string Specialty { get; set; }
		public string Languages { get; set; }
		public bool IsEmployed { get; set; }
		public string CompanyName { get; set; }
		public string Position { get; set; }
		public string City { get; set; }

		public long PaymentBalance { get; set; }
		public long LockedBalance { get; set; }

		public virtual User User { get; set; }

		public UserInfo() {
			ExtraDescription = string.Empty;
			HasCar = false;
			EducationInstitute = string.Empty;
			Specialty = string.Empty;
			Languages = string.Empty;
			IsEmployed = false;
			CompanyName = string.Empty;
			Position = string.Empty;
			City = string.Empty;
		}
	}

	public class UserInfoTypeConfiguration : EntityTypeConfiguration<UserInfo> {
		public UserInfoTypeConfiguration() {
			HasKey(k => k.UserInfoId);
			HasRequired(p => p.User).WithOptional(p => p.UserInfo);
		}
	}
}
