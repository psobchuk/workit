﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using Workit.DataModel;

namespace Workit.DataModel {
	public class Comment {
		public long CommentId { get; set; }
		public string Text { get; set; }
		public Nullable<DateTime> Created { get; set; }

		public virtual Request Request { get; set; }
		public virtual User Commenter { get; set; }
		
	}

	public class CommentTypeConfiguration: EntityTypeConfiguration<Comment> {
		public CommentTypeConfiguration() {
			HasKey(k => k.CommentId);
			Property(p => p.CommentId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
			HasRequired(p => p.Request).WithMany(p => p.Comments);
			HasRequired(p => p.Commenter).WithMany(p => p.Comments);
			Property(p => p.Text).IsRequired();
		}
	}
}
