﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;

namespace Workit.DataModel {
	public class ExternalUserInfo {
		public long ExternalUserInfoId { get; set; }
		public string ProviderUserId { get; set; }
		public string Email { get; set; }
		public string ProviderName { get; set; }
		public string AvatarImageLink { get; set; }

		public virtual User User { get; set; }
	}

	public class ExternalUserTypeConfiguration: EntityTypeConfiguration<ExternalUserInfo> {
		public ExternalUserTypeConfiguration() {
			HasKey(key => key.ExternalUserInfoId);
			Property(p => p.ProviderUserId).IsRequired();
			HasRequired(p => p.User).WithMany(p => p.ExternalLogins);
		}
	}
}
